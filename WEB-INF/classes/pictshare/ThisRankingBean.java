package pictshare;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.directwebremoting.annotations.RemoteMethod;
import org.directwebremoting.annotations.RemoteProxy;
import org.directwebremoting.annotations.ScriptScope;

/**
 * 今月ランキング
 * 
 * RankingBeanの配列をもつ
 */
@RemoteProxy(scope = ScriptScope.APPLICATION)
public class ThisRankingBean implements Serializable {
	
	/**
	 * ランキング
	 */
	private RankingBean[] ranking;
	public  RankingBean[] getRanking() {return ranking;}
	public  void setRanking(RankingBean[] ranking) {this.ranking = ranking;}
	
	/**
	 * コンストラクタ
	 */
	public ThisRankingBean() {}
	
	/**
	 * 今月ランキングを設定する
	 * 
	 * tomcat 起動時と、日付が変わるときのバッチ処理の中で実行される
	 * 
	 * RankingBeanの配列を生成
	 * 期間高評価数の多い投稿IDを100個まで取得する
	 * 連想配列に、投稿IDと、配列の要素番号を格納する準備
	 * 投稿リストを取得する
	 * 投稿IDと期間高評価数から順位を計算
	 * 連想配列と投稿IDで投稿リストのデータをセット
	 */
	public void setThisRanking() {
		
		// RankingBeanを生成
		RankingBean[] ranking = new RankingBean[100];
		for (int i=0; i<100; i++) {
			ranking[i] = new RankingBean();
		}
		
		// 期間高評価数の多い投稿IDを100個まで取得する
		ThisRankingSet thisRankingSet = get100Post();
		
		// 投稿IDリスト
		ArrayList<Integer> idList = thisRankingSet.idList;
		
		// 期間高評価数リスト
		ArrayList<Integer> goodList = thisRankingSet.goodList;
		
		// 投稿IDとRankingBean配列の添え字の連想配列
		HashMap<Integer, Integer> idIndexMap = new HashMap<Integer, Integer>();
		
		// ランキングなし
		if (idList.size() == 0) {
			setRanking(ranking);
			return;
		}
		
		// 投稿リストを取得する
		ArrayList<PostBean> post = PostBean.getPostByPostIdList(null, false, idList);
		
		int rank = 1;
		int tempGood = 0;
		for (int i=0; i<idList.size(); i++) {
			
			// 投稿ID
			int postId = idList.get(i);
			
			// 期間高評価数
			int good = goodList.get(i);
			
			// 期間高評価数をセット
			ranking[i].setGood(good);
			
			// 同じ順位なら順位そのまま
			// 同じ順位でなければ順位を計算
			if (good != tempGood) {
				rank = (i+1);
				tempGood = good;
			}
			
			// 順位をセット
			ranking[i].setRank(rank);
			
			// 連想配列に投稿IDと配列の添え字をセット
			idIndexMap.put(postId, i);
		}
		
		for (int i=0; i<post.size(); i++) {
			
			// 投稿IDを取得
			int postId = post.get(i).getPostId();
			
			// 投稿IDが 0 ならスキップ
			if (postId == 0) {
				continue;
			}
			
			// 連想配列から配列の添え字を取得
			int index = idIndexMap.get(postId);
			
			// 投稿IDなどをセット
			ranking[index].setPostId  (postId);
			ranking[index].setUserId  (post.get(i).getUserId());
			ranking[index].setDispName(post.get(i).getDispName());
			ranking[index].setIconFile(post.get(i).getIconFile());
			ranking[index].setImgFile (post.get(i).getImgFile().get(0));
			ranking[index].setMessage (post.get(i).getMessage());
			ranking[index].setPostDate(post.get(i).getPostDate());
		}
		setRanking(ranking);
	}
	
	/**
	 * 今月ランキングをリセットする
	 * 
	 * 月初のバッチ処理で実行
	 * 
	 * 初期化されたRankingBeanの配列をセット
	 * 今月ランキングテーブルを空に
	 */
	public void resetThisRanking() {
		
		// RankingBeanを生成
		RankingBean[] ranking = new RankingBean[100];
		for (int i=0; i<100; i++) {
			ranking[i] = new RankingBean();
		}
		
		// 初期化されたRankingBeanの配列をセット
		setRanking(ranking);
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// 今月ランキングテーブルを空に
			ps = db.prepareStatement("DELETE FROM thisranking");
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (ps != null) {ps.close();}
				if (db != null) {db.close();}
			} catch (Exception e) {}
		}
	}
	
	/**
	 * 評価する 評価をキャンセルする
	 * Good を押したとき キャンセルしたとき
	 * 投稿ユーザーが鍵つきなら評価しない
	 * 
	 * @param postId 投稿ID
	 * @param cancel キャンセル
	 */
	@RemoteMethod
	public static void evaluatePost(int postId, boolean cancel) {
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// 投稿テーブルから投稿IDで投稿ユーザーIDを取得し
			// ユーザーテーブルから投稿ユーザーIDで鍵つきフラグを取得
			ps = db.prepareStatement("SELECT islocked FROM users WHERE userid = " +
			  "(SELECT userid FROM posts WHERE postid = ?)");
			ps.setInt(1, postId);
			rs = ps.executeQuery();
			
			// 投稿ユーザーが鍵なしなら評価する
			if (rs.next() && !rs.getBoolean("islocked")) {
				
				// キャンセル
				if (cancel) {
					// 今月ランキングテーブル
					// 指定投稿IDがなければ、なにもしない
					// 指定投稿IDがあれば、期間高評価数を1減らす
					ps = db.prepareStatement("UPDATE thisranking SET good = good - 1 " +
					  "WHERE postid = ?");
				} else {
					// 今月ランキングテーブル
					// 指定投稿IDがなければ、追加して期間高評価数を1にする
					// 指定投稿IDがあれば、期間高評価数を1増やす
					ps = db.prepareStatement("INSERT INTO thisranking(postid, good) VALUES(?, 1) " +
					  "ON DUPLICATE KEY UPDATE good = good + 1");
				}
				ps.setInt(1, postId);
				ps.executeUpdate();
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (rs != null) {rs.close();}
				if (ps != null) {ps.close();}
				if (db != null) {db.close();}
			} catch (Exception e) {}
		}
	}
	
	/**
	 * 期間高評価数の多い投稿IDを100個まで取得する
	 * 
	 * @return 投稿IDと期間高評価数 リスト
	 */
	private ThisRankingSet get100Post() {
		
		// 戻り値
		ThisRankingSet thisRankingSet = new ThisRankingSet();
		
		// 投稿IDリスト
		ArrayList<Integer> idList = new ArrayList<Integer>();
		
		// 期間高評価数リスト
		ArrayList<Integer> goodList = new ArrayList<Integer>();
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// 今月ランキングテーブルから期間高評価数の多い投稿IDを100件まで取得
			ps = db.prepareStatement("SELECT postid, good FROM thisranking " +
			  "WHERE good > 0 ORDER BY good DESC LIMIT 100");
			rs = ps.executeQuery();
			
			while (rs.next()) {
				
				// 投稿IDリストに投稿IDをセット
				idList.add(rs.getInt("postid"));
				
				// 期間高評価数リストに期間高評価数をセット
				goodList.add(rs.getInt("good"));
			}
			
			// 戻り値にセット
			thisRankingSet.idList   = idList;
			thisRankingSet.goodList = goodList;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (rs != null) {rs.close();}
				if (ps != null) {ps.close();}
				if (db != null) {db.close();}
			} catch (Exception e) {}
		}
		return thisRankingSet;
	}
}

/**
 * 投稿IDと期間高評価数リスト
 */
class ThisRankingSet {
	public ArrayList<Integer> idList;
	public ArrayList<Integer> goodList;
}

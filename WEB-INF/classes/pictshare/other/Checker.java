package pictshare.other;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * 文字列をチェックする
 */
public class Checker {
	
	// 使用できない文字のブラックリスト
	private static final String[] BLACK = {
		"&", "\\",
		"うんこ","うんち"
	};
	
	// 画像ファイルの拡張子リスト
	private static final String[] EXTENSION = {
		"png", "jpg", "jpeg", "gif"
	};
	
	/**
	 * 文字列が空かチェックする
	 * 
	 * @param strVal チェックする文字列
	 * @return 空なら、"NULL!"という文字列 空でないなら、そのままの文字列
	 */
	public static String nullFilter(String strVal) {
		if (strVal == null) {
			return "NULL!";
		}
		
		strVal = strVal.trim();
		if (strVal.length() == 0) {
			return "NULL!";
		}
		
		return strVal;
	}
	
	/**
	 * 文字列をエスケープする
	 * 
	 * @param strVal エスケープする文字列
	 * @return エスケープした文字列
	 */
	public static String sanitize(String strVal) {
		strVal = nullFilter(strVal);
		
		strVal = strVal.replaceAll("&",  "&amp;")
		               .replaceAll("<",  "&lt;")
		               .replaceAll(">",  "&gt;")
		               .replaceAll("\"", "&quot;")
		               .replaceAll("'",  "&#39;");
		return strVal;
	}
	
	/**
	 * 文字列が英数字のみかチェックする
	 * 
	 * @param strVal チェックする文字列
	 * @return 英数字のみならtrue
	 */
	public static boolean isAlnum(String strVal) {
		strVal = nullFilter(strVal);
		
		String regex = "^[a-zA-Z0-9]+$";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(strVal);
		
		return m.find();
	}
	
	/**
	 * メールアドレスの形式かチェックする
	 * 
	 * @param strVal チェックする文字列
	 * @return メールアドレスの形式ならtrue
	 */
	public static boolean isMail(String strVal) {
		strVal = nullFilter(strVal);
		
		// 少し厳密にチェック
		// @ 直前はピリオド以外
		// @ 以降にピリオド 1つ以上
		// @ 以降は英大文字不可
		// 最後のピリオド以降は英小文字のみ
		// String regex = "^([a-zA-Z0-9_\\-]+)(\\.[a-zA-Z0-9_\\-]+)*@([a-z0-9\\-]+\\.)+[a-z]+$";
		
		// ゆるいチェック
		// @ が 1つ以上
		// 英数字、 @ 、アンダーバー、ハイフン、ピリオドのみ
		String regex = "^[a-zA-Z0-9@_\\-\\.]*@[a-zA-Z0-9@_\\-\\.]*$";
		
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(strVal);
		
		return m.find();
	}
	
	/**
	 * 使用できない文字が含まれているかチェックする
	 * 
	 * @param strVal チェックする文字列
	 * @return 使用できない文字
	 */
	public static String containsBlack(String strVal) {
		strVal = nullFilter(strVal);
		
		for (String b: BLACK) {
			if (strVal.contains(b)) {
				if (b.equals("&")) {
					return "「&amp;」、「&lt;」、「&gt;」、「&quot;」、「&#39;」";
				} else if (b.equals("\\")) {
					return "「\\」";
				}
				return "「" + b + "」";
			}
		}
		
		return null;
	}
	
	/**
	 * 絵文字が含まれているかチェックする
	 * 
	 * @param strVal チェックする文字列
	 * @return 使用できない絵文字
	 */
	public static String containsSymbol(String strVal) {
		strVal = nullFilter(strVal);
		
		Pattern p = Pattern.compile("[^\\u0000-\\uFFFF]");
		Matcher m = p.matcher(strVal);
		
		// 絵文字がある
		if (m.find()) {
			return m.group();
		}
		
		return null;
	}
	
	/**
	 * 画像の拡張子をチェックする
	 * 
	 * @param strVal チェックする文字列 先頭に . がついている
	 * @return 画像の拡張子リストにあるならtrue
	 */
	public static boolean checkExtension(String strVal) {
		strVal = nullFilter(strVal);
		
		// 先頭の . をとる
		strVal = strVal.substring(1);
		
		// 小文字にする
		strVal = strVal.toLowerCase();
		
		for (String ext: EXTENSION) {
			if (strVal.equals(ext)) {
				return true;
			}
		}
		
		return false;
	}
}

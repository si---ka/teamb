package pictshare.other;

/**
 * メッシュコード
 */
public class MeshCode {
	
	/**
	 * メッシュコードを計算する
	 * 
	 * @param lat 緯度
	 * @param lng 経度
	 * @return メッシュコード
	 */
	public static int get(float lat, float lng) {
		
		// 日本の東西南北の端を超えていたら 0 を返す
		if ((lat<20)||(lat>46)||(lng<121)||(lng>154)) {
			return 0;
		}
		
		int p, u, q, v, r, w;
		float a, f, g;
		
		lat *= 60;
		p = (int)lat/40;
		a = lat%40;
		q = (int)a/5;
		r = (int)(a%5*60)/30;
		
		lng -= 100;
		u = (int)lng;
		f = (lng - u)*60;
		v = (int)(f/7.5);
		w = (int)(f%7.5*60)/45;
		
		return p*1000000+u*10000+q*1000+v*100+r*10+w;
	}
}

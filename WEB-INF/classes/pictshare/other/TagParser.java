package pictshare.other;

import java.util.ArrayList;
import java.util.regex.Pattern;

import pictshare.TagBean;

/**
 * タグを判定する
 */
public class TagParser {
	
	/**
	 * 文字列のタグを判定する
	 * '#' から始まる文字列をタグとみなす
	 * 空白文字があれば、そこで切る
	 * 
	 * @param strVal タグを判定する文字列
	 * @return タグのリスト
	 */
	public static ArrayList<String> parse(String strVal) {
		
		// 戻り値 タグのリスト
		ArrayList<String> tagList = new ArrayList<String>();
		
		// '#' で分割
		String[] array = strVal.split("#");
		
		for (int i=1; i<array.length; i++) {
			String str = array[i];
			
			// 全角空白を半角空白に置換
			str = str.replaceAll("　", " ");
			
			// 空白文字で分割
			Pattern p = Pattern.compile("\\s");
			String[] tag = p.split(str, 2);
			
			// null ならスキップ
			tag[0] = Checker.nullFilter(tag[0]);
			if (tag[0].equals("NULL!")) {
				continue;
			}
			
			// タグのリストに追加
			tagList.add(tag[0]);
		}
		
		return tagList;
	}
	
	/**
	 * 文字列のタグにリンクをつける
	 * 
	 * @param strVal  リンクをつける文字列
	 * @return リンクをつけた文字列
	 */
	public static String setTagLink(String strVal) {
		
		// '#' で分割
		String[] array = strVal.split("#");
		
		for (int i=1; i<array.length; i++) {
			String str = array[i];
			
			// 全角空白を半角空白に置換
			str = str.replaceAll("　", " ");
			
			// 空白文字で分割
			Pattern p = Pattern.compile("\\s");
			String[] tag = p.split(str, 2);
			
			// null ならスキップ
			tag[0] = Checker.nullFilter(tag[0]);
			if (tag[0].equals("NULL!")) {
				continue;
			}
			
			// タグIDを取得する
			// 取得できないならスキップ
			int tagId = TagBean.getTagIdByTagName(tag[0]);
			if (tagId == 0) {
				continue;
			}
			
			// リンクをつける
			array[i] = "<a href=\"/teamb/pictshare/searchtag?id=" + tagId+ "\">#" + tag[0] + "</a>";
			
			// 空白以降を結合
			if (tag.length > 1) {
				array[i] += " " + tag[1];
			}
		}
		
		// 結合
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<array.length; i++) {
			sb.append(array[i]);
		}
		return sb.toString();
	}
}

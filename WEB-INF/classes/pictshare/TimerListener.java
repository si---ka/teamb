package pictshare;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * tomcat 起動時に一緒に起動する
 * 日付が変わるときにバッチ処理を行う
 */
public class TimerListener implements ServletContextListener {
	
	/**
	 * タイマーの起動時間
	 * 24:00:00
	 */
	private static final int HOUR   = 24;
	private static final int MINUTE = 0;
	private static final int SECOND = 0;
	
	/**
	 * タイマー
	 */
	private Timer timer = null;
	
	/**
	 * 初期化処理
	 * 
	 * 全ユーザー共通の、
	 * 今月ランキングとおすすめタグを取得しておく
	 */
	public void contextInitialized(ServletContextEvent event) {
		
		// アプリケーションスコープの変数 sc を取得
		ServletContext sc = event.getServletContext();
		
		// 今月ランキングの生成
		ThisRankingBean thisRanking = new ThisRankingBean();
		
		// 今月ランキングを設定する
		thisRanking.setThisRanking();
		
		// 今月ランキングをアプリケーションスコープに保存
		sc.setAttribute("thisRanking", thisRanking);
		
		// おすすめタグを取得する
		TagBean[] recommendedTag = TagBean.getRecommendedTag();
		
		// おすすめタグをアプリケーションスコープに保存
		sc.setAttribute("recommendedTag", recommendedTag);
		
		// タイマーの生成
		timer=new Timer(true);
		
		// バッチ処理の生成
		DailyTask dailyTask = new DailyTask(event.getServletContext());
		
		// タイマーの起動時間を設定する
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, HOUR);
		calendar.set(Calendar.MINUTE, MINUTE);
		calendar.set(Calendar.SECOND, SECOND);
		
		// タイマーに実行したいジョブを設定
		timer.schedule(dailyTask, calendar.getTime(),
		  1*24*60*60*1000);
	}
	
	/**
	 * 終了時処理
	 */
	public void contextDestroyed(ServletContextEvent event) {
		// タイマーを止める
		timer.cancel();
	}
}

/**
 * 毎日する処理
 * 今月ランキングとおすすめタグを更新する
 * 
 * 月初には過去ランキングを設定して
 * 今月ランキングをリセットする
 */
class DailyTask extends TimerTask {
	
	private ServletContext sc = null;
	
	/**
	 * コンストラクタ
	 */
	public DailyTask(ServletContext sc) {
		this.sc = sc;
	}
	
	/**
	 * 処理
	 */
	@Override
	public void run() {
		
		// 月初か判定
		Calendar cal = Calendar.getInstance();
		int date = cal.get(Calendar.DATE);
		if (date == 1) {
			
			// 先月の年と月を取得
			cal.add(Calendar.MONTH, -1);
			int year  = cal.get(Calendar.YEAR);      // 年
			int month = cal.get(Calendar.MONTH) + 1; // 月
			
			// 今月ランキングの取得
			ThisRankingBean thisRanking = (ThisRankingBean)sc.getAttribute("thisRanking");
			
			// 今月ランキングを設定する
			thisRanking.setThisRanking();
			
			//過去ランキングを設定する
			PastRankingBean.setPastRanking(year, month, thisRanking);
			
			// 今月ランキングをリセットする
			thisRanking.resetThisRanking();
		}
		
		// 月初でない
		else {
			
			// 今月ランキングの生成
			ThisRankingBean thisRanking = new ThisRankingBean();
			
			// 今月ランキングを設定する
			thisRanking.setThisRanking();
			
			// 今月ランキングをアプリケーションスコープに保存
			sc.setAttribute("thisRanking", thisRanking);
		}
		
		// 期間タグ使用回数を計算する
		TagBean.calculate();
		
		// おすすめタグを取得する
		TagBean[] recommendedTag = TagBean.getRecommendedTag();
		
		// おすすめタグをアプリケーションスコープに保存
		sc.setAttribute("recommendedTag", recommendedTag);
	}
}

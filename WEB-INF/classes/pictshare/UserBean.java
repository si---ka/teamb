package pictshare;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.directwebremoting.annotations.DataTransferObject;
import org.directwebremoting.annotations.RemoteMethod;
import org.directwebremoting.annotations.RemoteProperty;
import org.directwebremoting.annotations.RemoteProxy;
import org.directwebremoting.annotations.ScriptScope;

/**
 * ユーザー
 * 
 * ユーザーID、パスワード、メールアドレス、表示名、アイコン、
 * 紹介文、鍵つきフラグ、画面設定、初期緯度、初期経度、
 * フォローID、フォロー申請中ID、フォロワーID、フォロワー申請中IDをもつ
 */
@DataTransferObject
@RemoteProxy(scope=ScriptScope.APPLICATION)
public class UserBean implements Serializable {
	
	//フォロー状態を定数で宣言
	/**
	 * フォロー状態 待ち
	 */
	public static final byte WAIT = 1;
	/**
	 * フォロー状態 承認
	 */
	public static final byte OK   = 2;
	/**
	 * フォロー状態 拒否
	 */
	public static final byte NG   = 3;
	
	/**
	 * ユーザーID
	 */
	@RemoteProperty
	private String userId;
	public String getUserId() {return userId;}
	public void setUserId(String userId) {this.userId = userId;}
	
	/**
	 * パスワード
	 */
	@RemoteProperty
	private String password;
	public String getPassword() {return password;}
	public void setPassword(String password) {this.password = password;}
	
	/**
	 * メールアドレス
	 */
	@RemoteProperty
	private String mail;
	public String getMail() {return mail;}
	public void setMail(String mail) {this.mail = mail;}
	
	/**
	 * 表示名
	 */
	@RemoteProperty
	private String dispName;
	public String getDispName() {return dispName;}
	public void setDispName(String dispName) {this.dispName = dispName;}
	
	/**
	 * ユーザーアイコンファイル名
	 */
	@RemoteProperty
	private String iconFile;
	public String getIconFile() {return iconFile;}
	public void setIconFile(String iconFile) {this.iconFile = iconFile;}
	
	/**
	 * 紹介文
	 */
	@RemoteProperty
	private String profile;
	public String getProfile() {return profile;}
	public void setProfile(String profile) {this.profile = profile;}
	
	/**
	 * 鍵つきフラグ
	 */
	@RemoteProperty
	private boolean isLocked;
	public boolean getIsLocked() {return isLocked;}
	public void setIsLocked(boolean isLocked) {this.isLocked = isLocked;}
	
	/**
	 * 画面設定
	 */
	@RemoteProperty
	private boolean isMapMode;
	public boolean getIsMapMode() {return isMapMode;}
	public void setIsMapMode(boolean isMapMode) {this.isMapMode = isMapMode;}
	
	/**
	 * 初期緯度
	 */
	@RemoteProperty
	private float initLat;
	public float getInitLat() {return initLat;}
	public void setInitLat(float initLat) {this.initLat = initLat;}
	
	/**
	 * 初期経度
	 */
	@RemoteProperty
	private float initLng;
	public float getInitLng() {return initLng;}
	public void setInitLng(float initLng) {this.initLng = initLng;}
	
	/**
	 * フォローID
	 */
	@RemoteProperty
	private ArrayList<String> followId;
	public ArrayList<String> getFollowId() {return followId;}
	public void setFollowId(ArrayList<String> followId) {this.followId = followId;}
	
	/**
	 * フォロー申請中ID
	 */
	@RemoteProperty
	private ArrayList<String> followWaitId;
	public ArrayList<String> getFollowWaitId() {return followWaitId;}
	public void setFollowWaitId(ArrayList<String> followWaitId) {this.followWaitId = followWaitId;}
	
	/**
	 * フォロワーID
	 */
	@RemoteProperty
	private ArrayList<String> followerId;
	public ArrayList<String> getFollowerId() {return followerId;}
	public void setFollowerId(ArrayList<String> followerId) {this.followerId = followerId;}
	
	/**
	 * フォロワー申請中ID
	 */
	@RemoteProperty
	private ArrayList<String> followerWaitId;
	public ArrayList<String> getFollowerWaitId() {return followerWaitId;}
	public void setFollowerWaitId(ArrayList<String> followerWaitId) {this.followerWaitId = followerWaitId;}
	
	/**
	 * コンストラクタ
	 */
	public UserBean() {}
	
	/**
	 * IDでユーザーを取得する
	 * 
	 * データベース処理
	 * ユーザーテーブルから取得
	 * フォローテーブルからフォローIDなどを取得
	 * 
	 * @param userId 取得するユーザーのID
	 * @return ユーザー
	 */
	@RemoteMethod
	public static UserBean getUserByUserId(String userId) {
		
		// 返すユーザー nullで初期化
		UserBean user = null;
		
		// 非ログインユーザー
		if (userId == null) {
			user = new UserBean();
			user.setIconFile("def_icon.png");
			user.setInitLat(35.7100f);
			user.setInitLng(139.8107f);
			user.setFollowId      (new ArrayList<String>());
			user.setFollowWaitId  (new ArrayList<String>());
			user.setFollowerId    (new ArrayList<String>());
			user.setFollowerWaitId(new ArrayList<String>());
			return user;
		}
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// ユーザーテーブルからIDで全ての情報を取得
			// GEOMETRY型を取り出すときは X()で経度 Y()緯度
			ps = db.prepareStatement(
			  "SELECT password, mail, dispname, iconfile, profile, " +
			  "islocked, ismapmode, X(initlatlng) as lng, Y(initlatlng) as lat " +
			  "FROM users WHERE userid = ?");
			ps.setString(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				
				// UserBeanを生成
				user = new UserBean();
				
				user.setUserId   (userId);
				user.setPassword (rs.getString ("password"));
				user.setMail     (rs.getString ("mail"));
				user.setDispName (rs.getString ("dispname"));
				user.setIconFile (rs.getString ("iconfile"));
				user.setProfile  (rs.getString ("profile"));
				user.setIsLocked (rs.getBoolean("islocked"));
				user.setIsMapMode(rs.getBoolean("ismapmode"));
				user.setInitLat  (rs.getFloat  ("lat"));
				user.setInitLng  (rs.getFloat  ("lng"));
				
				// フォローID、フォロー申請中IDを取り出す
				ArrayList<String> followId     = new ArrayList<String>();
				ArrayList<String> followWaitId = new ArrayList<String>();
				ps = db.prepareStatement("SELECT followid, followstate FROM follows WHERE userid = ?");
				ps.setString(1, userId);
				rs = ps.executeQuery();
				while (rs.next()) {
					
					// 承認
					if(rs.getByte("followstate") == OK) {
						followId.add(rs.getString("followid"));
					}
					
					// 待ち
					else if(rs.getByte("followstate") == WAIT) {
						followWaitId.add(rs.getString("followid"));
					}
					
					// 拒否もフォロー申請中IDに追加しておく
					else if(rs.getByte("followstate") == NG) {
						followWaitId.add(rs.getString("followid"));
					}
				}
				user.setFollowId(followId);
				user.setFollowWaitId(followWaitId);
				
				// フォロワーID、フォロワー申請中IDを取り出す
				ArrayList<String> followerId     = new ArrayList<String>();
				ArrayList<String> followerWaitId = new ArrayList<String>();
				ps = db.prepareStatement("SELECT userid, followstate FROM follows WHERE followid = ?");
				ps.setString(1, userId);
				rs = ps.executeQuery();
				while (rs.next()) {
					
					// 承認
					if(rs.getByte("followstate") == OK) {
						followerId.add(rs.getString("userid"));
					}
					
					// 待ち
					else if(rs.getByte("followstate") == WAIT) {
						followerWaitId.add(rs.getString("userid"));
					}
				}
				
				user.setFollowerId(followerId);
				user.setFollowerWaitId(followerWaitId);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (rs != null) {rs.close();}
				if (ps != null) {ps.close();}
				if (db != null) {db.close();}
			} catch (Exception e) {}
		}
		return user;
	}
	
	/**
	 * ユーザーリストを取得する
	 * 
	 * フォローリスト、フォロワーリスト、
	 * フォローリクエスト許可・拒否画面で使う
	 * 
	 * ユーザーテーブルからユーザーID、表示名、アイコン、紹介文を取得
	 * 
	 * @param userIdList 取得するユーザーのIDのリスト
	 * @return ユーザーリスト
	 */
	@RemoteMethod
	public static ArrayList<UserBean> getUserListByUserIdList(ArrayList<String> userIdList) {
		
		// 戻り値 ユーザーリスト
		ArrayList<UserBean> userList = new ArrayList<UserBean>();
		
		// ユーザーIDの個数
		int count = userIdList.size();
		
		// ユーザーIDがないなら終了
		if (count == 0) {
			return userList;
		}
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// ユーザーテーブルをユーザーIDで検索
			// ユーザーID、表示名、アイコン、紹介文を取得
			String sql = "SELECT userid, dispname, iconfile, profile " +
			  "FROM users WHERE userid IN (?";
			// (ユーザーIDの数 - 1)だけ ? を追加
			for (int i=1; i<count; i++) {
				sql += ", ?";
			}
			sql += ")";
			ps = db.prepareStatement(sql);
			// ユーザーIDを ps にセット
			for (int i=0; i<count; i++) {
				ps.setString((i + 1), userIdList.get(i));
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				
				// UserBeanを生成
				UserBean user = new UserBean();
				
				user.setUserId  (rs.getString("userid"));
				user.setDispName(rs.getString("dispname"));
				user.setIconFile(rs.getString("iconfile"));
				user.setProfile (rs.getString("profile"));
				
				// ユーザーリストに追加
				userList.add(user);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (rs != null) {rs.close();}
				if (ps != null) {ps.close();}
				if (db != null) {db.close();}
			} catch (Exception e) {}
		}
		return userList;
	}
	
	/**
	 * パスワードを確認する
	 * 
	 * @param userId 確認するユーザーのID
	 * @param input  入力されたパスワード
	 * @return 一致したらtrue
	 */
	public static boolean authPass(String userId, String input) {
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// ユーザーテーブルからIDでパスワードを取得
			ps = db.prepareStatement("SELECT password FROM users WHERE userid = ?");
			ps.setString(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				
				// 入力値とデータベースのパスワードが
				// 一致していればtrueを返す
				if (input.equals(rs.getString("password"))) {
					return true;
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (rs != null) {rs.close();}
				if (ps != null) {ps.close();}
				if (db != null) {db.close();}
			} catch (Exception e) {}
		}
		return false;
	}
	
	/**
	 * ユーザーIDでユーザーを検索する
	 * 
	 * 検索画面でユーザーID検索を行うときにAjax経由で使う
	 * 
	 * ユーザーテーブルをユーザーIDであいまい検索
	 * 
	 * @param userId 検索するユーザーのID
	 * @return ユーザーリスト
	 */
	@RemoteMethod
	public static ArrayList<UserBean> searchUserListByUserId(String userId) {
		
		// 戻り値 ユーザーリスト
		ArrayList<UserBean> userList = new ArrayList<UserBean>();
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// ユーザーテーブルをユーザーIDであいまい検索
			// ユーザーID、表示名、アイコン、紹介文を取得
			ps = db.prepareStatement(
			  "SELECT userid, dispname, iconfile, profile " +
			  "FROM users WHERE userid LIKE ?");
			// "%"と"_"をエスケープ
			userId = userId.replaceAll("%","\\\\%").replaceAll("_","\\\\_");
			ps.setString(1, "%" + userId + "%");
			rs = ps.executeQuery();
			while (rs.next()) {
				
				// ユーザーIDが"null"(非会員)ならスキップ
				String id = rs.getString("userid");
				if (id.equals("null")) {
					continue;
				}
				
				// UserBeanを生成
				UserBean user = new UserBean();
				
				user.setUserId  (id);
				user.setDispName(rs.getString("dispname"));
				user.setIconFile(rs.getString("iconfile"));
				user.setProfile (rs.getString("profile"));
				
				// ユーザーリストに追加
				userList.add(user);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (rs != null) {rs.close();}
				if (ps != null) {ps.close();}
				if (db != null) {db.close();}
			} catch (Exception e) {}
		}
		return userList;
	}
	
	/**
	 * ユーザーIDを登録してみる
	 * 
	 * ユーザー登録時にユーザーIDが重複していないか確認するために実行する
	 * 
	 * データベース処理
	 * ユーザーテーブルにユーザーIDを登録
	 * ユーザーID以外は仮
	 * 
	 * @param userId 登録してみるユーザーのID
	 * @return 登録できたときtrue
	 */
	public static boolean tryRegisterUserId(String userId) {
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// ユーザーテーブルにユーザーIDを登録
			// ユーザーID以外は仮
			ps = db.prepareStatement(
			  "INSERT INTO users" +
			  "(userid, password, mail, dispname, initlatlng) VALUES " +
			  "(?, ' temp ', ' temp ', ' 仮 ', GeomFromText('POINT(0.0 0.0)'))");
			ps.setString(1, userId);
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			
			// 登録できなかった
			return false;
		} finally {
			try {
				if (ps != null) {ps.close();}
				if (db != null) {db.close();}
			} catch (Exception e) {}
		}
		
		// 登録できた
		return true;
	}
	
	/**
	 * ユーザーをデータベースに登録する
	 * 新規登録時のみ
	 */
	public void registerUser() {
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// ユーザーテーブルにパスワード、メールアドレス、表示名を登録
			// 初期位置はスカイツリー
			ps = db.prepareStatement(
			  "UPDATE users SET password = ?, mail = ?, dispname = ?, " +
			  "initlatlng = GeomFromText('POINT(139.8107 35.7100)') " +
			  "WHERE userid = ?");
			ps.setString(1, password);
			ps.setString(2, mail);
			ps.setString(3, dispName);
			ps.setString(4, userId);
			ps.executeUpdate();
			
			// このUserBeanにその他のデフォルト値を設定する
			setIconFile("def_icon.png");
			setProfile ("hello");
			setInitLat (35.71f);
			setInitLng (139.8107f);
			setFollowId      (new ArrayList<String>());
			setFollowWaitId  (new ArrayList<String>());
			setFollowerId    (new ArrayList<String>());
			setFollowerWaitId(new ArrayList<String>());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (ps != null) {ps.close();}
				if (db != null) {db.close();}
			} catch (Exception e) {}
		}
	}
	
	/**
	 * フォローIDを追加する
	 * 
	 * ユーザー情報閲覧画面で、フォローするボタンを押したとき
	 * 
	 * @param userId ユーザーID
	 * @param followId フォローID
	 */
	@RemoteMethod
	public static void addFollowId(String userId, String followId) {
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// フォローテーブルにユーザーID、フォローID、フォロー状態 承認 を設定
			ps = db.prepareStatement("INSERT INTO follows (userid, followid, followstate) " +
			  "VALUES (?, ?, ?)");
			ps.setString(1, userId);
			ps.setString(2, followId);
			ps.setInt   (3, OK);
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (ps != null) {ps.close();}
				if (db != null) {db.close();}
			} catch (Exception e) {}
		}
	}
	
	/**
	 * フォロー申請中IDを追加する
	 * 
	 * ユーザー情報閲覧画面で、フォロー申請を送るボタンを押したとき
	 * 
	 * @param userId ユーザーID
	 * @param followWaitId フォロー申請中ID
	 */
	@RemoteMethod
	public static void addFollowWaitId(String userId, String followWaitId) {
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// フォローテーブルにユーザーID、フォローID、フォロー状態 待ち を設定
			ps = db.prepareStatement("INSERT INTO follows (userid, followid, followstate) " +
			  "VALUES (?, ?, ?)");
			ps.setString(1, userId);
			ps.setString(2, followWaitId);
			ps.setInt   (3, WAIT);
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (ps != null) {ps.close();}
				if (db != null) {db.close();}
			} catch (Exception e) {}
		}
	}
	
	/**
	 * フォローIDから削除する
	 * 
	 * ユーザー情報閲覧画面で、フォロー解除ボタンを押したとき
	 * 
	 * @param userId ユーザーID
	 * @param followId フォローID
	 */
	@RemoteMethod
	public static void removeFollowId(String userId, String followId) {
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// フォローテーブルから指定ユーザーID、フォローIDのデータを削除
			ps = db.prepareStatement("DELETE FROM follows WHERE userid = ? AND followid = ?");
			ps.setString(1, userId);
			ps.setString(2, followId);
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (ps != null) {ps.close();}
				if (db != null) {db.close();}
			} catch (Exception e) {}
		}
	}
	
	/**
	 * フォロワー申請中IDを承認する
	 * 
	 * フォローリクエスト許可・拒否画面で、許可ボタンを押したとき
	 * 
	 * @param userId ユーザーID
	 * @param followerWaitId フォロワー申請中ID
	 */
	@RemoteMethod
	public static void approveFollowerWaitId(String userId, String followerWaitId) {
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// フォローテーブルの指定フォロワー申請中ID、ユーザーIDにフォロー状態 承認 を設定
			ps = db.prepareStatement("UPDATE follows SET followstate = ? " +
			  "WHERE userid = ? AND followid = ?");
			ps.setInt   (1, OK);
			ps.setString(2, followerWaitId);
			ps.setString(3, userId);
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (ps != null) {ps.close();}
				if (db != null) {db.close();}
			} catch (Exception e) {}
		}
	}
	
	/**
	 * フォロワー申請中IDを拒否する
	 * 
	 * フォローリクエスト許可・拒否画面で、拒否ボタンを押したとき
	 * 
	 * @param userId ユーザーID
	 * @param followerWaitId フォロワー申請中ID
	 */
	@RemoteMethod
	public static void rejectFollowerWaitId(String userId, String followerWaitId) {
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// フォローテーブルの指定フォロワー申請中ID、ユーザーIDにフォロー状態 拒否 を設定
			ps = db.prepareStatement("UPDATE follows SET followstate = ? " +
			  "WHERE userid = ? AND followid = ?");
			ps.setInt   (1, NG);
			ps.setString(2, followerWaitId);
			ps.setString(3, userId);
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (ps != null) {ps.close();}
				if (db != null) {db.close();}
			} catch (Exception e) {}
		}
	}
	
	/**
	 * アカウント設定を行う
	 * 
	 * データベース処理
	 * ユーザーテーブルにパスワード、アイコン、紹介文、
	 * 鍵つきフラグ、画面設定、初期緯度、初期経度を設定
	 */
	public void setAccount() {
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps = null;
		ResultSet rs  = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// ユーザーテーブルにパスワード、アイコン、紹介文、
			// 鍵つきフラグ、画面設定、初期緯度、初期経度を設定
			ps = db.prepareStatement(
			  "UPDATE users SET password = ?, iconfile = ?, profile = ?, " +
			  "islocked = ?, ismapmode = ?, " +
			  "initlatlng = GeomFromText(?) " +
			  "WHERE userid = ?");
			ps.setString (1, password);
			ps.setString (2, iconFile);
			ps.setString (3, profile);
			ps.setBoolean(4, isLocked);
			ps.setBoolean(5, isMapMode);
			ps.setString (6, "POINT(" + initLng + " " + initLat + ")");
			ps.setString (7, userId);
			ps.executeUpdate();
			
			// 鍵つきなら今月ランキングから投稿を削除
			if (isLocked) {
				
				// 投稿IDのリスト
				ArrayList<Integer> postIdList = new ArrayList<Integer>();
				
				// 投稿テーブルからユーザーの投稿の投稿IDを取得
				ps = db.prepareStatement("SELECT postid FROM posts WHERE userid = ?");
				ps.setString (1, userId);
				rs = ps.executeQuery();
				while (rs.next()) {
					// 投稿IDを取得してリストに追加
					postIdList.add(rs.getInt("postid"));
				}
				
				// 投稿がないなら終了
				if (postIdList.size() == 0) {
					throw new Exception("投稿0です");
				}
				
				String sql = "DELETE FROM thisranking WHERE postid IN (?";
				// (投稿IDの数 - 1)だけ ? を追加
				for (int i=1; i<postIdList.size(); i++) {
					sql += ", ?";
				}
				sql += ")";
				ps = db.prepareStatement(sql);
				
				// 投稿IDをセット
				for (int i=0; i<postIdList.size(); i++) {
					ps.setInt(i+1, postIdList.get(i));
				}
				ps.executeUpdate();
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (rs != null) {rs.close();}
				if (ps != null) {ps.close();}
				if (db != null) {db.close();}
			} catch (Exception e) {}
		}
	}
	
	/**
	 * 対象ユーザーをフォローしているか調べる
	 * 
	 * @param targetUserId フォローしているか調べる対象ユーザーのID
	 * @return フォローしていれば true
	 */
	public boolean isFollowing(String targetUserId) {
		for (String f : followId) {
			if (targetUserId.equals(f)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 対象ユーザーをフォロー申請しているか調べる
	 * 
	 * @param targetUserId フォロー申請しているか調べる対象ユーザーのID
	 * @return フォロー申請中なら true
	 */
	public boolean isFollowWaiting(String targetUserId) {
		for (String f : followWaitId) {
			if (targetUserId.equals(f)) {
				return true;
			}
		}
		return false;
	}
}

package pictshare;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.directwebremoting.annotations.DataTransferObject;
import org.directwebremoting.annotations.RemoteMethod;
import org.directwebremoting.annotations.RemoteProperty;
import org.directwebremoting.annotations.RemoteProxy;
import org.directwebremoting.annotations.ScriptScope;

import pictshare.other.Checker;

/**
 * コメント
 * 
 * コメントID、投稿ID、
 * コメントしたユーザーのユーザーID・ユーザー名・アイコン、
 * コメント文をまとめたもの
 */
@DataTransferObject
@RemoteProxy(scope = ScriptScope.APPLICATION)
public class CommentBean implements Serializable {
	
	/**
	 * コメントID
	 */
	@RemoteProperty
	private int  commentId;
	public  int  getCommentId() {return commentId;}
	public  void setCommentId(int commentId) {this.commentId = commentId;}
	
	/**
	 * 投稿ID
	 */
	@RemoteProperty
	private int  postId;
	public  int  getPostId() {return postId;}
	public  void setPostId(int postId) {this.postId = postId;}
	
	/**
	 * コメントユーザーID
	 */
	@RemoteProperty
	private String userId;
	public  String getUserId() {return userId;}
	public  void   setUserId(String userId) {this.userId = userId;}
	
	/**
	 * コメントユーザー表示名
	 */
	@RemoteProperty
	private String dispName;
	public  String getDispName() {return dispName;}
	public  void   setDispName(String dispName) {this.dispName = dispName;}
	
	/**
	 * コメントユーザーアイコン
	 */
	@RemoteProperty
	private String iconFile;
	public  String getIconFile() {return iconFile;}
	public  void   setIconFile(String iconFile) {this.iconFile = iconFile;}
	
	/**
	 * コメント文
	 */
	@RemoteProperty
	private String message;
	public  String getMessage() {return message;}
	public  void   setMessage(String message) {this.message = message;}
	
	/**
	 * コンストラクタ
	 */
	public CommentBean() {}
	
	/**
	 * 投稿IDからコメントのリストを取得する
	 * 
	 * 投稿詳細表示のときに、Ajax経由で利用する
	 * 
	 * @param postId 投稿ID
	 * @return コメントリスト
	 */
	@RemoteMethod
	public static ArrayList<CommentBean> getCommentByPostId(int postId) {
		
		// 戻り値 コメントリスト
		ArrayList<CommentBean> commentList = new ArrayList<CommentBean>();
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps  = null;
		PreparedStatement ps2 = null;
		ResultSet rs  = null;
		ResultSet rs2 = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// ユーザーIDのリスト
			ArrayList<String> uIdList = new ArrayList<String>();
			
			// コメントテーブルから投稿IDでユーザーIDを取得
			ps = db.prepareStatement(
			  "SELECT userid FROM comments WHERE postid = ? GROUP BY userid");
			// 投稿IDを ps にセット
			ps.setInt(1, postId);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				// ユーザーIDを取得してリストに追加
				uIdList.add(rs.getString("userid"));
			}
			
			// コメントが1件もないときは終了
			if (uIdList.size() == 0) {
				throw new Exception("コメント0です");
			}
			
			// コメントテーブルから投稿IDでコメントを取得
			ps = db.prepareStatement(
			  "SELECT commid, userid, comment FROM comments WHERE postid = ?");
			// 投稿IDを ps にセット
			ps.setInt(1, postId);
			
			rs = ps.executeQuery();
			
			// ユーザーテーブルからユーザーIDで
			// 表示名、アイコン、鍵つきフラグを取得
			String sql = "SELECT userid, dispname, iconfile, islocked " +
			  "FROM users WHERE userid IN (?";
			// (ユーザーIDの数 - 1)だけ ? を追加
			for (int i=1; i<uIdList.size(); i++) {
				sql += ", ?";
			}
			sql += ")";
			ps2 = db.prepareStatement(sql);
			// ユーザーIDを ps2 にセット
			for (int i=0; i<uIdList.size(); i++) {
				ps2.setString((i + 1), uIdList.get(i));
			}
			rs2 = ps2.executeQuery();
			
			// ユーザーのIDと
			// 表示名、アイコン、鍵つきフラグを連想配列に
			HashMap<String, UserData> userMap = new HashMap<String, UserData>();
			
			// ユーザーの表示名、アイコンを取得
			while (rs2.next()) {
				String id   = rs2.getString("userid");
				String name = rs2.getString("dispname");
				String icon = rs2.getString("iconfile");
				
				// 表示名、アイコンをセット
				UserData ud = new UserData(name, icon);
				
				// 連想配列に格納
				userMap.put(id, ud);
			}
			
			while (rs.next()) {
				
				// CommentBeanを生成
				CommentBean comment = new CommentBean();
				
				comment.setCommentId(rs.getInt   ("commid"));
				comment.setPostId   (postId);
				comment.setUserId   (rs.getString("userid"));
				comment.setMessage  (rs.getString("comment"));
				
				// 表示名、アイコンを連想配列から取得
				UserData ud = userMap.get(rs.getString("userid"));
				
				// 表示名、アイコンをセット
				comment.setDispName(ud.dispName);
				comment.setIconFile(ud.iconFile);
				
				// コメントリストに追加
				commentList.add(comment);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (rs2 != null) {rs2.close();}
				if (rs  != null) {rs .close();}
				if (ps2 != null) {ps2.close();}
				if (ps  != null) {ps .close();}
				if (db  != null) {db .close();}
			} catch (Exception e) {}
		}
		return commentList;
	}
	
	/**
	 * コメントをデータベースに登録する
	 * 
	 * 投稿詳細表示でコメントを入力し、
	 * コメント投稿ボタンを押したとき、Ajax経由で実行される
	 * 
	 * @param postId  投稿ID
	 * @param userId  コメントユーザーID
	 * @param message コメント文
	 * @return エラーメッセージ
	 */
	@RemoteMethod
	public static String registerComment(int postId, String userId, String message) {
		
		// コメント文のチェック
		message = Checker.sanitize(message);
		// 入力されていない
		if (message.equals("NULL!")) {
			return "コメントを入力してください";
		} else {
			// 使用できない文字がある
			String black = Checker.containsBlack(message);
			if (black != null) {
				return "コメントに使用できない文字" + black + "が含まれています";
			} else {
				
				// 絵文字がある
				String symbol = Checker.containsSymbol(message);
				if (symbol != null) {
					return symbol + "などの絵文字は使用できません";
				}
				
				// 140文字を超えている
				else if (message.length()>140) {
					return "コメントは140文字以内にしてください";
				}
			}
		}
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps  = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// コメントテーブルに投稿ID、コメントユーザーID、コメント文を登録
			ps = db.prepareStatement("INSERT INTO comments(postId, userId, comment)" +
			  " VALUES(?, ?, ?)");
			ps.setInt   (1, postId);
			ps.setString(2, userId);
			ps.setString(3, message);
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (ps  != null) {ps .close();}
				if (db  != null) {db .close();}
			} catch (Exception e) {}
		}
		return "";
	}
}

/**
 * ユーザーの表示名、アイコンを持つだけ
 */
class UserData {
	public String  dispName;
	public String  iconFile;
	public UserData(String name, String icon) {
		dispName = name;
		iconFile = icon;
	}
}

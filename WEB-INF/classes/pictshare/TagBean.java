package pictshare;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.directwebremoting.annotations.DataTransferObject;
import org.directwebremoting.annotations.RemoteMethod;
import org.directwebremoting.annotations.RemoteProperty;
import org.directwebremoting.annotations.RemoteProxy;
import org.directwebremoting.annotations.ScriptScope;

/**
 * タグ
 * 
 * IDとタグ名をもつ
 */
@DataTransferObject
@RemoteProxy(scope = ScriptScope.APPLICATION)
public class TagBean implements Serializable {
	
	/**
	 * タグID
	 */
	@RemoteProperty
	private int  tagId;
	public  int  getTagId() {return tagId;}
	public  void setTagId(int tagId) {this.tagId = tagId;}
	
	/**
	 * タグ
	 */
	@RemoteProperty
	private String tagName;
	public  String getTagName() {return tagName;}
	public  void   setTagName(String tagName) {this.tagName = tagName;}
	
	/**
	 * コンストラクタ
	 */
	public TagBean() {}
	
	/**
	 * タグ名からタグIDを取得する
	 * 
	 * 検索画面でタグ検索をしたときにAjax経由で使う
	 * getOrAddTagIdByTagName でも使う
	 * 
	 * タグテーブルからタグ名でIDを取得
	 * 
	 * @param tagName タグ
	 * @return タグID なければ 0
	 */
	@RemoteMethod
	public static int getTagIdByTagName(String tagName) {
		
		// 戻り値
		int id = 0;
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// タグテーブルからタグ名でIDを取得
			ps = db.prepareStatement("SELECT tagid FROM tags WHERE tag = ?");
			ps.setString(1, tagName);
			rs = ps.executeQuery();
			if (rs.next()) {
				id = rs.getInt("tagid");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (rs != null) {rs.close();}
				if (ps != null) {ps.close();}
				if (db != null) {db.close();}
			} catch (Exception e) {}
		}
		
		return id;
	}
	
	/**
	 * タグIDを取得する
	 * なければタグを登録する
	 * 
	 * 写真投稿時と、投稿編集時に使う
	 * 
	 * getTagIdByTagName でタグIDがなければ、
	 * タグテーブルとおすすめタグテーブルに登録する
	 * 
	 * @param tagName タグ
	 * @return タグID
	 */
	public static int getOrAddTagIdByTagName(String tagName) {
		
		// 戻り値
		int id = getTagIdByTagName(tagName);
		
		// タグIDがなければタグを登録する
		if (id == 0) {
			
			// データベース処理
			Connection db = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				Context context = new InitialContext();
				DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
				db = ds.getConnection();
				
				// タグテーブルにタグを登録
				// 自動生成のタグIDを取得
				ps = db.prepareStatement("INSERT INTO tags(tag) VALUES(?)",
				  PreparedStatement.RETURN_GENERATED_KEYS);
				ps.setString(1, tagName);
				ps.executeUpdate();
				
				// 自動生成のタグIDを取得
				rs = ps.getGeneratedKeys();
				rs.next();
				id = rs.getInt(1);
				
				// おすすめタグテーブルにタグIDを登録
				ps = db.prepareStatement("INSERT INTO recommendedtags(tagid) VALUES(?)");
				ps.setInt(1, id);
				ps.executeUpdate();
			} catch (Exception e) {
				System.out.println(e.getMessage());
			} finally {
				try {
					if (rs != null) {rs.close();}
					if (ps != null) {ps.close();}
					if (db != null) {db.close();}
				} catch (Exception e) {}
			}
		}
		
		return id;
	}
	
	/**
	 * タグIDでタグを取得する
	 * 
	 * 投稿文のタグのリンクをクリックした時に使う
	 * 
	 * タグテーブルからタグIDでタグ名を取得
	 * 
	 * @param tagId タグID
	 * @return タグ
	 */
	public static String getTagNameByTagId(int tagId) {
		
		// 戻り値
		String tagName = "";
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// タグテーブルからタグIDでタグ名を取得
			ps = db.prepareStatement("SELECT tag FROM tags WHERE tagid = ?");
			ps.setInt(1, tagId);
			rs = ps.executeQuery();
			if (rs.next()) {
				tagName = rs.getString("tag");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (rs != null) {rs.close();}
				if (ps != null) {ps.close();}
				if (db != null) {db.close();}
			} catch (Exception e) {}
		}
		
		return tagName;
	}
	
	/**
	 * 使用回数を1増やす
	 * 
	 * 写真投稿時に行う
	 * 
	 * 曜日から数字を決めて、おすすめタグテーブルの回数を1増やす
	 * 
	 * @param tagId タグID
	 */
	public static void increaseCountByTagId(int tagId) {
		
		// 現在の曜日の数字
		int number = decideNumber();
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// おすすめタグテーブルの指定タグIDの使用回数を1増やす
			String sql = "UPDATE recommendedtags SET count" + number +
			  " = count" + number + " + 1 WHERE tagid = ?";
			ps = db.prepareStatement(sql);
			ps.setInt(1, tagId);
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (ps != null) {ps.close();}
				if (db != null) {db.close();}
			} catch (Exception e) {}
		}
	}
	
	/**
	 * 使用回数を1増やす
	 * 
	 * 投稿編集時、投稿日時が1週間以内だった場合に使う
	 * 
	 * 曜日から数字を決めて、おすすめタグテーブルの回数を1増やす
	 * 
	 * @param postDate 投稿日時
	 * @param tagId    タグID
	 */
	public static void increaseCountByTagId(Timestamp postDate, int tagId) {
		
		// 投稿日時の曜日の数字
		int number = decideNumber(postDate);
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// おすすめタグテーブルの指定タグIDの使用回数を1増やす
			String sql = "UPDATE recommendedtags SET count" + number +
			  " = count" + number + " + 1 WHERE tagid = ?";
			ps = db.prepareStatement(sql);
			ps.setInt(1, tagId);
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (ps != null) {ps.close();}
				if (db != null) {db.close();}
			} catch (Exception e) {}
		}
	}
	
	/**
	 * 使用回数を1減らす
	 * 
	 * 投稿編集時と投稿削除時に、投稿日時が1週間以内だった場合に使う
	 * 
	 * 曜日から数字を決めて、おすすめタグテーブルの回数を1減らす
	 * 
	 * @param postDate 投稿日時
	 * @param tagId    タグID
	 */
	public static void decreaseCountByTagId(Timestamp postDate, int tagId) {
		
		// 投稿日時の曜日の数字
		int number = decideNumber(postDate);
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// おすすめタグテーブルの指定タグIDの使用回数を1減らす
			String sql = "UPDATE recommendedtags SET count" + number +
			  " = count" + number + " - 1 WHERE tagid = ?";
			ps = db.prepareStatement(sql);
			ps.setInt(1, tagId);
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (ps != null) {ps.close();}
				if (db != null) {db.close();}
			} catch (Exception e) {}
		}
	}
	
	/**
	 * 期間タグ使用回数を計算する
	 * 
	 * 日付が変わるときのバッチ処理の中で実行される
	 * 直近6日の使用回数の合計を計算する
	 */
	public static void calculate() {
		
		// 現在の曜日の数字
		int number = decideNumber();
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// おすすめタグテーブル
			// 1週間前の使用回数を0にする
			String sql = "UPDATE recommendedtags SET count" + number + " = 0";
			ps = db.prepareStatement(sql);
			ps.executeUpdate();
			// 合計を計算する
			ps = db.prepareStatement("UPDATE recommendedtags SET countsum = " +
			  "count0 + count1 + count2 + count3 + count4 + count5 + count6");
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (ps != null) {ps.close();}
				if (db != null) {db.close();}
			} catch (Exception e) {}
		}
	}
	
	/**
	 * おすすめタグを取得する
	 * 
	 * tomcat 起動時と、日付が変わるときのバッチ処理の中で実行される
	 * 
	 * おすすめタグテーブルからタグIDを期間合計の多い順に3つ取得
	 * タグテーブルからタグIDでタグ名を取得
	 * 
	 * @return おすすめタグ
	 */
	public static TagBean[] getRecommendedTag() {
		
		// TagBean を3つ生成
		TagBean[] tag = new TagBean[3];
		for (int i=0; i<3; i++) {
			tag[i] = new TagBean();
		}
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps = null;
		ResultSet rs  = null;
		ResultSet rs2 = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// おすすめタグテーブルからタグIDを期間合計の多い順に3つ取得
			ps = db.prepareStatement("SELECT tagid FROM recommendedtags " +
			  "ORDER BY countsum DESC LIMIT 3");
			rs = ps.executeQuery();
			int i = 0;
			while (rs.next()) {
				
				// タグテーブルからタグIDでタグ名を取得
				ps = db.prepareStatement("SELECT tag FROM tags WHERE tagid = ?");
				ps.setInt(1, rs.getInt("tagid"));
				rs2 = ps.executeQuery();
				rs2.next();
				
				// タグIDとタグ名をセット
				tag[i].setTagId  (rs .getInt   ("tagid"));
				tag[i].setTagName(rs2.getString("tag"));
				
				i++;
			}
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (rs2 != null) {rs2.close();}
				if (rs  != null) {rs.close();}
				if (ps  != null) {ps.close();}
				if (db  != null) {db.close();}
			} catch (Exception e) {}
		}
		
		return tag;
	}
	
	/**
	 * 数字を決める
	 * 0～6 のどれかを曜日で決める
	 */
	private static int decideNumber() {
		
		// 現在日時の取得
		Calendar calendar = Calendar.getInstance();
		
		// 曜日の数字を7で割った余り
		int number = (calendar.get(Calendar.DAY_OF_WEEK) % 7);
		
		return number;
	}
	
	/**
	 * 数字を決める
	 * 0～6 のどれかを曜日で決める
	 * 
	 * @param postDate 投稿日時
	 */
	private static int decideNumber(Timestamp postDate) {
		
		// カレンダーの取得
		Calendar calendar = Calendar.getInstance();
		
		// 投稿日時をカレンダーにセット
		calendar.setTimeInMillis(postDate.getTime());
		
		// 曜日の数字を7で割った余り
		int number = (calendar.get(Calendar.DAY_OF_WEEK) % 7);
		
		return number;
	}
}

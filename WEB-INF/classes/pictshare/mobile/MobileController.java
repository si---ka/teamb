package pictshare.mobile;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import pictshare.PastRankingBean;
import pictshare.PostBean;
import pictshare.TagBean;
import pictshare.ThisRankingBean;
import pictshare.UserBean;
import pictshare.other.Checker;
import pictshare.other.MeshCode;
import pictshare.other.TagParser;

/**
 * スマホ用コントローラ
 * AjaxじゃないHTTP通信を全て処理する
 */
@MultipartConfig
public class MobileController extends HttpServlet {
	
	/**
	 * GETの処理
	 * doPostに転送する
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException
	{
		doPost(request, response);
	}
	
	/**
	 * POSTの処理（GETも）
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException
	{
		request.setCharacterEncoding("UTF-8");
		
		// 拡張パスを取得
		String path = request.getPathInfo();
		
		// 念のため遷移先をindexで初期化
		String dispatcherURL = "/m/index.jsp";
		
		// 拡張パスがない
		// セッションにユーザーがあるか確認、あればホームに遷移する
		// なければ login.jsp に遷移する
		if (path == null || path.equals("") || path.equals("/")) {
			dispatcherURL = checkSession(request);
		}
		
		// PC版へ
		// /teamb/pictsharemobile/pc
		else if (path.equals("/pc")) {
			dispatcherURL = pc(request);
		}
		
		// ユーザー登録
		// /teamb/pictsharemobile/register
		else if (path.equals("/register")) {
			dispatcherURL = registerUser(request);
		}
		
		// ログイン
		// /teamb/pictsharemobile/login
		else if (path.equals("/login")) {
			
			// メニューのホームの色を変える
			request.setAttribute("selectMenu", "home");
			
			dispatcherURL = login(request);
		}
		
		// ログアウト
		// /teamb/pictsharemobile/logout
		else if (path.equals("/logout")) {
			dispatcherURL = logout(request);
		}
		
		// アカウント設定画面
		// /teamb/pictsharemobile/setting
		else if (path.equals("/setting")) {
			
			// メニューの設定の色を変える
			request.setAttribute("selectMenu", "setting");
			
			dispatcherURL = setting(request);
		}
		
		// アカウント設定を行う
		// /teamb/pictsharemobile/settingdone
		else if (path.equals("/settingdone")) {
			
			// メニューのホームの色を変える
			request.setAttribute("selectMenu", "home");
			
			dispatcherURL = settingDone(request);
		}
		
		// ホーム
		// /teamb/pictsharemobile/home
		else if (path.equals("/home")) {
			
			// メニューのホームの色を変える
			request.setAttribute("selectMenu", "home");
			
			dispatcherURL = home(request);
		}
		
		// タイムライン地図版
		// /teamb/pictsharemobile/map
		else if (path.equals("/map")) {
			
			// メニューのタイムラインの色を変える
			request.setAttribute("selectMenu", "timeline");
			
			dispatcherURL = map(request);
		}
		
		// タイムライン友達版
		// /teamb/pictsharemobile/friend
		else if (path.equals("/friend")) {
			
			// メニューのタイムラインの色を変える
			request.setAttribute("selectMenu", "timeline");
			
			dispatcherURL = friend(request);
		}
		
		// 写真投稿を行う
		// /teamb/pictsharemobile/postdone
		else if (path.equals("/postdone")) {
			postDone(request, response);
			return;
		}
		
		// 投稿編集・削除画面
		// /teamb/pictsharemobile/edit
		else if (path.equals("/edit")) {
			
			// メニューのアカウントの色を変える
			request.setAttribute("selectMenu", "account");
			
			dispatcherURL = edit(request);
		}
		
		// 投稿を編集する
		// /teamb/pictsharemobile/editdone
		else if (path.equals("/editdone")) {
			
			// メニューのアカウントの色を変える
			request.setAttribute("selectMenu", "account");
			
			dispatcherURL = editDone(request);
		}
		
		// 投稿を削除する
		// /teamb/pictsharemobile/deletedone
		else if (path.equals("/deletedone")) {
			
			// メニューのアカウントの色を変える
			request.setAttribute("selectMenu", "account");
			
			dispatcherURL = deleteDone(request);
		}
		
		// ランキング
		// /teamb/pictsharemobile/ranking?y=2015&m=1&s=1
		else if (path.startsWith("/ranking")) {
			
			// メニューのランキングの色を変える
			request.setAttribute("selectMenu", "ranking");
			
			dispatcherURL = showRanking(request);
		}
		
		// 検索
		// /teamb/pictsharemobile/search
		else if (path.equals("/search")) {
			
			// メニューの検索の色を変える
			request.setAttribute("selectMenu", "search");
			
			dispatcherURL = "/m/jsp/search.jsp";
		}
		
		// ユーザー情報閲覧
		// フォロー/フォロワーリスト
		// /teamb/pictsharemobile/user/Piyohiko
		// /teamb/pictsharemobile/user/Piyohiko/follow
		// /teamb/pictsharemobile/user/Piyohiko/follower
		else if (path.startsWith("/user/")) {
			
			// メニューの色を変えない
			request.removeAttribute("selectMenu");
			
			dispatcherURL = showUserDetail(request);
		}
		
		// フォローリクエスト許可・拒否画面
		// /teamb/pictsharemobile/followrequest
		else if (path.equals("/followrequest")) {
			
			// メニューのアカウントの色を変える
			request.setAttribute("selectMenu", "account");
			
			dispatcherURL = followRequest(request);
		}
		
		// 拡張パスが想定外
		// login.jspに遷移
		else {
			dispatcherURL = errorURL(request);
		}
		
		request.getRequestDispatcher(dispatcherURL).forward(request, response);
	}
	
	/**
	 * PC版へ
	 * 
	 * @return 遷移先URL
	 */
	private String pc(HttpServletRequest request) {
		
		// セッションの取得
		HttpSession session = request.getSession();
		
		// スマホ用設定を no にする
		session.setAttribute("mobile", "no");
		
		// PC版の login.jsp に遷移する
		return "/jsp/login.jsp";
	}
	
	/**
	 * セッションにユーザーがあるか確認、あればホームに遷移する
	 * なければ login.jsp に遷移する
	 * 
	 * @return 遷移先URL
	 */
	private String checkSession(HttpServletRequest request) {
		
		// セッションの取得
		HttpSession session = request.getSession();
		
		// セッションからユーザーを取得
		UserBean user = (UserBean)session.getAttribute("user");
		
		// セッションにユーザーがあるならホームに遷移する
		if (user != null && user.getUserId() != null) {
			
			// メニューのホームの色を変える
			request.setAttribute("selectMenu", "home");
			
			return home(request);
		}
		
		// セッションにユーザーがないならlogin.jsp に遷移する
		session.removeAttribute("user");
		return "/m/jsp/login.jsp";
	}
	
	/**
	 * ユーザー登録
	 * /teamb/pictsharemobile/register
	 * 
	 * 表示名、ユーザーID、パスワード、メールアドレスをチェック
	 * ユーザーIDをDBに登録してみて重複チェック
	 * データベースに登録
	 * セッションにユーザーをセット
	 * タイムライン友達版に遷移
	 * 
	 * @return 遷移先URL
	 */
	private String registerUser(HttpServletRequest request) {
		
		boolean isError  = false; // エラー
		String  errorMsg = "";    // エラーメッセージ
		
		// IDを取得
		String id = request.getParameter("UserID");
		
		// パスワードを取得
		String pass = request.getParameter("PASS");
		
		// メールアドレスを取得
		String mail = request.getParameter("Mail");
		
		// 表示名を取得
		String dispName = request.getParameter("UserName");
		
		// 表示名のチェック
		dispName = Checker.sanitize(dispName);
		// 入力されていない
		if (dispName.equals("NULL!")) {
			dispName = "";
			isError = true;
			errorMsg += "ユーザー名を入力してください<br />";
		} else {
			// 使用できない文字がある
			String black = Checker.containsBlack(dispName);
			if (black != null) {
				isError = true;
				errorMsg += "ユーザー名に使用できない文字" + black + "が含まれています<br />";
			} else {
				
				// 絵文字がある
				String symbol = Checker.containsSymbol(dispName);
				if (symbol != null) {
					isError = true;
					errorMsg += symbol + "などの絵文字は使用できません";
				}
				
				// 100文字を超えている
				else if (dispName.length()>100) {
					isError = true;
					errorMsg += "ユーザー名は100文字以内にしてください<br />";
				}
			}
		}
		// IDのチェック
		id = Checker.sanitize(id);
		// 入力されていない
		if (id.equals("NULL!")) {
			id = "";
			isError = true;
			errorMsg += "ユーザーIDを入力してください<br />";
		}
		// 英数字のみでない
		else if (Checker.isAlnum(id) == false) {
			isError = true;
			errorMsg += "ユーザーIDに使用可能な文字は英数字のみです<br />";
		}
		// 20文字を超えている
		else if (id.length()>20) {
			isError = true;
			errorMsg += "ユーザーIDは20文字以内にしてください<br />";
		}
		
		// パスワードのチェック
		pass = Checker.sanitize(pass);
		// 入力されていない
		if (pass.equals("NULL!")) {
			isError = true;
			errorMsg += "パスワードを入力してください<br />";
		}
		// 英数字のみでない
		else if (Checker.isAlnum(pass) == false) {
			isError = true;
			errorMsg += "パスワードに使用可能な文字は英数字のみです<br />";
		}
		// 20文字を超えている
		else if (pass.length()>20) {
			isError = true;
			errorMsg += "パスワードは20文字以内にしてください<br />";
		}
		
		// メールアドレスのチェック
		mail = Checker.sanitize(mail);
		// 入力されていない
		if (mail.equals("NULL!")) {
			mail = "";
			isError = true;
			errorMsg += "メールアドレスを入力してください";
		}
		// メールアドレスでない
		else if (Checker.isMail(mail) == false) {
			isError = true;
			errorMsg += "メールアドレスを正しく入力してください";
		}
		// 100文字を超えている
		else if (mail.length()>100) {
			isError = true;
			errorMsg += "100文字以内のメールアドレスしか登録できません";
		}
		
		// エラーがないとき
		// ユーザーIDを登録してみる
		if (isError == false) {
			// ユーザーIDが登録できない
			if (UserBean.tryRegisterUserId(id) == false) {
				isError = true;
				errorMsg = "このユーザーIDは既に登録されています";
			}
		}
		
		// エラー
		// ID、メールアドレス、表示名と
		// エラーメッセージをセットしてログイン画面に戻す
		if (isError) {
			
			// リクエストにID、メールアドレス、表示名をセット
			request.setAttribute("rId"  , id);
			request.setAttribute("rMail", mail);
			request.setAttribute("rName", dispName);
			
			// エラーメッセージをセット
			request.setAttribute("rMessage", errorMsg);
			
			// ログイン画面に戻す
			return "/m/jsp/login.jsp";
		}
		
		// ユーザーを生成する
		UserBean user = new UserBean();
		
		// ID、パスワード、メールアドレス、表示名をセットする
		user.setUserId(id);
		user.setPassword(pass);
		user.setMail(mail);
		user.setDispName(dispName);
		
		// ユーザーをデータベースに登録する
		user.registerUser();
		
		// セッションの取得
		HttpSession session = request.getSession();
		
		// セッションにユーザーをセット
		session.setAttribute("user", user);
		
		// セッションのタイムアウト秒数
		// 負の値でタイムアウトなし
		session.setMaxInactiveInterval(-1);
		
		// リクエストにメッセージをセット
		String welcome =
		  "Welcome！<br />" +
		  "ここにはあなたの投稿と、<br />" +
		  "あなたがフォローしている<br />" +
		  "ユーザーの投稿が表示されます<br />" +
		  "<br />" +
		  "上のメニューを押すと<br />" +
		  "他の画面に移動できます";
		request.setAttribute("message", welcome);
		
		// メニューのホームの色を変える
		request.setAttribute("selectMenu", "home");
		
		// タイムライン友達版に遷移
		return friend(request);
	}
	
	/**
	 * ログイン
	 * /teamb/pictsharemobile/login
	 * 
	 * ID、パスワードでログイン
	 * セッションにユーザーをセット
	 * ホームに遷移
	 * 
	 * @return 遷移先URL
	 */
	private String login(HttpServletRequest request) {
		
		// セッションの取得
		HttpSession session = request.getSession();
		
		UserBean u = (UserBean)session.getAttribute("user");
		if (u != null) {

			// ホームに遷移
			return home(request);
		}
		
		// IDを取得
		String id = request.getParameter("loginUserID");
		id = Checker.sanitize(id);
		
		// パスワードを取得
		String pass = request.getParameter("loginPASS");
		pass = Checker.sanitize(pass);
		
		// パスワードを確認する
		// パスワードが違う
		if (UserBean.authPass(id, pass) == false) {
			
			// リクエストに入力されたIDと、エラーメッセージをセット
			if (id.equals("NULL!")) {
				id = "";
			}
			request.setAttribute("id", id);
			request.setAttribute("message", "ID か パスワード が間違っています");
			
			// ログイン画面に戻す
			return "/m/jsp/login.jsp";
		}
		
		// IDでユーザーを取得
		UserBean user = UserBean.getUserByUserId(id);
		
		// セッションにユーザーをセット
		session.setAttribute("user", user);
		
		// セッションのタイムアウト秒数
		// 負の値でタイムアウトなし
		session.setMaxInactiveInterval(-1);
		
		// リクエストにメッセージをセット
		request.setAttribute("message", "こんにちは");
		
		// ホームに遷移
		return home(request);
	}
	
	/**
	 * ログアウト
	 * /teamb/pictsharemobile/logout
	 * 
	 * @return 遷移先URL
	 */
	private String logout(HttpServletRequest request) {
		
		// セッションの取得
		HttpSession session = request.getSession();
		
		// セッションの破棄
		session.invalidate();
		
		// ログアウトメッセージをセット
		request.setAttribute("message", "ログアウトしました");
		
		// ログイン画面に戻す
		return "/m/jsp/login.jsp";
	}
	
	/**
	 * アカウント設定画面
	 * /teamb/pictsharemobile/setting
	 * 
	 * 二重設定防止のために process をアカウント設定に
	 * 
	 * @return 遷移先URL
	 */
	private String setting(HttpServletRequest request) {
		
		// セッションの取得
		HttpSession session = request.getSession();
		
		// process をアカウント設定に
		session.setAttribute("process", "setting");
		
		// アカウント設定画面に遷移
		return "/m/jsp/accountsetting.jsp";
	}
	
	/**
	 * アカウント設定を行う
	 * /teamb/pictsharemobile/settingdone
	 * 
	 * リクエストの大きさが 500KB 以上なら拒否
	 * アップロードされたファイルを保存するための準備
	 * リクエストを、アイコンファイルと、パスワードなどの文字列に分解
	 * アイコンがあれば、拡張子をチェックして保存
	 * パスワードと紹介文のチェック
	 * UserBean のメソッドでアカウント設定(DB処理)
	 * 設定後のユーザーをセッションにセット
	 * ホームに遷移
	 * 
	 * @return 遷移先URL
	 */
	private String settingDone(HttpServletRequest request) throws ServletException, IOException {
		
		List items = null; // リクエストから送られてくる、全ての項目
		
		FileItem icon = null; // アイコンファイル
		
		String pass     = null; // パスワード
		String profile  = null; // 紹介文
		String lock     = null; // 鍵つき設定
		String mapMode  = null; // 画面設定
		String lat      = null; // 緯度
		String lng      = null; // 経度
		String saveName = null; // 保存するファイル名 (ユーザーID＋拡張子)
		
		boolean isError  = false; // エラー
		String  errorMsg = "";    // エラーメッセージ
		
		// リクエスト全体のサイズが 500KB 以上ならエラー
		if (request.getContentLength() >= (500 * 1024)) {
			
			// エラーメッセージをセット
			request.setAttribute("message", "通信が 500KB を超えました");
			
			// メニューの設定の色を変える
			request.setAttribute("selectMenu", "setting");
			
			// アカウント設定画面に戻す
			return "/m/jsp/accountsetting.jsp";
		}
		
		// セッションの取得
		HttpSession session = request.getSession();
		
		// セッションからユーザーを取得
		UserBean user = (UserBean)session.getAttribute("user");
		
		// ログインの確認
		if (user == null || user.getUserId() == null) {
			// ログイン画面に戻す
			return "/m/jsp/login.jsp";
		}
		
		// 二重設定の防止
		// process がアカウント設定でなければ終了
		String process = (String)session.getAttribute("process");
		if (process != null && process.equals("setting")) {
			// process を消去
			session.removeAttribute("process");
		} else {
			// ホームに遷移
			return home(request);
		}
		
		// ファクトリ（ファイル工場）の生成
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// バッファの最大値 よくわからんけど1024
		factory.setSizeThreshold(1024);
		
		// ファイルの保存場所を指定 /teamb/img/user/ の中
		String savePath = getServletContext().getRealPath("/img/user") + "/";
		factory.setRepository(new File(savePath));
		
		// リクエストの中からファイルとか探してくれるやつ
		ServletFileUpload upload = new ServletFileUpload(factory);
		
		// アップロードするファイル全体の最大サイズ 500KB
		upload.setSizeMax(500 * 1024);
		// アップロードするファイル1つの最大サイズ 500KB
		upload.setFileSizeMax(500 * 1024);
		
		// リクエストから送られてきたものを
		// ファイルとか文字列とかに分解
		try {
			items = upload.parseRequest(request);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		for (Object val : items) {
			FileItem item = (FileItem)val;
			
			// フォーム入力など ファイルじゃないもの
			if (item.isFormField()) {
				
				// name属性の値を取得
				String name = item.getFieldName();
				
				// パスワードなどをセット
				if (name.equals("pass")) {
					pass = item.getString("UTF-8");
				} else if (name.equals("profile")) {
					profile = item.getString("UTF-8");
				} else if (name.equals("lock")) {
					lock = item.getString("UTF-8");
				} else if (name.equals("home")) {
					mapMode = item.getString("UTF-8");
				} else if (name.equals("lat")) {
					lat = item.getString("UTF-8");
				} else if (name.equals("lng")) {
					lng = item.getString("UTF-8");
				}
			}
			
			// ファイル
			else {
				
				// ファイル名を取得
				String fileName = item.getName();
				
				if (fileName != null && !fileName.equals("")) {
					// 拡張子を取得 .png など
					String extension = fileName.substring(fileName.lastIndexOf('.'));
					
					// 拡張子をチェックする
					if (Checker.checkExtension(extension)) {
						
						// 保存するファイル名 (ユーザーID＋拡張子)
						saveName = user.getUserId() + extension;
						
						// アイコンファイルを確保
						icon = item;
					} else {
						// 一時保存ファイルがあれば削除する
						item.delete();
					}
				}
			}
		}
		
		// パスワードのチェック
		pass = Checker.sanitize(pass);
		// 入力されている
		if (!pass.equals("NULL!")) {
			
			// 英数字のみでない
			if (Checker.isAlnum(pass) == false) {
				isError = true;
				errorMsg += "パスワードに使用可能な文字は英数字のみです<br />";
			}
			// 20文字を超えている
			else if (pass.length()>20) {
				isError = true;
				errorMsg += "パスワードは20文字以内にしてください<br />";
			}
		}
		
		// 紹介文のチェック
		profile = Checker.sanitize(profile);
		// 入力されていない
		if (profile.equals("NULL!")) {
			profile = "";
		} else {
			// 使用できない文字がある
			String black = Checker.containsBlack(profile);
			if (black != null) {
				isError = true;
				errorMsg += "プロフィールに使用できない文字" + black + "が含まれています";
			} else {
				
				// 絵文字がある
				String symbol = Checker.containsSymbol(profile);
				if (symbol != null) {
					isError = true;
					errorMsg += symbol + "などの絵文字は使用できません";
				}
				
				// 140文字を超えている
				else if (profile.length()>140) {
					isError = true;
					errorMsg += "プロフィールは140文字以内にしてください";
				}
			}
		}
		
		// エラー
		// 紹介文とエラーメッセージをセットしてアカウント設定画面に戻す
		if (isError) {
			
			// 一時保存ファイルがあれば削除する
			if (icon != null) {
				icon.delete();
			}
			
			// リクエストに紹介文をセット
			request.setAttribute("profile", profile);
			
			// エラーメッセージをセット
			request.setAttribute("message", errorMsg);
			
			// process をアカウント設定に
			session.setAttribute("process", "setting");
			
			// メニューの設定の色を変える
			request.setAttribute("selectMenu", "setting");
			
			// アカウント設定画面に戻す
			return "/m/jsp/accountsetting.jsp";
		}
		
		// アイコンファイルの保存
		if (icon != null) {
			try {
				
				// アイコンファイルを保存する
				icon.write(new File(savePath + saveName));
				
				// 一時保存ファイルがあれば削除する
				icon.delete();
				
				// ユーザーのユーザーアイコンファイル名をセット
				user.setIconFile(saveName);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
		
		// ユーザーにパスワードなどをセット
		if (!pass.equals("NULL!")) {
			user.setPassword(pass);
		}
		user.setProfile(profile);
		if (lock.equals("true")) {
			user.setIsLocked(true);
		} else {
			user.setIsLocked(false);
		}
		if(mapMode.equals("map")) {
			user.setIsMapMode(true);
		} else {
			user.setIsMapMode(false);
		}
		user.setInitLat(Float.parseFloat(lat));
		user.setInitLng(Float.parseFloat(lng));
		
		// アカウント設定を行う
		user.setAccount();
		
		// セッションにユーザーをセット
		session.setAttribute("user", user);
		
		// リクエストにメッセージをセット
		request.setAttribute("message", "アカウント設定が完了しました");
		
		// ホームに遷移
		return home(request);
	}
	
	/**
	 * ホーム
	 * /teamb/pictsharemobile/home
	 * 
	 * 画面設定によって
	 * 遷移先を地図と友達に分ける
	 * 
	 * @return 遷移先URL
	 */
	private String home(HttpServletRequest request) {
		
		// セッションの取得
		HttpSession session = request.getSession();
		
		// セッションからユーザーを取得
		UserBean user = (UserBean)session.getAttribute("user");
		
		// 自分のユーザー情報をリロードする
		if (user == null) {
			user = UserBean.getUserByUserId(null);
		} else {
			user = UserBean.getUserByUserId(user.getUserId());
		}
		session.setAttribute("user", user);
		
		// 画面設定を確認する
		// 地図指定ならタイムライン地図版に遷移
		if (user.getIsMapMode() == true) {
			return map(request);
		}
		
		// 友達閲覧ならタイムライン友達版に遷移
		return friend(request);
	}
	
	/**
	 * タイムライン地図版
	 * /teamb/pictsharemobile/map
	 * 
	 * アカウント設定の初期緯度経度から、
	 * グーグルマップに初期表示される範囲の投稿リストを取得
	 * セッションに投稿リストをセット
	 * 地図指定閲覧画面に遷移
	 * 
	 * @return 遷移先URL
	 */
	private String map(HttpServletRequest request) {
		
		// セッションの取得
		HttpSession session = request.getSession();
		
		// セッションからユーザーを取得
		UserBean user = (UserBean)session.getAttribute("user");
		
		// 自分のユーザー情報をリロードする
		if (user == null) {
			user = UserBean.getUserByUserId(null);
		} else {
			user = UserBean.getUserByUserId(user.getUserId());
		}
		session.setAttribute("user", user);
		
		// 始点と終点の緯度経度を計算する
		// 今は適当です
		float centerLat = user.getInitLat();
		float centerLng = user.getInitLng();
		
		String startLat = Double.toString(centerLat-0.396);
		String startLng = Double.toString(centerLng-0.52);
		String endLat   = Double.toString(centerLat+0.396);
		String endLng   = Double.toString(centerLng+0.52);
		
		// 投稿リストを取得する
		ArrayList<MobilePostBean> postList =
		  MobilePostBean.getPostByPosition
		  (user.getUserId(), startLat, startLng, endLat, endLng, 0);
		
		// セッションに投稿リストをセット
		session.setAttribute("postList", postList);
		
		// 地図指定閲覧画面に遷移
		return "/m/jsp/map.jsp";
	}
	
	/**
	 * タイムライン友達版
	 * /teamb/pictsharemobile/friend
	 * 
	 * 自分とフォローの投稿リストを取得
	 * セッションに投稿リストをセット
	 * タイムライン友達版画面に遷移
	 * 
	 * @return 遷移先URL
	 */
	private String friend(HttpServletRequest request) {
		
		// セッションの取得
		HttpSession session = request.getSession();
		
		// セッションからユーザーを取得
		UserBean user = (UserBean)session.getAttribute("user");
		
		// 自分のユーザー情報をリロードする
		if (user == null) {
			user = UserBean.getUserByUserId(null);
		} else {
			user = UserBean.getUserByUserId(user.getUserId());
		}
		session.setAttribute("user", user);
		
		// 投稿リストを取得する
		ArrayList<MobilePostBean> postList =
		  MobilePostBean.getPostByFriend(user.getUserId(), 0);
		
		// セッションに投稿リストをセット
		session.setAttribute("postList", postList);
		
		// タイムライン友達版画面に遷移
		return "/m/jsp/main.jsp";
	}
	
	/**
	 * 写真投稿を行う
	 * /teamb/pictsharemobile/postdone
	 * 
	 * アップロードされたファイルを保存する準備
	 * リクエストを、写真とそれ以外に分解
	 * 投稿文のチェック
	 * 投稿日時、メッシュコード、タグなどの準備
	 * データベースに登録して投稿IDを取得
	 * 投稿IDからファイル名を決めて写真を保存
	 */
	private void postDone(HttpServletRequest request, HttpServletResponse response)
	  throws ServletException, IOException
	{
		response.setContentType("text/plain; charset=UTF-8");
		
		List items = null; // リクエストから送られてくる全ての項目
		
		ArrayList<FileItem> picture = new ArrayList<FileItem>(); // 写真
		
		String postMsg = null; // 投稿文
		String lat     = null; // 緯度
		String lng     = null; // 経度
		
		// 保存するファイル名 (投稿ID5桁＋写真番号2桁＋拡張子)
		ArrayList<String> saveName = new ArrayList<String>();
		
		int     count    = 0;     // 写真番号
		boolean isError  = false; // エラー
		String  errorMsg = "";    // エラーメッセージ
		
		// セッションの取得
		HttpSession session = request.getSession();
		
		// セッションからユーザーを取得
		UserBean user = (UserBean)session.getAttribute("user");
		
		// ログインの確認
		if (user == null || user.getUserId() == null) {
			// エラーメッセージを返す
			response.getWriter().print("ユーザーを判別できません 再ログインしてください");
			return ;
		}
		
		// ファクトリ（ファイル工場）の生成
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// バッファの最大値 よくわからんけど1024
		factory.setSizeThreshold(1024);
		
		// ファイルの保存場所を指定 /teamb/img/post/ の中
		String savePath = getServletContext().getRealPath("/img/post") + "/";
		factory.setRepository(new File(savePath));
		
		// リクエストの中からファイルとか探してくれるやつ
		ServletFileUpload upload = new ServletFileUpload(factory);
		
		// アップロードするファイル全体の最大サイズ 無制限
		upload.setSizeMax(-1);
		// アップロードするファイル1つの最大サイズ 無制限
		upload.setFileSizeMax(-1);
		
		// リクエストから送られてきたものを
		// ファイルとか文字列とかに分解
		try {
			items = upload.parseRequest(request);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		for (Object val : items) {
			FileItem item = (FileItem)val;
			
			// フォーム入力など ファイルじゃないもの
			if (item.isFormField()) {
				
				// name属性の値を取得
				String name = item.getFieldName();
				
				// 投稿文などをセット
				if (name.equals("postmsg")) {
					postMsg = item.getString("UTF-8");
				} else if (name.equals("lat")) {
					lat = item.getString("UTF-8");
				} else if (name.equals("lng")) {
					lng = item.getString("UTF-8");
				}
			}
			
			// ファイル
			else {
				
				// ファイル名を取得
				String fileName = item.getName();
				
				if (fileName != null && !fileName.equals("")) {
					// 拡張子を取得 .png など
					String extension = fileName.substring(fileName.lastIndexOf('.'));
					
					// 拡張子をチェックする
					if (Checker.checkExtension(extension)) {
						
						// 保存するファイル名 (投稿ID5桁＋写真番号2桁＋拡張子)
						// 投稿IDは一時的に "xxxxx" にしておく
						count++;
						saveName.add("xxxxx0" + count + extension);
						
						// 写真を確保
						picture.add(item);
					} else {
						// 一時保存ファイルがあれば削除する
						item.delete();
					}
				}
			}
		}
		
		// 写真がない
		if (count == 0) {
			isError = true;
			errorMsg = "写真がありません<br />";
		}
		
		// 投稿文のチェック
		postMsg = Checker.sanitize(postMsg);
		// 入力されていない
		if (postMsg.equals("NULL!")) {
			postMsg = "";
		} else {
			// 使用できない文字がある
			String black = Checker.containsBlack(postMsg);
			if (black != null) {
				isError = true;
				errorMsg += "投稿文に使用できない文字" + black + "が含まれています";
			} else {
				
				// 絵文字がある
				String symbol = Checker.containsSymbol(postMsg);
				if (symbol != null) {
					isError = true;
					errorMsg += symbol + "などの絵文字は使用できません";
				}
				
				// 140文字を超えている
				else if (postMsg.length()>140) {
					isError = true;
					errorMsg += "投稿文は140文字以内にしてください";
				}
			}
		}
		
		// エラー
		// 一時保存ファイルを削除して、エラーメッセージを返す
		if (isError) {
			
			// 一時保存ファイルがあれば削除する
			for (int i=0; i<count; i++) {
				picture.get(i).delete();
			}
			
			// エラーメッセージを返す
			response.getWriter().print(errorMsg);
			return;
		}
		
		// 投稿の生成
		PostBean post = new PostBean();
		
		// ユーザーIDをセット
		post.setUserId(user.getUserId());
		
		// 緯度経度をセット
		float fLat = Float.parseFloat(lat);
		float fLng = Float.parseFloat(lng);
		post.setLat(fLat);
		post.setLng(fLng);
		
		// 画像ファイル名をセット
		post.setImgFile(saveName);
		
		// 投稿文をセット
		post.setMessage(postMsg);
		
		// 投稿日時をセット
		post.setPostDate(new Timestamp(System.currentTimeMillis()));
		
		// メッシュコードを計算する
		int meshCode = MeshCode.get(fLat, fLng);
		// メッシュコードをセット
		post.setMeshCode(meshCode);
		
		// 投稿文からタグを判定する
		ArrayList<String> tagList = TagParser.parse(postMsg);
		
		// タグIDのリスト
		ArrayList<Integer> tagIdList = new ArrayList<Integer>();
		
		tagLoop: for (String t : tagList) {
			
			// タグIDを取得する
			int tagId = TagBean.getOrAddTagIdByTagName(t);
			
			// 同じタグが複数使われていないかチェック
			for (int i : tagIdList) {
				if (tagId == i) {
					continue tagLoop;
				}
			}
			
			// タグIDのリストに追加する
			tagIdList.add(tagId);
			
			// 使用回数を1増やす
			TagBean.increaseCountByTagId(tagId);
		}
		
		// タグIDをセット
		post.setTagId(tagIdList);
		
		// 投稿をデータベースに登録する
		post.registerPost();
		
		// 保存するファイル名を取得
		saveName = post.getImgFile();
		
		// 写真の保存
		try {
			for (int i=0; i<count; i++) {
				
				// アイコンファイルを保存する
				picture.get(i).write(new File(savePath + saveName.get(i)));
				
				// 一時保存ファイルがあれば削除する
				picture.get(i).delete();
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		// メッセージを返す
		response.getWriter().print("ok");
	}
	
	/**
	 * 投稿編集・削除画面
	 * /teamb/pictsharemobile/edit
	 * 
	 * 投稿がユーザーのものかチェック
	 * 投稿をセッションにセット
	 * 二重編集防止のため process を編集・削除に
	 * 
	 * @return 遷移先URL
	 */
	private String edit(HttpServletRequest request) {
		
		// セッションの取得
		HttpSession session = request.getSession();
		
		// セッションからユーザーを取得
		UserBean user = (UserBean)session.getAttribute("user");
		
		// ログインの確認
		if (user == null || user.getUserId() == null) {
			// ログイン画面に戻す
			return "/m/jsp/login.jsp";
		}
		
		// 投稿IDを取得
		int postId = Integer.parseInt(request.getParameter("editPostId"));
		
		// 投稿を取得
		PostBean post = PostBean.getPostByPostId(postId, false);
		
		// 投稿がユーザーのものでない
		if (!user.getUserId().equals(post.getUserId())) {
			
			// リクエストにメッセージをセット
			request.setAttribute("message", "編集できませんでした");
			
			// ユーザー情報閲覧画面に遷移
			return "/m/jsp/userdetail/userdetail.jsp";
		}
		
		// セッションに投稿をセット
		session.setAttribute("editPost", post);
		
		// process を編集・削除に
		session.setAttribute("process", "edit");
		
		// 投稿編集・削除画面に遷移
		return "/m/jsp/edit.jsp";
	}
	
	/**
	 * 投稿を編集する
	 * /teamb/pictsharemobile/editdone
	 * 
	 * 投稿文のチェック
	 * メッシュコードの計算
	 * 投稿日時が1週間以内なら、編集前の投稿文のタグを、おすすめタグの回数から減らす
	 * タグの処理
	 * データベースの更新
	 * ユーザー情報閲覧画面に遷移
	 * 
	 * @return 遷移先URL
	 */
	private String editDone(HttpServletRequest request) {
		
		String postMsg = null; // 投稿文
		float  lat     = 0.0f; // 緯度
		float  lng     = 0.0f; // 経度
		
		boolean isError  = false; // エラー
		String  errorMsg = "";    // エラーメッセージ
		
		// セッションの取得
		HttpSession session = request.getSession();
		
		// セッションからユーザーを取得
		UserBean user = (UserBean)session.getAttribute("user");
		
		// ログインの確認
		if (user == null || user.getUserId() == null) {
			// ログイン画面に戻す
			return "/m/jsp/login.jsp";
		}
		
		// 二重編集の防止
		// process が編集・削除でなければ終了
		String process = (String)session.getAttribute("process");
		if (process != null && process.equals("edit")) {
			// process を消去
			session.removeAttribute("process");
		} else {
			// ユーザー情報閲覧画面に遷移
			return "/m/jsp/userdetail/userdetail.jsp";
		}
		
		// セッションから投稿を取得
		PostBean post = (PostBean)session.getAttribute("editPost");
		
		// 投稿文を取得
		postMsg = request.getParameter("postmsg");
		
		// 緯度経度を取得
		lat = Float.parseFloat(request.getParameter("lat"));
		lng = Float.parseFloat(request.getParameter("lng"));
		
		// 投稿文のチェック
		postMsg = Checker.sanitize(postMsg);
		// 入力されていない
		if (postMsg.equals("NULL!")) {
			postMsg = "";
		} else {
			// 使用できない文字がある
			String black = Checker.containsBlack(postMsg);
			if (black != null) {
				isError = true;
				errorMsg = "投稿文に使用できない文字" + black + "が含まれています";
			} else {
				
				// 絵文字がある
				String symbol = Checker.containsSymbol(postMsg);
				if (symbol != null) {
					isError = true;
					errorMsg += symbol + "などの絵文字は使用できません";
				}
				
				// 140文字を超えている
				else if (postMsg.length()>140) {
					isError = true;
					errorMsg += "投稿文は140文字以内にしてください";
				}
			}
		}
		
		// エラー
		// 投稿文とエラーメッセージをセットして投稿編集・削除画面に戻す
		if (isError) {
			
			// リクエストに投稿文をセット
			request.setAttribute("postmsg", postMsg);
			
			// エラーメッセージをセット
			request.setAttribute("message", errorMsg);
			
			// process を編集・削除に
			session.setAttribute("process", "edit");
			
			// 投稿編集・削除画面に戻す
			return "/m/jsp/edit.jsp";
		}
		
		// 緯度経度をセット
		post.setLat(lat);
		post.setLng(lng);
		
		// 投稿文をセット
		post.setMessage(postMsg);
		
		// メッシュコードを計算する
		int meshCode = MeshCode.get(lat, lng);
		// メッシュコードをセット
		post.setMeshCode(meshCode);
		
		// 現在日時のカレンダーを取得
		Calendar cal = Calendar.getInstance();
		
		// 6日前の0時0分にする
		cal.set  (Calendar.HOUR_OF_DAY, 0);
		cal.clear(Calendar.MINUTE);
		cal.clear(Calendar.SECOND);
		cal.clear(Calendar.MILLISECOND);
		cal.add  (Calendar.DAY_OF_MONTH, -6);
		
		// 投稿日時を取得
		Timestamp postDate = post.getPostDate();
		
		// 投稿日時のカレンダーを取得
		Calendar postCal = Calendar.getInstance();
		postCal.setTimeInMillis(postDate.getTime());
		
		// 投稿日時が1週間以内ならtrue
		// 投稿日時を6日前の0時0分と比較する
		boolean within1week = (postCal.compareTo(cal) >= 0);
		
		// 投稿日時が1週間以内ならタグの使用回数を1ずつ減らす
		ArrayList<Integer> tagIdList = post.getTagId();
		if (within1week) {
			for (int i : tagIdList) {
				TagBean.decreaseCountByTagId(postDate, i);
			}
		}
		
		// 投稿文からタグを判定する
		ArrayList<String> tagList = TagParser.parse(postMsg);
		
		// タグIDのリストを初期化
		tagIdList = new ArrayList<Integer>();
		
		tagLoop: for (String t : tagList) {
			
			// タグIDを取得する
			int tagId = TagBean.getOrAddTagIdByTagName(t);
			
			// 同じタグが複数使われていないかチェック
			for (int i : tagIdList) {
				if (tagId == i) {
					continue tagLoop;
				}
			}
			
			// タグIDのリストに追加する
			tagIdList.add(tagId);
			
			// 投稿日時が1週間以内なら使用回数を1増やす
			if (within1week) {
				TagBean.increaseCountByTagId(postDate, tagId);
			}
		}
		
		// タグIDをセット
		post.setTagId(tagIdList);
		
		// 投稿をデータベースに登録する
		post.editPost();
		
		// リクエストにメッセージをセット
		request.setAttribute("message", "投稿を編集しました");
		
		// 自分の投稿リストを取得する
		ArrayList<MobilePostBean> targetPostList =
		  MobilePostBean.getPostByUserId(user.getUserId(), user.getUserId(), 0);
		
		// セッションに自分の投稿リストをセット
		session.setAttribute("targetPostList", targetPostList);
		
		// ユーザー情報閲覧画面に遷移
		return "/m/jsp/userdetail/userdetail.jsp";
	}
	
	/**
	 * 投稿を削除する
	 * /teamb/pictsharemobile/deletedone
	 * 
	 * 投稿日時が1週間以内なら、編集前の投稿文のタグを、おすすめタグの回数から減らす
	 * データベースから削除
	 * ユーザー情報閲覧画面に遷移
	 * 
	 * @return 遷移先URL
	 */
	private String deleteDone(HttpServletRequest request) {
		
		// セッションの取得
		HttpSession session = request.getSession();
		
		// セッションからユーザーを取得
		UserBean user = (UserBean)session.getAttribute("user");
		
		// ログインの確認
		if (user == null || user.getUserId() == null) {
			// ログイン画面に戻す
			return "/m/jsp/login.jsp";
		}
		
		// 二重削除の防止
		// process が編集・削除でなければ終了
		String process = (String)session.getAttribute("process");
		if (process != null && process.equals("edit")) {
			// process を消去
			session.removeAttribute("process");
		} else {
			// ユーザー情報閲覧画面に遷移
			return "/m/jsp/userdetail/userdetail.jsp";
		}
		
		// セッションから投稿を取得
		PostBean post = (PostBean)session.getAttribute("editPost");
		
		// 現在日時のカレンダーを取得
		Calendar cal = Calendar.getInstance();
		
		// 6日前の0時0分にする
		cal.set  (Calendar.HOUR_OF_DAY, 0);
		cal.clear(Calendar.MINUTE);
		cal.clear(Calendar.SECOND);
		cal.clear(Calendar.MILLISECOND);
		cal.add  (Calendar.DAY_OF_MONTH, -6);
		
		// 投稿日時を取得
		Timestamp postDate = post.getPostDate();
		
		// 投稿日時のカレンダーを取得
		Calendar postCal = Calendar.getInstance();
		postCal.setTimeInMillis(postDate.getTime());
		
		// 投稿日時が1週間以内ならtrue
		// 投稿日時を6日前の0時0分と比較する
		boolean within1week = (postCal.compareTo(cal) >= 0);
		
		// 投稿日時が1週間以内ならタグの使用回数を1ずつ減らす
		ArrayList<Integer> tagIdList = post.getTagId();
		if (within1week) {
			for (int i : tagIdList) {
				TagBean.decreaseCountByTagId(postDate, i);
			}
		}
		
		// 投稿をデータベースから削除する
		post.deletePost();
		
		// リクエストにメッセージをセット
		request.setAttribute("message", "投稿を削除しました");
		
		// 自分の投稿リストを取得する
		ArrayList<MobilePostBean> targetPostList =
		  MobilePostBean.getPostByUserId(user.getUserId(), user.getUserId(), 0);
		
		// セッションに自分の投稿リストをセット
		session.setAttribute("targetPostList", targetPostList);
		
		// ユーザー情報閲覧画面に遷移
		return "/m/jsp/userdetail/userdetail.jsp";
	}
	
	/**
	 * ランキング
	 * /teamb/pictsharemobile/ranking?y=2015&m=1&s=1
	 * 
	 * URLのパラメタで、年、月、表示開始順位を取得
	 * 今月ランキングならセッションに過去ランキングを保存しない
	 * 過去ランキングならセッションに過去ランキングを保存する
	 * 
	 * @return 遷移先URL
	 */
	private String showRanking(HttpServletRequest request) {
		
		int year      = 0; // 年 0なら今月
		int month     = 0; // 月 0なら今月
		int startRank = 1; // 表示開始順位
		
		// アプリケーションスコープの変数 sc を取得
		ServletContext sc = getServletContext();
		
		// セッションの取得
		HttpSession session = request.getSession();
		
		// 年と月を取得
		String y = request.getParameter("y");
		String m = request.getParameter("m");
		
		// 年か月が null なら今月のランキング
		if (y == null || m == null || y.equals("") || m.equals("")) {
			year  = 0;
			month = 0;
		} else {
			try {
				year  = Integer.parseInt(y);
				month = Integer.parseInt(m);
				
				// 年を範囲内に
				Calendar cal  = Calendar.getInstance();
				int thisYear  = cal.get(Calendar.YEAR);
				int thisMonth = cal.get(Calendar.MONTH) + 1;
				if (year < 2014) {
					year = 2014;
				} else if (year > thisYear) {
					year = thisYear;
				}
				
				// 月を範囲内に
				if (month < 1) {
					month = 1;
				} else if (month > 12) {
					month = 12;
				}
				
				// 今年
				if (year == thisYear) {
					// 今月以降なら今月のランキング
					if (month >= thisMonth) {
						year  = 0;
						month = 0;
					}
				}
			} catch (NumberFormatException e) {
				year  = 0;
				month = 0;
			}
		}
		
		// 表示開始順位を取得
		String s = request.getParameter("s");
		
		// 表示開始順位が null なら1位から表示
		if (s == null || s.equals("")) {
			startRank = 1;
		} else {
			try {
				startRank = Integer.parseInt(s);
				
				// 表示開始順位を範囲内に
				if (startRank < 1) {
					startRank = 1;
				} else if (startRank > 91) {
					startRank = 91;
				}
			} catch (NumberFormatException e) {
				startRank = 1;
			}
		}
		
		// 今月ランキング
		if (year == 0) {
			// セッションから過去ランキングを消去
			session.removeAttribute("pastRanking");
		}
		
		// 過去ランキング
		else {
			
			// アプリケーションスコープから過去ランキングを取得
			PastRankingBean pastRanking =
			  (PastRankingBean)sc.getAttribute("pastRanking" + year + "_" + month);
			
			// アプリケーションスコープにない
			if (pastRanking == null) {
				
				// 過去ランキングを取得する
				pastRanking = PastRankingBean.getPastRankingByYearMonth(year, month);
				
				// 過去ランキングをアプリケーションスコープに保存
				sc.setAttribute("pastRanking" + year + "_" + month, pastRanking);
			}
			
			// セッションに過去ランキングをセット
			session.setAttribute("pastRanking", pastRanking);
		}
		
		// リクエストに表示開始順位をセット
		request.setAttribute("startRank", new Integer(startRank));
		
		// ランキング画面に遷移
		return "/m/jsp/ranking.jsp";
	}
	
	/**
	 * ユーザー情報閲覧
	 * フォロー/フォロワーリスト
	 * /teamb/pictsharemobile/user/Piyohiko
	 * /teamb/pictsharemobile/user/Piyohiko/follow
	 * /teamb/pictsharemobile/user/Piyohiko/follower
	 * 
	 * 拡張パスで分岐
	 * 拡張パスのIDで対象ユーザーを取得
	 * 
	 * 対象ユーザーが自分か判定
	 * 対象ユーザーをフォローしているか判定
	 * 対象ユーザーが鍵つきか判定
	 * 
	 * セッションの relation に、自分と対象ユーザーの関係をセット
	 * 対象ユーザーの投稿リストを取得し、セッションにセット
	 * 
	 * 対象ユーザーが自分で、フォロー申請があればメッセージを表示
	 * 
	 * フォロー/フォロワーリストなら、
	 * そのユーザーのフォロー/フォロワーの一覧をセッションにセット
	 * 
	 * @return 遷移先URL
	 */
	private String showUserDetail(HttpServletRequest request) {
		
		boolean followList = true; // フォローリスト or フォロワーリスト
		String id = null; // 拡張パスに含まれるユーザーID
		
		// セッションの取得
		HttpSession session = request.getSession();
		
		// セッションからユーザーを取得
		UserBean user = (UserBean)session.getAttribute("user");
		
		// 自分のユーザー情報をリロードする
		if (user == null) {
			user = UserBean.getUserByUserId(null);
		} else {
			user = UserBean.getUserByUserId(user.getUserId());
		}
		session.setAttribute("user", user);
		
		// 拡張パス
		// /user/Piyohiko
		// /user/Piyohiko/follow
		// /user/Piyohiko/follower
		String path = request.getPathInfo();
		
		// /user/以降を取得
		// Piyohiko
		// Piyohiko/follow
		// Piyohiko/follower
		path = path.substring(6);
		
		// "/"で分割する 最大2個
		String[] paths = path.split("/", 2);
		
		// '/'がない
		// ユーザー情報閲覧
		if (paths.length == 1) {
			id = path;
			
			id = Checker.sanitize(id);
			
			// 対象ユーザーを取得する
			UserBean targetUser = UserBean.getUserByUserId(id);
			
			// そのIDのユーザーがいない
			if (targetUser == null) {
				
				// セッションに自分をセット
				session.setAttribute("targetUser", user);
				session.setAttribute("relation", "yourself");
				
				// セッションに空の投稿リストをセット
				session.setAttribute("targetPostList", null);
				
				// リクエストにメッセージをセット
				request.setAttribute("message", id + "というユーザーは存在しません");
				
				// ユーザー情報閲覧画面に遷移
				return "/m/jsp/userdetail/userdetail.jsp";
			}
			
			// セッションに対象ユーザーをセット
			session.setAttribute("targetUser", targetUser);
			
			// 対象ユーザーが自分でない ＆ フォローしていない ＆ 鍵つき
			// 投稿リストを取得せずにユーザー情報閲覧画面に遷移
			if (user.getUserId() == null || !user.getUserId().equals(id)) {
				if (user.isFollowing(id) == false) {
					if (targetUser.getIsLocked()) {
						if(user.isFollowWaiting(id)) {
							// セッションに対象ユーザーをフォロー申請中であることをセット
							session.setAttribute("relation", "followWait");
						} else {
							// セッションに対象ユーザーが鍵つきであることをセット
							session.setAttribute("relation", "locked");
						}
						
						// セッションに空の投稿リストをセット
						session.setAttribute("targetPostList", null);
						
						// リクエストにメッセージをセット
						request.setAttribute("message",
						  "鍵つきユーザーの投稿は<br />" +
						  "フォロー申請が許可されると見ることができます");
						
						// ユーザー情報閲覧画面に遷移
						return "/m/jsp/userdetail/userdetail.jsp";
					} else {
						// セッションに対象ユーザーを未フォロー鍵なしであることをセット
						session.setAttribute("relation", "notFollow");
					}
				} else {
					// セッションに対象ユーザーをフォロー済みであることをセット
					session.setAttribute("relation", "follow");
				}
			} else {
				// セッションに対象ユーザーが自分であることをセット
				session.setAttribute("relation", "yourself");
				
				// フォロワー申請中IDがあればメッセージをセット
				if (user.getFollowerWaitId().size() > 0) {
					request.setAttribute("message",
					  "<a href=\"/teamb/pictsharemobile/followrequest\">" +
					  "フォローリクエストがあります</a>");
				}
				
				// メニューのアカウントの色を変える
				request.setAttribute("selectMenu", "account");
			}
			
			// 対象ユーザーの投稿リストを取得する
			ArrayList<MobilePostBean> targetPostList =
			  MobilePostBean.getPostByUserId(user.getUserId(), id, 0);
			
			// セッションに対象ユーザーの投稿リストをセット
			session.setAttribute("targetPostList", targetPostList);
			
			// ユーザー情報閲覧画面に遷移
			return "/m/jsp/userdetail/userdetail.jsp";
		}
		
		// '/'より前をユーザーIDにセット
		else {
			id = paths[0];
			
			// follow
			if (paths[1].equals("follow")) {
				followList = true;
			}
			
			// follower
			else if (paths[1].equals("follower")) {
				followList = false;
			}
			
			// 想定外
			else {
				
				// セッションに自分をセット
				session.setAttribute("targetUser", user);
				session.setAttribute("relation", "yourself");
				
				// セッションに空の投稿リストをセット
				session.setAttribute("targetPostList", null);
				
				// リクエストにメッセージをセット
				request.setAttribute("message", "エラー");
				
				// ユーザー情報閲覧画面に遷移
				return "/m/jsp/userdetail/userdetail.jsp";
			}
		}
		
		id = Checker.sanitize(id);
			
		// 対象ユーザーを取得する
		UserBean targetUser = UserBean.getUserByUserId(id);
		
		// そのIDのユーザーがいない
		if (targetUser == null) {
			
			// セッションに自分をセット
			session.setAttribute("targetUser", user);
			session.setAttribute("relation", "yourself");
			
			// セッションに空の投稿リストをセット
			session.setAttribute("targetPostList", null);
			
			// リクエストにメッセージをセット
			request.setAttribute("message", id + "というユーザーは存在しません");
			
			// ユーザー情報閲覧画面に遷移
			return "/m/jsp/userdetail/userdetail.jsp";
		}
		
		// セッションに対象ユーザーをセット
		session.setAttribute("targetUser", targetUser);
		
		// 対象ユーザーが自分か
		// 対象ユーザーをフォローしているか
		// 対象ユーザーが鍵つきか
		if (user.getUserId() == null || !user.getUserId().equals(id)) {
			if (user.isFollowing(id) == false) {
				if (targetUser.getIsLocked()) {
					if(user.isFollowWaiting(id)) {
						// セッションに対象ユーザーをフォロー申請中であることをセット
						session.setAttribute("relation", "followWait");
					} else {
						// セッションに対象ユーザーが鍵つきであることをセット
						session.setAttribute("relation", "locked");
					}
				} else {
					// セッションに対象ユーザーを未フォロー鍵なしであることをセット
					session.setAttribute("relation", "notFollow");
				}
			} else {
				// セッションに対象ユーザーをフォロー済みであることをセット
				session.setAttribute("relation", "follow");
			}
		} else {
			// セッションに対象ユーザーが自分であることをセット
			session.setAttribute("relation", "yourself");
			
			// メニューのアカウントの色を変える
			request.setAttribute("selectMenu", "account");
		}
		
		// フォローリスト
		if (followList) {
			
			// 対象ユーザーのフォローリストを取得する
			ArrayList<UserBean> targetFollowList =
			  UserBean.getUserListByUserIdList(targetUser.getFollowId());
			
			// セッションに対象ユーザーのフォローリストをセット
			session.setAttribute("targetFollowList", targetFollowList);
			
			// フォロワーリストは削除
			session.removeAttribute("targetFollowerList");
		}
		
		// フォロワーリスト
		else {
			
			// 対象ユーザーのフォロワーリストを取得する
			ArrayList<UserBean> targetFollowerList =
			  UserBean.getUserListByUserIdList(targetUser.getFollowerId());
			
			// セッションに対象ユーザーのフォロワーリストをセット
			session.setAttribute("targetFollowerList", targetFollowerList);
			
			// フォローリストは削除
			session.removeAttribute("targetFollowList");
		}
		
		// フォロー/フォロワーリスト画面に遷移
		return "/m/jsp/userdetail/follow.jsp";
	}
	
	/**
	 * フォローリクエスト許可・拒否画面
	 * /teamb/pictsharemobile/followrequest
	 * 
	 * @return 遷移先URL
	 */
	private String followRequest(HttpServletRequest request) {
		
		// セッションの取得
		HttpSession session = request.getSession();
		
		// セッションからユーザーを取得
		UserBean user = (UserBean)session.getAttribute("user");
		
		// 自分のユーザー情報をリロードする
		if (user == null) {
			user = UserBean.getUserByUserId(null);
		} else {
			user = UserBean.getUserByUserId(user.getUserId());
		}
		session.setAttribute("user", user);
		
		// フォロワー申請中リストを取得する
		ArrayList<UserBean> followerWaitList =
		  UserBean.getUserListByUserIdList(user.getFollowerWaitId());
		
		// セッションにフォロワー申請中リストをセット
		session.setAttribute("followerWaitList", followerWaitList);
		
		// セッションに自分をセット
		session.setAttribute("targetUser", user);
		session.setAttribute("relation", "yourself");
		
		// フォローリクエスト許可・拒否画面に遷移
		return "/m/jsp/userdetail/request.jsp";
	}
	
	/**
	 * 拡張パスが想定外
	 * 
	 * @return 遷移先URL
	 */
	private String errorURL(HttpServletRequest request) {
		
		return "/m/index.jsp";
	}
}

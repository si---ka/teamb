package pictshare.mobile;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Timestamp;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.directwebremoting.annotations.DataTransferObject;
import org.directwebremoting.annotations.RemoteMethod;
import org.directwebremoting.annotations.RemoteProperty;
import org.directwebremoting.annotations.RemoteProxy;
import org.directwebremoting.annotations.ScriptScope;

import pictshare.UserBean;
import pictshare.other.TagParser;

/**
 * 投稿(スマホ用)
 * 
 * 投稿ID、
 * 投稿ユーザーのID・表示名・アイコン・鍵つきフラグ、
 * メッシュコード、緯度経度、画像ファイル名(5枚まで)、
 * 投稿文、タグIDリスト、タグリスト、投稿日時、高評価数、低評価数、コメント数をもつ
 */
@DataTransferObject
@RemoteProxy(scope = ScriptScope.APPLICATION)
public class MobilePostBean implements Serializable {

	/**
	 * 投稿ID
	 */
	@RemoteProperty
	private int  postId;
	public  int  getPostId() {return postId;}
	public  void setPostId(int postId) {this.postId = postId;}
	
	/**
	 * 投稿ユーザーID
	 */
	@RemoteProperty
	private String userId;
	public  String getUserId() {return userId;}
	public  void   setUserId(String userId) {this.userId = userId;}
	
	/**
	 * 投稿ユーザー表示名
	 */
	@RemoteProperty
	private String dispName;
	public  String getDispName() {return dispName;}
	public  void   setDispName(String dispName) {this.dispName = dispName;}
	
	/**
	 * 投稿ユーザーアイコン
	 */
	@RemoteProperty
	private String iconFile;
	public  String getIconFile() {return iconFile;}
	public  void   setIconFile(String iconFile) {this.iconFile = iconFile;}
	
	/**
	 * 投稿ユーザー鍵つきフラグ
	 */
	@RemoteProperty
	private boolean isLocked;
	public  boolean getIsLocked() {return isLocked;}
	public  void    setIsLocked(boolean isLocked) {this.isLocked = isLocked;}
	
	/**
	 * メッシュコード
	 */
	@RemoteProperty
	private int  meshCode;
	public  int  getMeshCode() {return meshCode;}
	public  void setMeshCode(int meshCode) {this.meshCode = meshCode;}
	
	/**
	 * 緯度
	 */
	@RemoteProperty
	private float lat;
	public  float getLat() {return lat;}
	public  void  setLat(float lat) {this.lat = lat;}
	
	/**
	 * 経度
	 */
	@RemoteProperty
	private float lng;
	public  float getLng() {return lng;}
	public  void  setLng(float lng) {this.lng = lng;}
	
	/**
	 * 画像ファイル名
	 */
	@RemoteProperty
	private ArrayList<String> imgFile;
	public  ArrayList<String> getImgFile() {return imgFile;}
	public  void setImgFile(ArrayList<String> imgFile) {this.imgFile = imgFile;}
	
	/**
	 * 投稿文
	 */
	@RemoteProperty
	private String message;
	public  String getMessage() {return message;}
	public  void   setMessage(String message) {this.message = message;}
	
	/**
	 * タグID
	 */
	@RemoteProperty
	private ArrayList<Integer> tagId;
	public  ArrayList<Integer> getTagId() {return tagId;}
	public  void setTagId(ArrayList<Integer> tagId) {this.tagId = tagId;}
	
	/**
	 * タグ
	 */
	@RemoteProperty
	private ArrayList<String> tagName;
	public  ArrayList<String> getTagName() {return tagName;}
	public  void setTagName(ArrayList<String> tagName) {this.tagName = tagName;}
	
	/**
	 * 投稿日時
	 */
	@RemoteProperty
	private Timestamp postDate;
	public  Timestamp getPostDate() {return postDate;}
	public  void setPostDate(Timestamp postDate) {this.postDate = postDate;}
	
	/**
	 * 高評価数
	 */
	@RemoteProperty
	private int  good;
	public  int  getGood() {return good;}
	public  void setGood(int good) {this.good = good;}
	
	/**
	 * 低評価数
	 */
	@RemoteProperty
	private int  bad;
	public  int  getBad() {return bad;}
	public  void setBad(int bad) {this.bad = bad;}
	
	/**
	 * コメント数
	 */
	@RemoteProperty
	private int  commentNum;
	public  int  getCommentNum() {return commentNum;}
	public  void setCommentNum(int commentNum) {this.commentNum = commentNum;}
	
	/**
	 * 高評価フラグ
	 * 
	 * ユーザーが高評価・低評価をしているか
	 * 0 未評価、1 高評価、2 低評価
	 */
	@RemoteProperty
	private int  isGood;
	public  int  getIsGood() {return isGood;}
	public  void setIsGood(int isGood) {this.isGood = isGood;}
	
	/**
	 * コンストラクタ
	 */
	public MobilePostBean() {}
	
	/**
	 * 緯度経度から投稿リストを取得する
	 * 
	 * 地図指定閲覧のときに使う
	 * 
	 * データベース処理
	 * まずは、その範囲の投稿の数を調べる
	 * その範囲の投稿の、ユーザーIDのリストを取得する
	 * 
	 * 投稿テーブル、ユーザーテーブル、写真テーブル、
	 * タグ管理テーブル、コメントテーブル、評価テーブル
	 * それぞれから項目を取得する
	 * 
	 * 投稿の配列を生成し、投稿テーブルから取得した項目をセット
	 * 連想配列には、投稿IDと配列の要素番号をセットしておく
	 * 
	 * 他のテーブルから取得した項目は、連想配列を利用して
	 * 適切な投稿にセットする
	 * 
	 * 鍵つきユーザーの投稿をチェックした後、ArrayListにして返す
	 * 
	 * @param userId      鍵チェックに使うユーザーID
	 * @param startLat    始点緯度
	 * @param startLng    始点経度
	 * @param endLat      終点緯度
	 * @param endLng      終点経度
	 * @param startPostId 取得開始投稿ID
	 * @return 投稿リスト
	 */
	@RemoteMethod
	public static ArrayList<MobilePostBean> getPostByPosition
	  (String userId, String startLat, String startLng, String endLat, String endLng,
	   int startPostId)
	{
		// 戻り値 投稿リスト
		ArrayList<MobilePostBean> postList = new ArrayList<MobilePostBean>();
		
		// 日付変更線をまたいでいる場合の補正
		if (Float.parseFloat(startLng) > Float.parseFloat(endLng)) {
			endLng = "180";
		}
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps  = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4 = null;
		PreparedStatement ps5 = null;
		PreparedStatement ps6 = null;
		PreparedStatement ps7 = null;
		ResultSet rs  = null;
		ResultSet rs2 = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// 投稿数
			int postNum = 0;
			
			// ユーザーIDのリスト
			ArrayList<String> uIdList = new ArrayList<String>();
			
			// 投稿テーブルから位置情報の範囲の
			// ユーザーIDごとの投稿数を取得
			// MBRContains(A, B)で AとBの当たり判定
			// GeomFromText(文字列)で 文字列をGEOMETRY型に変換
			// LineString(C経度 C緯度, D経度 D緯度)で CとDの直線の文字列
			String sql = "SELECT COUNT(*) AS uPostNum, userid FROM posts " +
			  "WHERE MBRContains(GeomFromText(?), latlng) ";
			// 取得開始投稿IDの指定
			if (startPostId != 0) {
				sql += "AND postid < ? ";
			}
			sql += "GROUP BY userid";
			ps = db.prepareStatement(sql);
			
			// 始点終点の緯度経度を ps にセット
			ps.setString(1, 
			  "LineString(" + startLng + " " + startLat +
			  ", " + endLng + " " + endLat + ")");
			
			// 取得開始投稿IDをセット
			if (startPostId != 0) {
				ps.setInt(2, startPostId);
			}
			
			rs = ps.executeQuery();
			while (rs.next()) {
				
				// 投稿数を取得
				postNum += rs.getInt("uPostNum");
				
				// ユーザーIDを取得してリストに追加
				uIdList.add(rs.getString("userid"));
			}
			// 投稿数が 0 なら終了
			if (postNum == 0) {
				throw new Exception("投稿0です");
			}
			// 投稿数が 10 より多いなら10に
			else if (postNum > 10) {
				postNum = 10;
			}
			
			// ループカウンタに使うために投稿数を 1 減らしておく
			postNum--;
			
			// 投稿テーブルから位置情報の範囲の投稿を取得
			// GEOMETRY型を取り出すときは X()で経度 Y()緯度
			// MBRContains(A, B)で AとBの当たり判定
			// GeomFromText(文字列)で 文字列をGEOMETRY型に変換
			// LineString(C経度 C緯度, D経度 D緯度)で CとDの直線の文字列
			sql = "SELECT postid, userid, meshcode, Y(latlng) AS lat, X(latlng) AS lng, " +
			  "message, postdate FROM posts " +
			  "WHERE MBRContains(GeomFromText(?), latlng) ";
			// 取得開始投稿IDの指定
			if (startPostId != 0) {
				sql += "AND postid < ? ";
			}
			sql += "ORDER BY postid DESC LIMIT 10";
			ps = db.prepareStatement(sql);
			
			// 始点終点の緯度経度を ps にセット
			ps.setString(1, 
			  "LineString(" + startLng + " " + startLat +
			  ", " + endLng + " " + endLat + ")");
			
			// 取得開始投稿IDをセット
			if (startPostId != 0) {
				ps.setInt(2, startPostId);
			}
			
			rs = ps.executeQuery();
			
			// ユーザーテーブルからユーザーIDで
			// 表示名、アイコン、鍵つきフラグを取得
			sql = "SELECT userid, dispname, iconfile, islocked " +
			  "FROM users WHERE userid IN (?";
			// (ユーザーIDの数 - 1)だけ ? を追加
			for (int i=1; i<uIdList.size(); i++) {
				sql += ", ?";
			}
			sql += ")";
			ps2 = db.prepareStatement(sql);
			// ユーザーIDを ps2 にセット
			for (int i=0; i<uIdList.size(); i++) {
				ps2.setString((i + 1), uIdList.get(i));
			}
			rs2 = ps2.executeQuery();
			
			// 写真テーブルから投稿IDで画像ファイル名を取得
			sql = "SELECT postid, imgfile FROM pictures WHERE postid IN (?";
			// 投稿数だけ ? を追加
			for (int i=0; i<postNum; i++) {
				sql += ", ?";
			}
			sql += ")";
			ps3 = db.prepareStatement(sql);
			
			// タグ管理テーブルから投稿IDでタグIDを取得
			// タグテーブルからタグIDでタグを取得
			sql = "SELECT tags.tagid AS tagid, tags.tag AS tag, p.postid AS postid " +
			  "FROM tags JOIN (SELECT postid, tagid FROM tagpost WHERE postid IN (?";
			// 投稿数だけ ? を追加
			for (int i=0; i<postNum; i++) {
				sql += ", ?";
			}
			sql += ")) AS p";
			ps4 = db.prepareStatement(sql);
			
			// コメントテーブルから投稿IDでコメント数を取得
			sql = "SELECT COUNT(*) AS commentnum, postid FROM comments WHERE postid IN (?";
			// 投稿数だけ ? を追加
			for (int i=0; i<postNum; i++) {
				sql += ", ?";
			}
			sql += ") GROUP BY postid";
			ps5 = db.prepareStatement(sql);
			
			// 評価テーブルから投稿IDで評価を取得
			sql = "SELECT COUNT(*), postid, good FROM goodbad WHERE postid IN (?";
			// 投稿数だけ ? を追加
			for (int i=0; i<postNum; i++) {
				sql += ", ?";
			}
			sql += ") GROUP BY postid, good";
			ps6 = db.prepareStatement(sql);
			
			// 評価テーブルから投稿IDとユーザーIDでそのユーザーの評価を取得
			sql = "SELECT postid, good FROM goodbad WHERE postid IN (?";
			// 投稿数だけ ? を追加
			for (int i=0; i<postNum; i++) {
				sql += ", ?";
			}
			sql += ") AND userid = ? GROUP BY postid, good";
			ps7 = db.prepareStatement(sql);
			
			// ユーザーのIDと
			// 表示名、アイコン、鍵つきフラグを連想配列に
			HashMap<String, UserDataL> userMap = new HashMap<String, UserDataL>();
			
			// ユーザーの表示名、アイコン、鍵つきフラグを取得
			while (rs2.next()) {
				String id    = rs2.getString ("userid");
				String name  = rs2.getString ("dispname");
				String icon  = rs2.getString ("iconfile");
				boolean lock = rs2.getBoolean("islocked");
				
				// 表示名、アイコン、鍵つきフラグをセット
				UserDataL ud = new UserDataL(name, icon, lock);
				
				// 連想配列に格納
				userMap.put(id, ud);
			}
			
			// 投稿数を 1 戻す
			postNum++;
			
			// MobilePostBeanの配列を投稿数だけ生成
			MobilePostBean[] post = new MobilePostBean[postNum];
			
			// ループ変数 MobilePostBeanの配列に使う
			int i = 0;
			
			// 投稿IDとループ変数 i を連想配列に
			HashMap<Integer, Integer> postMap = new HashMap<Integer, Integer>();
			
			while (rs.next()) {
				
				// MobilePostBeanを生成
				post[i] = new MobilePostBean();
				
				// 投稿ID
				int id = rs.getInt("postid");
				
				post[i].setPostId  (id);
				post[i].setUserId  (rs.getString   ("userid"));
				post[i].setMeshCode(rs.getInt      ("meshcode"));
				post[i].setLat     (rs.getFloat    ("lat"));
				post[i].setLng     (rs.getFloat    ("lng"));
				post[i].setPostDate(rs.getTimestamp("postdate"));
				
				// 投稿文のタグにリンクをつける
				String linkMessage = TagParser.setTagLink(rs.getString("message"));
				post[i].setMessage(linkMessage);
				
				// 表示名、アイコン、鍵つきフラグを連想配列から取得
				UserDataL ud = userMap.get(rs.getString("userid"));
				
				// 表示名、アイコン、鍵つきフラグをセット
				post[i].setDispName(ud.dispName);
				post[i].setIconFile(ud.iconFile);
				post[i].setIsLocked(ud.isLocked);
				
				// 画像ファイル名リストを用意
				ArrayList<String> imgFileList = new ArrayList<String>();
				
				// 画像ファイル名リストをセット
				post[i].setImgFile(imgFileList);
				
				// タグIDリスト、タグリストを用意
				ArrayList<Integer> tagIdList = new ArrayList<Integer>();
				ArrayList<String>  tagNameList   = new ArrayList<String>();
				
				// タグIDリストをセット
				post[i].setTagId(tagIdList);
				// タグリストをセット
				post[i].setTagName(tagNameList);
				
				// ps3 ps4 ps5 ps6 ps7 に投稿IDをセット
				ps3.setInt((i+1), id);
				ps4.setInt((i+1), id);
				ps5.setInt((i+1), id);
				ps6.setInt((i+1), id);
				ps7.setInt((i+1), id);
				
				// 連想配列に投稿IDとループ変数を格納
				postMap.put(id, i);
				
				i++;
			}
			
			// 投稿が足りない
			for ( ; i<postNum; i++) {
				// ps3 ps4 ps5 ps6 ps7 の数合わせ
				ps3.setInt((i+1), 0);
				ps4.setInt((i+1), 0);
				ps5.setInt((i+1), 0);
				ps6.setInt((i+1), 0);
				ps7.setInt((i+1), 0);
			}
			
			// ps7 にユーザーIDをセット
			ps7.setString((i+1), userId);
			
			// 画像ファイル名を取得
			rs = ps3.executeQuery();
			while (rs.next()) {
				
				// 投稿IDを取得
				int id = rs.getInt("postid");
				// 画像ファイル名を取得
				String imgFile = rs.getString("imgfile");
				
				// 連想配列からループ変数を取り出す
				int index = postMap.get(id);
				
				// 画像ファイル名を追加
				post[index].addImgFile(imgFile);
			}
			
			// タグを取得
			rs = ps4.executeQuery();
			while (rs.next()) {
				
				// 投稿IDを取得
				int id = rs.getInt("postid");
				// タグIDを取得
				int tId = rs.getInt("tagid");
				// タグを取得
				String tName = rs.getString("tag");
				
				// 連想配列からループ変数を取り出す
				int index = postMap.get(id);
				
				// タグIDを追加
				post[index].addTagId(tId);
				// タグを追加
				post[index].addTagName(tName);
			}
			
			// コメント数を取得
			rs = ps5.executeQuery();
			while (rs.next()) {
				
				// 投稿IDを取得
				int id = rs.getInt("postid");
				// コメント数を取得
				int cNum = rs.getInt("commentnum");
				
				// 連想配列からループ変数を取り出す
				int index = postMap.get(id);
				
				// コメント数をセット
				post[index].setCommentNum(cNum);
			}
			
			// 評価を取得
			rs = ps6.executeQuery();
			while (rs.next()) {
				
				// 投稿IDを取得
				int id = rs.getInt("postid");
				
				// 連想配列からループ変数を取り出す
				int index = postMap.get(id);
				
				// 高評価
				if (rs.getInt("good") == 1) {
					post[index].setGood(rs.getInt(1));
				}
				
				// 低評価
				else if (rs.getInt("good") == 2) {
					post[index].setBad(rs.getInt(1));
				}
			}
			
			// 高評価フラグを取得
			rs = ps7.executeQuery();
			while (rs.next()) {
				
				// 投稿IDを取得
				int id = rs.getInt("postid");
				
				// 連想配列からループ変数を取り出す
				int index = postMap.get(id);
				
				// 高評価フラグをセット
				post[index].setIsGood(rs.getInt("good"));
			}
			
			// ユーザーを取得
			UserBean key = UserBean.getUserByUserId(userId);
			
			// 投稿リストに追加
			for (int j=0; j<postNum; j++) {
				
				// 鍵チェック
				if (post[j].getIsLocked() &&
				    !userId.equals(post[j].getUserId()) &&
				    (!key.isFollowing(post[j].getUserId()))) {
					continue;
				}
				
				postList.add(post[j]);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (rs2 != null) {rs2.close();}
				if (rs  != null) {rs .close();}
				if (ps7 != null) {ps7.close();}
				if (ps6 != null) {ps6.close();}
				if (ps5 != null) {ps5.close();}
				if (ps4 != null) {ps4.close();}
				if (ps3 != null) {ps3.close();}
				if (ps2 != null) {ps2.close();}
				if (ps  != null) {ps .close();}
				if (db  != null) {db .close();}
			} catch (Exception e) {}
		}
		
		return postList;
	}
	
	/**
	 * 投稿IDリストから投稿リストを取得する
	 * 
	 * タグ検索時に使う
	 * 
	 * データベース処理
	 * 投稿IDリストから投稿を取得し、
	 * ユーザーIDのリストも取得する
	 * 
	 * 投稿テーブル、ユーザーテーブル、写真テーブル、
	 * タグ管理テーブル、コメントテーブル、評価テーブル
	 * それぞれから項目を取得する
	 * 
	 * 投稿の配列を生成し、投稿テーブルから取得した項目をセット
	 * 連想配列には、投稿IDと配列の要素番号をセットしておく
	 * 
	 * 他のテーブルから取得した項目は、連想配列を利用して
	 * 適切な投稿にセットする
	 * 
	 * 鍵つきユーザーの投稿をチェックした後、ArrayListにして返す
	 * 
	 * @param userId     鍵チェックに使うユーザーID
	 * @param postIdList 投稿IDリスト
	 * @return 投稿リスト
	 */
	public static ArrayList<MobilePostBean> getPostByPostIdList
	  (String userId, ArrayList<Integer> postIdList)
	{
		
		// 戻り値 投稿リスト
		ArrayList<MobilePostBean> postList = new ArrayList<MobilePostBean>();
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps  = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4 = null;
		PreparedStatement ps5 = null;
		PreparedStatement ps6 = null;
		PreparedStatement ps7 = null;
		ResultSet rs  = null;
		ResultSet rs2 = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// 投稿数
			int postNum = postIdList.size();
			
			// 投稿数が 0 なら終了
			if (postNum == 0) {
				throw new Exception("投稿0です");
			}
			
			// 投稿数が 10 より多いなら10に
			else if (postNum > 10) {
				Collections.sort(postIdList);
				postNum = 10;
			}
			
			// ユーザーIDのリスト
			ArrayList<String> uIdList = new ArrayList<String>();
			
			// 投稿テーブルから投稿IDを含むユーザーIDを取得
			String sql = "SELECT userid FROM posts " +
			  "WHERE postId IN (?";
			// (投稿数 - 1)だけ ? を追加
			for (int i=1; i<postNum; i++) {
				sql += ", ?";
			}
			sql += ")";
			ps = db.prepareStatement(sql);
			
			// 投稿IDを ps にセット
			for (int i=0; i<postNum; i++) {
				ps.setInt((i + 1), postIdList.get(i));
			}
			
			rs = ps.executeQuery();
			while (rs.next()) {
				// ユーザーIDを取得してリストに追加
				uIdList.add(rs.getString("userid"));
			}
			
			// 投稿テーブルから投稿IDを含む投稿を取得
			sql = "SELECT postid, userid, meshcode, Y(latlng) AS lat, " +
			  "X(latlng) AS lng, message, postdate FROM posts " +
			  "WHERE postId IN (?";
			// (投稿数 - 1)だけ ? を追加
			for (int i=1; i<postNum; i++) {
				sql += ", ?";
			}
			sql += ") ORDER BY postid DESC";
			ps = db.prepareStatement(sql);
			
			// 投稿IDを ps にセット
			for (int i=0; i<postNum; i++) {
				ps.setInt((i + 1), postIdList.get(i));
			}
			
			rs = ps.executeQuery();
			
			// ユーザーテーブルからユーザーIDで
			// 表示名、アイコン、鍵つきフラグを取得
			sql = "SELECT userid, dispname, iconfile, islocked " +
			  "FROM users WHERE userid IN (?";
			// (ユーザーIDの数 - 1)だけ ? を追加
			for (int i=1; i<uIdList.size(); i++) {
				sql += ", ?";
			}
			sql += ")";
			ps2 = db.prepareStatement(sql);
			// ユーザーIDを ps2 にセット
			for (int i=0; i<uIdList.size(); i++) {
				ps2.setString((i + 1), uIdList.get(i));
			}
			rs2 = ps2.executeQuery();
			
			// ループカウンタに使うために投稿数を 1 減らしておく
			postNum--;
			
			// 写真テーブルから投稿IDで画像ファイル名を取得
			sql = "SELECT postid, imgfile FROM pictures WHERE postid IN (?";
			// 投稿数だけ ? を追加
			for (int i=0; i<postNum; i++) {
				sql += ", ?";
			}
			sql += ")";
			ps3 = db.prepareStatement(sql);
			
			// タグ管理テーブルから投稿IDでタグIDを取得
			// タグテーブルからタグIDでタグを取得
			sql = "SELECT tags.tagid AS tagid, tags.tag AS tag, p.postid AS postid " +
			  "FROM tags JOIN (SELECT postid, tagid FROM tagpost WHERE postid IN (?";
			// 投稿数だけ ? を追加
			for (int i=0; i<postNum; i++) {
				sql += ", ?";
			}
			sql += ")) AS p";
			ps4 = db.prepareStatement(sql);
			
			// コメントテーブルから投稿IDでコメント数を取得
			sql = "SELECT COUNT(*) AS commentnum, postid FROM comments WHERE postid IN (?";
			// 投稿数だけ ? を追加
			for (int i=0; i<postNum; i++) {
				sql += ", ?";
			}
			sql += ") GROUP BY postid";
			ps5 = db.prepareStatement(sql);
			
			// 評価テーブルから投稿IDで評価を取得
			sql = "SELECT COUNT(*), postid, good FROM goodbad WHERE postid IN (?";
			// 投稿数だけ ? を追加
			for (int i=0; i<postNum; i++) {
				sql += ", ?";
			}
			sql += ") GROUP BY postid, good";
			ps6 = db.prepareStatement(sql);
			
			// 評価テーブルから投稿IDとユーザーIDでそのユーザーの評価を取得
			sql = "SELECT postid, good FROM goodbad WHERE postid IN (?";
			// 投稿数だけ ? を追加
			for (int i=0; i<postNum; i++) {
				sql += ", ?";
			}
			sql += ") AND userid = ? GROUP BY postid, good";
			ps7 = db.prepareStatement(sql);
			
			// ユーザーのIDと
			// 表示名、アイコン、鍵つきフラグを連想配列に
			HashMap<String, UserDataL> userMap = new HashMap<String, UserDataL>();
			
			// ユーザーの表示名、アイコン、鍵つきフラグを取得
			while (rs2.next()) {
				String id    = rs2.getString ("userid");
				String name  = rs2.getString ("dispname");
				String icon  = rs2.getString ("iconfile");
				boolean lock = rs2.getBoolean("islocked");
				
				// 表示名、アイコン、鍵つきフラグをセット
				UserDataL ud = new UserDataL(name, icon, lock);
				
				// 連想配列に格納
				userMap.put(id, ud);
			}
			
			// 投稿数を 1 戻す
			postNum++;
			
			// MobilePostBeanの配列を投稿数だけ生成
			MobilePostBean[] post = new MobilePostBean[postNum];
			
			// ループ変数 MobilePostBeanの配列に使う
			int i = 0;
			
			// 投稿IDとループ変数 i を連想配列に
			HashMap<Integer, Integer> postMap = new HashMap<Integer, Integer>();
			
			while (rs.next()) {
				
				// MobilePostBeanを生成
				post[i] = new MobilePostBean();
				
				// 投稿ID
				int id = rs.getInt("postid");
				
				post[i].setPostId  (id);
				post[i].setUserId  (rs.getString   ("userid"));
				post[i].setMeshCode(rs.getInt      ("meshcode"));
				post[i].setLat     (rs.getFloat    ("lat"));
				post[i].setLng     (rs.getFloat    ("lng"));
				post[i].setPostDate(rs.getTimestamp("postdate"));
				
				// 投稿文のタグにリンクをつける
				String linkMessage = TagParser.setTagLink(rs.getString("message"));
				post[i].setMessage(linkMessage);
				
				// 表示名、アイコン、鍵つきフラグを連想配列から取得
				UserDataL ud = userMap.get(rs.getString("userid"));
				
				// 表示名、アイコン、鍵つきフラグをセット
				post[i].setDispName(ud.dispName);
				post[i].setIconFile(ud.iconFile);
				post[i].setIsLocked(ud.isLocked);
				
				// 画像ファイル名リストを用意
				ArrayList<String> imgFileList = new ArrayList<String>();
				
				// 画像ファイル名リストをセット
				post[i].setImgFile(imgFileList);
				
				// タグIDリスト、タグリストを用意
				ArrayList<Integer> tagIdList = new ArrayList<Integer>();
				ArrayList<String>  tagNameList   = new ArrayList<String>();
				
				// タグIDリストをセット
				post[i].setTagId(tagIdList);
				// タグリストをセット
				post[i].setTagName(tagNameList);
				
				// ps3 ps4 ps5 ps6 ps7 に投稿IDをセット
				ps3.setInt((i+1), id);
				ps4.setInt((i+1), id);
				ps5.setInt((i+1), id);
				ps6.setInt((i+1), id);
				ps7.setInt((i+1), id);
				
				// 連想配列に投稿IDとループ変数を格納
				postMap.put(id, i);
				
				i++;
			}
			
			// 投稿が足りない
			for ( ; i<postNum; i++) {
				// ps3 ps4 ps5 ps6 ps7 の数合わせ
				ps3.setInt((i+1), 0);
				ps4.setInt((i+1), 0);
				ps5.setInt((i+1), 0);
				ps6.setInt((i+1), 0);
				ps7.setInt((i+1), 0);
			}
			
			// ps7 にユーザーIDをセット
			ps7.setString((i+1), userId);
			
			// 画像ファイル名を取得
			rs = ps3.executeQuery();
			while (rs.next()) {
				
				// 投稿IDを取得
				int id = rs.getInt("postid");
				// 画像ファイル名を取得
				String imgFile = rs.getString("imgfile");
				
				// 連想配列からループ変数を取り出す
				int index = postMap.get(id);
				
				// 画像ファイル名を追加
				post[index].addImgFile(imgFile);
			}
			
			// タグを取得
			rs = ps4.executeQuery();
			while (rs.next()) {
				
				// 投稿IDを取得
				int id = rs.getInt("postid");
				// タグIDを取得
				int tId = rs.getInt("tagid");
				// タグを取得
				String tName = rs.getString("tag");
				
				// 連想配列からループ変数を取り出す
				int index = postMap.get(id);
				
				// タグIDを追加
				post[index].addTagId(tId);
				// タグを追加
				post[index].addTagName(tName);
			}
			
			// コメント数を取得
			rs = ps5.executeQuery();
			while (rs.next()) {
				
				// 投稿IDを取得
				int id = rs.getInt("postid");
				// コメント数を取得
				int cNum = rs.getInt("commentnum");
				
				// 連想配列からループ変数を取り出す
				int index = postMap.get(id);
				
				// コメント数をセット
				post[index].setCommentNum(cNum);
			}
			
			// 評価を取得
			rs = ps6.executeQuery();
			while (rs.next()) {
				
				// 投稿IDを取得
				int id = rs.getInt("postid");
				
				// 連想配列からループ変数を取り出す
				int index = postMap.get(id);
				
				// 高評価
				if (rs.getInt("good") == 1) {
					post[index].setGood(rs.getInt(1));
				}
				
				// 低評価
				else if (rs.getInt("good") == 2) {
					post[index].setBad(rs.getInt(1));
				}
			}
			
			// 高評価フラグを取得
			rs = ps7.executeQuery();
			while (rs.next()) {
				
				// 投稿IDを取得
				int id = rs.getInt("postid");
				
				// 連想配列からループ変数を取り出す
				int index = postMap.get(id);
				
				// 高評価フラグをセット
				post[index].setIsGood(rs.getInt("good"));
			}
			
			UserBean key = UserBean.getUserByUserId(userId);
			
			// 投稿リストに追加
			for (int j=0; j<postNum; j++) {
				
				// 鍵チェック
				if (post[j].getIsLocked() &&
				    !userId.equals(post[j].getUserId()) &&
				    (!key.isFollowing(post[j].getUserId()))) {
					continue;
				}
				
				postList.add(post[j]);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (rs2 != null) {rs2.close();}
				if (rs  != null) {rs .close();}
				if (ps7 != null) {ps7.close();}
				if (ps6 != null) {ps6.close();}
				if (ps5 != null) {ps5.close();}
				if (ps4 != null) {ps4.close();}
				if (ps3 != null) {ps3.close();}
				if (ps2 != null) {ps2.close();}
				if (ps  != null) {ps .close();}
				if (db  != null) {db .close();}
			} catch (Exception e) {}
		}
		
		return postList;
	}
	
	/**
	 * 1人のユーザーの投稿リストを取得する
	 * 
	 * ユーザー情報閲覧で使う
	 * 
	 * @param userId       ユーザーID
	 * @param targetUserId 対象のユーザーID
	 * @param startPostId  取得開始投稿ID
	 * @return 投稿リスト
	 */
	@RemoteMethod
	public static ArrayList<MobilePostBean> getPostByUserId
	  (String userId, String targetUserId, int startPostId)
	{
		// getPostByUserIdListに対象のユーザーID 1つだけを入れて投稿リストを取得する
		ArrayList<String> list = new ArrayList<String>();
		list.add(targetUserId);
		return getPostByUserIdList(userId, list, startPostId);
	}
	
	/**
	 * 自分とフォローの投稿リストを取得する
	 * 
	 * タイムライン友達版で使う
	 * 
	 * @param userId      ユーザーID
	 * @param startPostId 取得開始投稿ID
	 * @return 投稿リスト
	 */
	@RemoteMethod
	public static ArrayList<MobilePostBean> getPostByFriend
	  (String userId, int startPostId)
	{
		// ユーザーを取得
		UserBean user = UserBean.getUserByUserId(userId);
		
		// フォローリストを取得
		ArrayList<String> list = user.getFollowId();
		
		// ユーザーを加える
		list.add(userId);
		
		// getPostByUserIdListにリストを入れて投稿リストを取得する
		return getPostByUserIdList(userId, list, startPostId);
	}
	
	/**
	 * ユーザーIDリストから投稿リストを取得する
	 * 
	 * データベース処理
	 * まずは、ユーザーの投稿の数を調べる
	 * 
	 * 投稿テーブル、ユーザーテーブル、写真テーブル、
	 * タグ管理テーブル、コメントテーブル、評価テーブル
	 * それぞれから項目を取得する
	 * 
	 * 投稿の配列を生成し、投稿テーブルから取得した項目をセット
	 * 連想配列には、投稿IDと配列の要素番号をセットしておく
	 * 
	 * 他のテーブルから取得した項目は、連想配列を利用して
	 * 適切な投稿にセットする
	 * 
	 * 投稿の配列をArrayListにして返す
	 * 
	 * @param userId      ユーザーID
	 * @param list        ユーザーIDリスト
	 * @param startPostId 取得開始投稿ID
	 * @return 投稿リスト
	 */
	@RemoteMethod
	public static ArrayList<MobilePostBean> getPostByUserIdList
	  (String userId, ArrayList<String> list, int startPostId)
	{
		// 戻り値 投稿リスト
		ArrayList<MobilePostBean> postList = new ArrayList<MobilePostBean>();
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps  = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		PreparedStatement ps4 = null;
		PreparedStatement ps5 = null;
		PreparedStatement ps6 = null;
		PreparedStatement ps7 = null;
		ResultSet rs  = null;
		ResultSet rs2 = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// ユーザーの数
			int userNum = list.size();
			
			// 投稿数
			int postNum = 0;
			
			// 投稿テーブルからユーザーの投稿数を取得
			String sql = "SELECT COUNT(*) FROM posts WHERE userid IN (?";
			// (ユーザーの数 - 1)だけ ? を追加
			for (int i=1; i<userNum; i++) {
				sql += ", ?";
			}
			sql += ")";
			// 取得開始投稿IDの指定
			if (startPostId != 0) {
				sql += " AND postid < ?";
			}
			ps = db.prepareStatement(sql);
			
			// ユーザーIDを ps にセット
			int j = 0;
			for ( ; j<userNum; j++) {
				ps.setString((j + 1), list.get(j));
			}
			// 取得開始投稿IDをセット
			if (startPostId != 0) {
				ps.setInt(j+1, startPostId);
			}
			
			rs = ps.executeQuery();
			if (rs.next()) {
				// 投稿数を取得
				postNum = rs.getInt(1);
			}
			// 投稿数が 0 なら終了
			if (postNum == 0) {
				throw new Exception("投稿0です");
			}
			// 投稿数が 10 より多いなら10に
			else if (postNum > 10) {
				postNum = 10;
			}
			// ループカウンタに使うために投稿数を 1 減らしておく
			postNum--;
			
			// 投稿テーブルからユーザーの投稿を取得
			// GEOMETRY型を取り出すときは X()で経度 Y()緯度
			sql = "SELECT postid, userid, meshcode, Y(latlng) AS lat, X(latlng) AS lng, " +
			  "message, postdate FROM posts WHERE userid IN (?";
			// (ユーザーの数 - 1)だけ ? を追加
			for (int i=1; i<userNum; i++) {
				sql += ", ?";
			}
			sql += ")";
			// 取得開始投稿IDの指定
			if (startPostId != 0) {
				sql += " AND postid < ?";
			}
			sql +=" ORDER BY postid DESC LIMIT 10";
			ps = db.prepareStatement(sql);
			
			// ユーザーIDを ps にセット
			for (j=0; j<userNum; j++) {
				ps.setString((j + 1), list.get(j));
			}
			// 取得開始投稿IDをセット
			if (startPostId != 0) {
				ps.setInt(j+1, startPostId);
			}
			rs = ps.executeQuery();
			
			// ユーザーテーブルからユーザーの
			// 表示名、アイコン、鍵つきフラグを取得
			sql = "SELECT userid, dispname, iconfile, islocked " +
			  "FROM users WHERE userid IN (?";
			// (ユーザーの数 - 1)だけ ? を追加
			for (int i=1; i<userNum; i++) {
				sql += ", ?";
			}
			sql += ")";
			ps2 = db.prepareStatement(sql);
			
			// ユーザーIDを ps2 にセット
			for (int i=0; i<userNum; i++) {
				ps2.setString((i + 1), list.get(i));
			}
			rs2 = ps2.executeQuery();
			
			// 写真テーブルから投稿IDで画像ファイル名を取得
			sql = "SELECT postid, imgfile FROM pictures WHERE postid IN (?";
			// 投稿数だけ ? を追加
			for (int i=0; i<postNum; i++) {
				sql += ", ?";
			}
			sql += ")";
			ps3 = db.prepareStatement(sql);
			
			// タグ管理テーブルから投稿IDでタグIDを取得
			// タグテーブルからタグIDでタグを取得
			sql = "SELECT tags.tagid AS tagid, tags.tag AS tag, p.postid AS postid " +
			  "FROM tags JOIN (SELECT postid, tagid FROM tagpost WHERE postid IN (?";
			// 投稿数だけ ? を追加
			for (int i=0; i<postNum; i++) {
				sql += ", ?";
			}
			sql += ")) AS p";
			ps4 = db.prepareStatement(sql);
			
			// コメントテーブルから投稿IDでコメント数を取得
			sql = "SELECT COUNT(*) AS commentnum, postid FROM comments WHERE postid IN (?";
			// 投稿数だけ ? を追加
			for (int i=0; i<postNum; i++) {
				sql += ", ?";
			}
			sql += ") GROUP BY postid";
			ps5 = db.prepareStatement(sql);
			
			// 評価テーブルから投稿IDで評価を取得
			sql = "SELECT COUNT(*), postid, good FROM goodbad WHERE postid IN (?";
			// 投稿数だけ ? を追加
			for (int i=0; i<postNum; i++) {
				sql += ", ?";
			}
			sql += ") GROUP BY postid, good";
			ps6 = db.prepareStatement(sql);
			
			// 評価テーブルから投稿IDとユーザーIDでそのユーザーの評価を取得
			sql = "SELECT postid, good FROM goodbad WHERE postid IN (?";
			// 投稿数だけ ? を追加
			for (int i=0; i<postNum; i++) {
				sql += ", ?";
			}
			sql += ") AND userid = ? GROUP BY postid, good";
			ps7 = db.prepareStatement(sql);
			
			// ユーザーのIDと
			// 表示名、アイコン、鍵つきフラグを連想配列に
			HashMap<String, UserDataL> userMap = new HashMap<String, UserDataL>();
			
			// ユーザーの表示名、アイコン、鍵つきフラグを取得
			while (rs2.next()) {
				String id    = rs2.getString ("userid");
				String name  = rs2.getString ("dispname");
				String icon  = rs2.getString ("iconfile");
				boolean lock = rs2.getBoolean("islocked");
				
				// 表示名、アイコン、鍵つきフラグをセット
				UserDataL ud = new UserDataL(name, icon, lock);
				
				// 連想配列に格納
				userMap.put(id, ud);
			}
			
			// 投稿数を 1 戻す
			postNum++;
			
			// MobilePostBeanの配列を投稿数だけ生成
			MobilePostBean[] post = new MobilePostBean[postNum];
			
			// ループ変数 MobilePostBeanの配列に使う
			int i = 0;
			
			// 投稿IDとループ変数 i を連想配列に
			HashMap<Integer, Integer> postMap = new HashMap<Integer, Integer>();
			
			while (rs.next()) {
				
				// MobilePostBeanを生成
				post[i] = new MobilePostBean();
				
				// 投稿ID
				int id = rs.getInt("postid");
				
				post[i].setPostId  (id);
				post[i].setUserId  (rs.getString   ("userid"));
				post[i].setMeshCode(rs.getInt      ("meshcode"));
				post[i].setLat     (rs.getFloat    ("lat"));
				post[i].setLng     (rs.getFloat    ("lng"));
				post[i].setPostDate(rs.getTimestamp("postdate"));
				
				// 投稿文のタグにリンクをつける
				String linkMessage = TagParser.setTagLink(rs.getString("message"));
				post[i].setMessage(linkMessage);
				
				// 表示名、アイコン、鍵つきフラグを連想配列から取得
				UserDataL ud = userMap.get(rs.getString("userid"));
				
				// 表示名、アイコン、鍵つきフラグをセット
				post[i].setDispName(ud.dispName);
				post[i].setIconFile(ud.iconFile);
				post[i].setIsLocked(ud.isLocked);
				
				// 画像ファイル名リストを用意
				ArrayList<String> imgFileList = new ArrayList<String>();
				
				// 画像ファイル名リストをセット
				post[i].setImgFile(imgFileList);
				
				// タグIDリスト、タグリストを用意
				ArrayList<Integer> tagIdList = new ArrayList<Integer>();
				ArrayList<String>  tagNameList   = new ArrayList<String>();
				
				// タグIDリストをセット
				post[i].setTagId(tagIdList);
				// タグリストをセット
				post[i].setTagName(tagNameList);
				
				// ps3 ps4 ps5 ps6 ps7 に投稿IDをセット
				ps3.setInt((i+1), id);
				ps4.setInt((i+1), id);
				ps5.setInt((i+1), id);
				ps6.setInt((i+1), id);
				ps7.setInt((i+1), id);
				
				// 連想配列に投稿IDとループ変数を格納
				postMap.put(id, i);
				
				i++;
			}
			
			// 投稿が足りない
			for ( ; i<postNum; i++) {
				// ps3 ps4 ps5 ps6 ps7 の数合わせ
				ps3.setInt((i+1), 0);
				ps4.setInt((i+1), 0);
				ps5.setInt((i+1), 0);
				ps6.setInt((i+1), 0);
				ps7.setInt((i+1), 0);
			}
			
			// ps7 にユーザーIDをセット
			ps7.setString((i+1), userId);
			
			// 画像ファイル名を取得
			rs = ps3.executeQuery();
			while (rs.next()) {
				
				// 投稿IDを取得
				int id = rs.getInt("postid");
				// 画像ファイル名を取得
				String imgFile = rs.getString("imgfile");
				
				// 連想配列からループ変数を取り出す
				int index = postMap.get(id);
				
				// 画像ファイル名を追加
				post[index].addImgFile(imgFile);
			}
			
			// タグを取得
			rs = ps4.executeQuery();
			while (rs.next()) {
				
				// 投稿IDを取得
				int id = rs.getInt("postid");
				// タグIDを取得
				int tId = rs.getInt("tagid");
				// タグを取得
				String tName = rs.getString("tag");
				
				// 連想配列からループ変数を取り出す
				int index = postMap.get(id);
				
				// タグIDを追加
				post[index].addTagId(tId);
				// タグを追加
				post[index].addTagName(tName);
			}
			
			// コメント数を取得
			rs = ps5.executeQuery();
			while (rs.next()) {
				
				// 投稿IDを取得
				int id = rs.getInt("postid");
				// コメント数を取得
				int cNum = rs.getInt("commentnum");
				
				// 連想配列からループ変数を取り出す
				int index = postMap.get(id);
				
				// コメント数をセット
				post[index].setCommentNum(cNum);
			}
			
			// 評価を取得
			rs = ps6.executeQuery();
			while (rs.next()) {
				
				// 投稿IDを取得
				int id = rs.getInt("postid");
				
				// 連想配列からループ変数を取り出す
				int index = postMap.get(id);
				
				// 高評価
				if (rs.getInt("good") == 1) {
					post[index].setGood(rs.getInt(1));
				}
				
				// 低評価
				else if (rs.getInt("good") == 2) {
					post[index].setBad(rs.getInt(1));
				}
			}
			
			// 高評価フラグを取得
			rs = ps7.executeQuery();
			while (rs.next()) {
				
				// 投稿IDを取得
				int id = rs.getInt("postid");
				
				// 連想配列からループ変数を取り出す
				int index = postMap.get(id);
				
				// 高評価フラグをセット
				post[index].setIsGood(rs.getInt("good"));
			}
			
			// 投稿リストに追加
			for (int k=0; k<postNum; k++) {
				postList.add(post[k]);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (rs2 != null) {rs2.close();}
				if (rs  != null) {rs .close();}
				if (ps7 != null) {ps7.close();}
				if (ps6 != null) {ps6.close();}
				if (ps5 != null) {ps5.close();}
				if (ps4 != null) {ps4.close();}
				if (ps3 != null) {ps3.close();}
				if (ps2 != null) {ps2.close();}
				if (ps  != null) {ps .close();}
				if (db  != null) {db .close();}
			} catch (Exception e) {}
		}
		return postList;
	}
	
	/**
	 * タグIDから投稿リストを取得する
	 * 
	 * タグ検索で使う
	 * 
	 * タグ管理テーブルからタグIDで投稿IDのリストを取得し、
	 * getPostByPostIdList に投稿IDのリストを渡す
	 * 
	 * @param userId      鍵チェックに使うユーザーID
	 * @param tagId       タグID
	 * @param startPostId 取得開始投稿ID
	 * @return 投稿リスト
	 */
	@RemoteMethod
	public static ArrayList<MobilePostBean> getPostByTagId
	  (String userId, int tagId, int startPostId)
	{
		
		// 投稿IDのリスト
		ArrayList<Integer> postIdList = new ArrayList<Integer>();
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps  = null;
		ResultSet rs  = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// タグ管理テーブルから指定タグIDを含む
			// 投稿IDのリストを取得
			String sql = "SELECT postid FROM tagpost WHERE tagid = ? ";
			// 取得開始投稿IDの指定
			if (startPostId != 0) {
				sql += "AND postid < ? ";
			}
			sql += "ORDER BY postid DESC LIMIT 10";
			ps = db.prepareStatement(sql);
			ps.setInt(1, tagId);
			// 取得開始投稿IDをセット
			if (startPostId != 0) {
				ps.setInt(2, startPostId);
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				// 投稿IDを取得してリストに追加
				postIdList.add(rs.getInt("postid"));
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (rs  != null) {rs .close();}
				if (ps  != null) {ps .close();}
				if (db  != null) {db .close();}
			} catch (Exception e) {}
		}
		
		// getPostByPostIdList に投稿IDのリストを渡す
		return getPostByPostIdList(userId, postIdList);
	}
	
	/**
	 * 画像ファイル名のリストに追加する
	 * 
	 * @param imgFile 追加する画像ファイル名
	 */
	private void addImgFile(String imgFile) {
		this.imgFile.add(imgFile);
	}
	
	/**
	 * タグIDのリストに追加する
	 * 
	 * @param tagName 追加するタグID
	 */
	private void addTagId(int tagId) {
		this.tagId.add(tagId);
	}
	
	/**
	 * タグのリストに追加する
	 * 
	 * @param tagName 追加するタグ
	 */
	private void addTagName(String tagName) {
		this.tagName.add(tagName);
	}
}

/**
 * ユーザーの表示名、アイコン、鍵つきフラグを持つだけ
 */
class UserDataL {
	public String  dispName;
	public String  iconFile;
	public boolean isLocked;
	public UserDataL(String name, String icon, boolean lock) {
		dispName = name;
		iconFile = icon;
		isLocked = lock;
	}
}

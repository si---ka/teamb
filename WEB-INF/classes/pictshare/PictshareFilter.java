package pictshare;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * フィルタ
 * jsp ディレクトリの中にだけかける
 */
public class PictshareFilter implements Filter {
	
	@Override
	public void init(FilterConfig filterConfig) { }
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
	  throws IOException, ServletException
	{
		System.out.println("フィルタ");
		
		// ログイン画面に遷移
		((HttpServletResponse)response).sendRedirect("/teamb/index.jsp");
		return;
	}
	
	@Override
	public void destroy() { }
}

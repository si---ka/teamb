package pictshare;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * ランキング
 * 
 * 今月ランキングと過去ランキングのプロパティの一部として存在する
 * 
 * 順位、期間高評価数、投稿ID、
 * 投稿ユーザーのID・表示名・アイコン、
 * 画像ファイル名(1枚目のみ)、投稿文、投稿日時をもつ
 */
public class RankingBean implements Serializable {
	
	/**
	 * 順位
	 */
	private int  rank;
	public  int  getRank() {return rank;}
	public  void setRank(int rank) {this.rank = rank;}
	
	/**
	 * 期間高評価数
	 */
	private int  good;
	public  int  getGood() {return good;}
	public  void setGood(int good) {this.good = good;}
	
	/**
	 * 投稿ID
	 */
	private int  postId;
	public  int  getPostId() {return postId;}
	public  void setPostId(int postId) {this.postId = postId;}
	
	/**
	 * 投稿ユーザーID
	 */
	private String userId;
	public  String getUserId() {return userId;}
	public  void   setUserId(String userId) {this.userId = userId;}
	
	/**
	 * 投稿ユーザー表示名
	 */
	private String dispName;
	public  String getDispName() {return dispName;}
	public  void   setDispName(String dispName) {this.dispName = dispName;}
	
	/**
	 * 投稿ユーザーアイコン
	 */
	private String iconFile;
	public  String getIconFile() {return iconFile;}
	public  void   setIconFile(String iconFile) {this.iconFile = iconFile;}
	
	/**
	 * 画像ファイル名
	 */
	private String imgFile;
	public  String getImgFile() {return imgFile;}
	public  void   setImgFile(String imgFile) {this.imgFile = imgFile;}
	
	/**
	 * 投稿文
	 */
	private String message;
	public  String getMessage() {return message;}
	public  void   setMessage(String message) {this.message = message;}
	
	/**
	 * 投稿日時
	 */
	private Timestamp postDate;
	public  Timestamp getPostDate() {return postDate;}
	public  void setPostDate(Timestamp postDate) {this.postDate = postDate;}
	
	/**
	 * コンストラクタ
	 * 
	 * 投稿が削除されたときのエラー用のもので初期化する
	 */
	public RankingBean() {
		dispName = "Not Found";
		iconFile = "def_icon.png";
		imgFile  = "notfound.png";
		message  = "投稿が見つかりません。<br />削除された可能性があります。";
		postDate = new Timestamp(System.currentTimeMillis());
	}
}

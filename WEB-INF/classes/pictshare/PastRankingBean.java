package pictshare;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

/**
 * 過去ランキング
 * 
 * 年、月、ランキング(RankingBeanの配列)をもつ
 */
public class PastRankingBean implements Serializable {
	
	/**
	 * 年
	 */
	private int  year;
	public  int  getYear() {return year;}
	public  void setYear(int year) {this.year = year;}
	
	/**
	 * 月
	 */
	private int  month;
	public  int  getMonth() {return month;}
	public  void setMonth(int month) {this.month = month;}
	
	/**
	 * ランキング
	 */
	private RankingBean[] ranking;
	public  RankingBean[] getRanking() {return ranking;}
	public  void setRanking(RankingBean[] ranking) {this.ranking = ranking;}
	
	/**
	 * コンストラクタ
	 */
	public PastRankingBean() {}
	
	/**
	 * 過去ランキングを設定する
	 * 
	 * 月初に今月ランキング(実行時には先月になってる分)をコピーして、
	 * 過去ランキングとしてデータベースに登録する
	 * 
	 * @param year        年
	 * @param month       月
	 * @param thisRanking 今月ランキング
	 */
	public static void setPastRanking(int year, int month, ThisRankingBean thisRanking) {
		
		// ランキングを取得
		RankingBean[] ranking = thisRanking.getRanking();
		
		// ランキングの個数をカウント
		int count = 100;
		for ( ; count>=1; count--) {
			// count 番目の順位が 0 でなければ count 個ある
			if (ranking[count-1].getRank() != 0) {
				break;
			}
		}
		
		// ランキングがないなら終了
		if (count == 0) {
			return;
		}
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// 過去ランキングテーブルにランキングを登録
			String sql = "INSERT INTO pastranking " +
			  "(rankingyear, rankingmonth, rank, postid, good) VALUES";
			// (ランキングの数 - 1)だけ ? を追加
			for (int i=1; i<count; i++) {
				sql += "(?, ?, ?, ?, ?),";
			}
			sql += "(?, ?, ?, ?, ?)";
			ps = db.prepareStatement(sql);
			// 年、月、順位、投稿ID、期間高評価数を ps にセット
			// 順位は、同じ評価数でも重複しないようにする
			for (int i=0; i<count; i++) {
				ps.setInt(5*i+1, year);
				ps.setInt(5*i+2, month);
				ps.setInt(5*i+3, i+1);
				ps.setInt(5*i+4, ranking[i].getPostId());
				ps.setInt(5*i+5, ranking[i].getGood());
			}
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (ps != null) {ps.close();}
				if (db != null) {db.close();}
			} catch (Exception e) {}
		}
	}
	
	/**
	 * 過去ランキングを取得する
	 * 
	 * @param year  年
	 * @param month 月
	 * @return 過去ランキング
	 */
	public static PastRankingBean getPastRankingByYearMonth(int year, int month) {
		
		// 戻り値
		PastRankingBean pastRanking = new PastRankingBean();
		
		// 投稿IDリスト
		ArrayList<Integer> idList = new ArrayList<Integer>();
		
		// 投稿IDとRankingBean配列の添え字の連想配列
		HashMap<Integer, Integer> idIndexMap = new HashMap<Integer, Integer>();
		
		// 年と月をセット
		pastRanking.setYear (year);
		pastRanking.setMonth(month);
		
		// RankingBeanを生成
		RankingBean[] ranking = new RankingBean[100];
		for (int i=0; i<100; i++) {
			ranking[i] = new RankingBean();
		}
		
		// データベース処理
		Connection db = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/pictshare");
			db = ds.getConnection();
			
			// 過去ランキングテーブルから指定の年月のランキングを取得
			ps = db.prepareStatement("SELECT rank, postid, good FROM pastranking " +
			  "WHERE rankingyear = ? AND rankingmonth = ? ORDER BY rank");
			ps.setInt(1, year);
			ps.setInt(2, month);
			rs = ps.executeQuery();
			
			int i = 0;
			int rank = 1;
			int tempGood = 0;
			while (rs.next()) {
				
				// 投稿ID
				int postId = rs.getInt("postid");
				
				// 期間高評価数
				int good = rs.getInt("good");
				
				// 投稿IDリストに投稿IDをセット
				idList.add(postId);
				
				// 期間高評価数をセット
				ranking[i].setGood(good);
				
				// 同じ順位なら順位そのまま
				// 同じ順位でなければ順位を計算
				if (good != tempGood) {
					rank = (i+1);
					tempGood = good;
				}
				
				// 順位をセット
				ranking[i].setRank(rank);
				
				// 連想配列に投稿IDと配列の添え字をセット
				idIndexMap.put(postId, i);
				
				i++;
			}
			
			// 投稿リストを取得する
			ArrayList<PostBean> post = PostBean.getPostByPostIdList(null, false, idList);
			
			for (int j=0; j<post.size(); j++) {
				
				// 投稿IDを取得
				int postId = post.get(j).getPostId();
				
				// 投稿IDが 0 ならスキップ
				if (postId == 0) {
					continue;
				}
				
				// 連想配列から配列の添え字を取得
				int index = idIndexMap.get(postId);
				
				// 投稿IDなどをセット
				ranking[index].setPostId  (postId);
				ranking[index].setUserId  (post.get(j).getUserId());
				ranking[index].setDispName(post.get(j).getDispName());
				ranking[index].setIconFile(post.get(j).getIconFile());
				ranking[index].setImgFile (post.get(j).getImgFile().get(0));
				ranking[index].setMessage (post.get(j).getMessage());
				ranking[index].setPostDate(post.get(j).getPostDate());
			}
			
			// ランキングをセット
			pastRanking.setRanking(ranking);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (rs != null) {rs.close();}
				if (ps != null) {ps.close();}
				if (db != null) {db.close();}
			} catch (Exception e) {}
		}
		
		return pastRanking;
	}
}

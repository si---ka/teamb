<%@ page contentType="text/html; charset=UTF-8" %>

<%--
ランキング

/teamb/pictsharemobile/ranking?y=2015&m=1&s=1 のように、
URLのパラメタで、年、月、表示開始順位を受け取る
パラメタの数値がおかしい場合はサーブレットが修正する
tomcat の時計以降のランキングを表示しようとした場合、今月ランキングを表示する
表示開始順位から 0 ～ 10 個の投稿を表示する

過去ランキングの場合はセッションの pastRanking にランキングがセットされている
pastRanking が null の場合、アプリケーションスコープから今月ランキングを取得する

表示開始順位とランキングの個数によって、前の10位、次の10位へのリンクを表示する
表示開始順位が 2位以降なら、無条件で前の10位へのリンクを表示する
次の10位へのリンクは、次に 1つでも存在すれば表示する

奇数行には oddRow 偶数行には evenRow を指定して色分けする

過去ランキングへのリンクは 2014年 4月から tomcat の時計の月まで表示する
--%>

<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="pictshare.RankingBean" %>
<jsp:useBean id="user" class="pictshare.UserBean" scope="session" />
<jsp:useBean id="pastRanking" class="pictshare.PastRankingBean" scope="session" />
<jsp:useBean id="thisRanking" class="pictshare.ThisRankingBean" scope="application" />
<%
	// YYYY-MM-DD hh:mm 形式
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
%>

<!DOCTYPE html>
<html lang="ja">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
	
	<!-- タイトル -->
	<title>PictShare</title>
	
	<!-- CSS -->
	<!-- ランキング -->
	<link rel="stylesheet" href="/teamb/m/css/ranking.css">
	<!-- メニュー(ヘッダ) -->
	<link rel="stylesheet" href="/teamb/m/css/menu.css">
	<!-- 投稿詳細 -->
	<link rel="stylesheet" href="/teamb/m/css/detail.css">
	<!-- 写真投稿 -->
	<link rel="stylesheet" href="/teamb/m/css/post.css">
	
	<!-- JavaScript -->
	<!-- グーグルマップ -->
	<script src="http://maps.google.com/maps/api/js?v=3&libraries=places&sensor=false"></script>
	<!-- DWR -->
		<!-- コメント -->
		<script src="/teamb/dwr/interface/CommentBean.js"></script>
		<!-- 投稿 -->
		<script src="/teamb/dwr/interface/PostBean.js"></script>
		<!-- 今月ランキング Good用 -->
		<script src="/teamb/dwr/interface/ThisRankingBean.js"></script>
		<!-- DWR本体 -->
		<script src="/teamb/dwr/engine.js"></script>
	<!-- 評価 -->
	<script src="/teamb/js/goodbad.js"></script>
	
	<!-- フォントサイズ -->
	<script src="/teamb/m/js/fontsize.js"></script>
	<!-- メニュー(ヘッダ) -->
	<script src="/teamb/m/js/menu.js"></script>
	<!-- 投稿詳細 -->
	<script src="/teamb/m/js/detail.js"></script>
	<!-- 写真投稿 -->
	<script src="/teamb/m/js/post.js"></script>
	
	<script>
		var userId = "<%= user.getUserId() %>"; // ユーザーID
		
		function init() {
			setFontSize(); // 画面の大きさから文字の大きさを決める
			menuInit();    // メニューの初期設定
			detailInit();  // 投稿詳細の初期設定
			postInit(<%= user.getInitLat() %>,
			  <%= user.getInitLng() %>); // 写真投稿画面のマップの初期設定
		}
	</script>
</head>

<body onload="init()">

<!-- メニュー -->
<%@ include file="/m/jsp/menu.jsp" %>

<!--【ページセンター】ページ上の白ページ-->

<div style='padding:10px;'>
	<div id="posts" class="keisen">
<%
	int year  = 0; // 過去ランキングの年
	int month = 0; // 過去ランキングの月
	String prev = ""; // 前の10位
	String next = ""; // 次の10位
	String row  = ""; // 奇数・偶数行
	String rankYearMonth = "今"; // ランキング名 今月なら今 過去なら xxxx年xx
	
	RankingBean[] ranking; // ランキング
	
	// リクエストに表示開始順位があれば取得
	Integer startRank = (Integer)request.getAttribute("startRank");
	int sr = startRank.intValue();
	
	// 過去ランキングがあれば過去ランキングを取得
	if ((pastRanking != null) && (pastRanking.getYear() != 0)) {
		ranking = pastRanking.getRanking();
		year  = pastRanking.getYear();
		month = pastRanking.getMonth();
		rankYearMonth = year + "年" + month;
	}
	
	// 今月ランキングを取得
	else {
		ranking = thisRanking.getRanking();
	}
	
	// 表示開始順位が1位でなければ前の10位を表示
	if (sr > 1) {
		int prevRank = sr - 10;
		if (prevRank < 1) {
			prevRank = 1;
		}
		prev = "<a class=\"prev10\" href=\"/teamb/pictsharemobile/ranking?";
		if ((year != 0) && (month != 0)) {
			prev += "y=" + year + "&m=" + month + "&";
		}
		prev += "s=" + prevRank + "\">←前の10位</a>";
	}
	
	// 表示開始順位から 1 引いておく
	sr -= 1;
	
	// 次の10位があれば表示
	if ((sr < 90) && (ranking[sr+10].getRank() != 0)) {
		int nextRank = sr + 11;
		if (nextRank > 91) {
			nextRank = 91;
		}
		next = "<a class=\"next10\" href=\"/teamb/pictsharemobile/ranking?";
		if ((year != 0) && (month != 0)) {
			next += "y=" + year + "&m=" + month + "&";
		}
		next += "s=" + nextRank + "\">次の10位→</a>";
	}
%>
		<div id="rankTitle"><%= rankYearMonth %>月のランキング</div>
		<%= prev %>
		<%= next %>
<%
	// 10回ループ
	for (int i=0; i<10; i++) {
		
		// データがなくなれば中止
		if (ranking[sr+i].getRank() == 0) {
			break;
		}
		
		// 奇数・偶数行の設定
		if (i%2 == 0) {
			row = "oddRow";
		} else {
			row = "evenRow";
		}
%>
		<div class="<%= row %>">
			<div class="rankNum">
				<%= ranking[sr+i].getRank() %><span class="rank"> 位</span><br />
				<%= ranking[sr+i].getGood() %><span class="pt"> pt</span>
			</div>
			<div class="rankUser">
				<!--【ユーザーアイコン】-->
				<figure class="user_photo_left">
<%
			// 投稿が削除された場合
			if (ranking[sr+i].getUserId() == null) {
%>
					<img class="userIcon" src="/teamb/img/user/<%= ranking[sr+i].getIconFile() %>" />
<%
			} else {
%>
					<a class="userLink" href="/teamb/pictsharemobile/user/<%= ranking[sr+i].getUserId() %>">
						<img class="userIcon" src="/teamb/img/user/<%= ranking[sr+i].getIconFile() %>" />
					</a>
<%
			}
%>
				</figure>
				
				<!--【ユーザー名】-->
				<div class="user-name">
					<%= ranking[sr+i].getDispName() %>
				</div>
				
				<!--【投稿日時】-->
				<div class="postDate">
					<%= df.format(ranking[sr+i].getPostDate()) %>
				</div>
				
				<!--【文章】-->
				<div class="document">
					<%= ranking[sr+i].getMessage() %>
				</div>
				
				<!--【写真】-->
				<div class="postphoto">
					<img id="<%= ranking[sr+i].getPostId() %>" class="photoImg" src="/teamb/img/post/<%= ranking[sr+i].getImgFile() %>" />
				</div>
			</div>
		</div>
<%
	}
%>
		<%= prev %>
		<%= next %>
	</div>
	
	<div class="pastSelect">
<%
	int startYear  = 2014; // 過去ランキング表示開始年
	int startMonth = 4;    // 過去ランキング表示開始月
	//int endYear    = 2015; // 過去ランキング表示終了年
	//int endMonth   = 1;    // 過去ランキング表示終了月
	Calendar cal = Calendar.getInstance();
	int endYear    = cal.get(Calendar.YEAR);      // 過去ランキング表示終了年
	int endMonth   = cal.get(Calendar.MONTH) + 1; // 過去ランキング表示終了月
	
	for (int y=startYear; y<=endYear; y++) {
%>
		<br />
		<%= y %>年<br />
<%
		int mStart = 1;
		if (y == startYear) {
			mStart = startMonth;
		}
		int mEnd = 12;
		if (y == endYear) {
			mEnd = endMonth;
		}
		for (int m=mStart; m<=mEnd; m++) {
%>
		<a href="/teamb/pictsharemobile/ranking?y=<%= y %>&m=<%= m %>"><%= m %>月</a><br />
<%
		}
	}
%>
	</div>
</div>

<!-- 投稿詳細 -->
<%@ include file="/m/jsp/detail.jsp" %>

<!-- 写真投稿画面 -->
<%@ include file="/m/jsp/post.jsp" %>

</body>
</html>

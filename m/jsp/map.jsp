<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="pictshare.PostBean" %>
<jsp:useBean id="user" class="pictshare.UserBean" scope="session" />
<!DOCTYPE html>
<html lang="ja">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
	
	<!-- タイトル -->
	<title>PictShare</title>
	
	<!-- CSS -->
	<!-- メニュー(ヘッダ) -->
	<link rel="stylesheet" href="/teamb/m/css/menu.css">
	<!-- タイムライン -->
	<link rel="stylesheet" href="/teamb/m/css/timeline.css">
	<!-- 友達閲覧画面 -->
	<link rel="stylesheet" href="/teamb/m/css/main.css">
	<!-- 投稿詳細 -->
	<link rel="stylesheet" href="/teamb/m/css/detail.css">
	<!-- 写真投稿 -->
	<link rel="stylesheet" href="/teamb/m/css/post.css">
	<!-- 地図 -->
	<link rel="stylesheet" href="/teamb/m/css/map.css">

	<!-- JavaScript -->
	<!-- グーグルマップ -->
	<script src="http://maps.google.com/maps/api/js?v=3&libraries=places&sensor=false"></script>
	<!-- DWR -->
		<!-- コメント -->
		<script src="/teamb/dwr/interface/CommentBean.js"></script>
		<!-- 投稿(スマホ用) -->
		<script src="/teamb/dwr/interface/MobilePostBean.js"></script>
		<!-- 投稿 -->
		<script src="/teamb/dwr/interface/PostBean.js"></script>
		<!-- 今月ランキング Good用 -->
		<script src="/teamb/dwr/interface/ThisRankingBean.js"></script>
		<!-- DWR本体 -->
		<script src="/teamb/dwr/engine.js"></script>
	<!-- 評価 -->
	<script src="/teamb/js/goodbad.js"></script>
	
	<!-- フォントサイズ -->
	<script src="/teamb/m/js/fontsize.js"></script>
	<!-- メニュー(ヘッダ) -->
	<script src="/teamb/m/js/menu.js"></script>
	<!-- 写真投稿 -->
	<script src="/teamb/m/js/post.js"></script>
	
	<script src="/teamb/m/js/geoInit.js"></script>
	<script src="/teamb/m/js/geoPin.js"></script>
	<script src="/teamb/m/js/geoGetPost.js"></script>
	<script src="/teamb/m/js/geoTimeline.js"></script>
	<script src="/teamb/m/js/map.js"></script>

	<script>
		var userId = "<%= user.getUserId() %>"; // ユーザーID
		var marker = null; // マーカー
		
		// 画面読み込み完了後に行う初期設定
		function init() {
			setFontSize(); // 画面の大きさから文字の大きさを決める
			menuInit();    // メニューの初期設定
			detailInit();  // 投稿詳細の初期設定
			postInit(<%= user.getInitLat() %>,
			  <%= user.getInitLng() %>); // 写真投稿画面のマップの初期設定
			initialize(<%= user.getInitLat() %>,
			  <%= user.getInitLng() %>); // マップの初期設定
			getPostInit(); // 投稿取得の初期設定
			//getPos();   // 端末の位置情報の取得
			google.maps.event.addListenerOnce(map, 'idle', function(){
					mapUpdate();
				});
		}
	</script>
</head>

<body onload="init()">

<!-- メニュー -->
<%@ include file="/m/jsp/menu.jsp" %>

<!-- マップ -->
<div id="map"></div>
	<input id="mapTextField" class="controls" type="text">
</div>

<!-- 下部に表示するボタン -->
<div id="buttonField" style="height:10%">
	<button type="button" class="submit-button" onClick="timelineDisp()">表示</button>
</div>

<!-- 投稿詳細 -->
<%@ include file="/m/jsp/detail.jsp" %>

<!-- 写真投稿画面 -->
<%@ include file="/m/jsp/post.jsp" %>

</body>
</html>

<%@ page contentType="text/html; charset=UTF-8" %>

<%--
ログイン画面

ログインボタンを押すと /teamb/pictsharemobile/login に ID とパスワードが POST される
エラーがあった場合、リクエストの id に ID がセットされ、
リクエストの message にメッセージがセットされて返ってくる

登録ボタンを押すと /teamb/pictsharemobile/register に
ユーザー名、ユーザーID、パスワード、メールアドレスが POST される
エラーがあった場合、リクエストの rIdに ID、rMail にメールアドレス、
rName にユーザー名、rMessage にメッセージがセットされて返ってくる
その場合は login.js の regError() を実行して、登録画面(id="registerDiv")を表示する
--%>

<%
	String regError = ""; // 登録エラー時に実行するスクリプト用
	
	// リクエストにID（ログイン用）があれば取得
	String id = (String)request.getAttribute("id");
	if (id == null) {
		id = "";
	}
	
	// リクエストにID（登録用）があれば取得
	String rId = (String)request.getAttribute("rId");
	if (rId == null) {
		rId = "";
	}
	
	// リクエストにメールアドレスがあれば取得
	String rMail = (String)request.getAttribute("rMail");
	if (rMail == null) {
		rMail = "";
	}
	
	// リクエストに表示名があれば取得
	String rName = (String)request.getAttribute("rName");
	if (rName == null) {
		rName = "";
	}
	
	// リクエストにメッセージ（ログイン用）があれば取得
	String message = (String)request.getAttribute("message");
	if (message == null) {
		message = "";
	}
	
	// リクエストにメッセージ（登録用）があれば取得
	String rMessage = (String)request.getAttribute("rMessage");
	if (rMessage == null) {
		rMessage = "";
	} else {
		regError = "regError();";
	}
%>

<!DOCTYPE html>
<html lang="ja">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
	
	<!-- タイトル -->
	<title>ログイン画面</title>
	<link rel="icon" href="/teamb/favicon.ico" />
	<link rel="apple-touch-icon" href="/teamb/apple-touch-icon.png" />
	
	<!-- CSS -->
	<!-- ログイン -->
	<link rel="stylesheet" href="/teamb/m/css/login.css">
	
	<!-- JavaScript -->
	<!-- ログイン -->
	<script src="/teamb/m/js/login.js"></script>
	<!-- フォントサイズ -->
	<script src="/teamb/m/js/fontsize.js"></script>
	
	<script>
		// 画面読み込み完了後に行う初期設定
		function init() {
			setFontSize();  // 画面の大きさから文字の大きさを決める
			<%= regError %> // 登録エラー時
		}
	</script>
</head>

<body onload="init()">
	<div class="topDiv">
		<div id="pc"><a href="/teamb/pictsharemobile/pc">PC版へ</a></div>
		<div id="icon"><img src="/teamb/img/login/icon.png" /></div>
	</div>
	
	<div id="mainDiv">
		<div id="loginDiv">
			<div id="welcome">
				ようこそ、<br>Pictshareへ
			</div>
			<div id="loginFormDiv" class="box">
				<div class="topDiv">
					<div class="msg"><%= message %></div>
				</div>
				<form method="POST" action="/teamb/pictsharemobile/login">
					<div>
						<input type="text" name="loginUserID" class="form-field" value="<%= id %>" placeholder="ユーザーID" />
						<input type="password" name="loginPASS" class="form-field" placeholder="パスワード" />
					</div>
					<button type="submit" class="submit-button">ログイン</button>
				</form>
			</div>
		</div>
		<div id="registerDiv">
			<div class="registerTopDiv"></div>
			<div id="registerFormDiv" class="box">
				<div class="topDiv">
					<div class="msg"><%= rMessage %></div>
				</div>
				<form method="POST" action="/teamb/pictsharemobile/register">
					<div>
						<input type="text" name="UserName" class="form-field" value="<%= rName %>" placeholder="ユーザー名" />
						<input type="text" name="UserID" class="form-field" value="<%= rId %>" placeholder="ユーザーID" />
						<input type="password" name="PASS" class="form-field" placeholder="パスワード" />
						<input type="email" name="Mail" class="form-field" value="<%= rMail %>" placeholder="メールアドレス" />
					</div>
					<button type="submit" class="submit-button">登録</button>
				</form>
			</div>
		</div>
	</div>
	
	<div class="bottomDiv">
		<button id="changeButton" onclick="change();">新規登録はコチラ</button>
	</div>
</body>

</html>

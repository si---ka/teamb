<%@ page contentType="text/html; charset=UTF-8" %>

<%--
ユーザー情報閲覧画面

セッションの relation には URL に含まれる対象ユーザーとの関係が
セッションの targetUser には対象ユーザーの情報がセットされている

対象ユーザーのプロフィールの targetprofile.jsp と
対象ユーザーのタイムラインの targettimeline.jsp でほとんどの処理をする
--%>

<jsp:useBean id="user" class="pictshare.UserBean" scope="session" />
<jsp:useBean id="targetUser" class="pictshare.UserBean" scope="session" />
<!DOCTYPE html>
<html lang="ja">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
	
	<!-- タイトル -->
	<title>PictShare</title>
	
	<!-- CSS -->
	<!-- 友達閲覧画面 -->
	<link rel="stylesheet" href="/teamb/m/css/main.css">
	<!-- メニュー(ヘッダ) -->
	<link rel="stylesheet" href="/teamb/m/css/menu.css">
	<!-- プロフィールとおすすめタグ -->
	<link rel="stylesheet" href="/teamb/m/css/profile.css">
	<!-- タイムライン -->
	<link rel="stylesheet" href="/teamb/m/css/timeline.css">
	<!-- 投稿詳細 -->
	<link rel="stylesheet" href="/teamb/m/css/detail.css">
	<!-- フォロー -->
	<link rel="stylesheet" href="/teamb/m/css/follow.css">
	<!-- 写真投稿 -->
	<link rel="stylesheet" href="/teamb/m/css/post.css">
	
	<!-- JavaScript -->
	<!-- グーグルマップ -->
	<script src="http://maps.google.com/maps/api/js?v=3&libraries=places&sensor=false"></script>
	<!-- DWR -->
		<!-- コメント -->
		<script src="/teamb/dwr/interface/CommentBean.js"></script>
		<!-- 投稿(スマホ用) -->
		<script src="/teamb/dwr/interface/MobilePostBean.js"></script>
		<!-- 投稿 -->
		<script src="/teamb/dwr/interface/PostBean.js"></script>
		<!-- 今月ランキング Good用 -->
		<script src="/teamb/dwr/interface/ThisRankingBean.js"></script>
		<!-- ユーザー -->
		<script src="/teamb/dwr/interface/UserBean.js"></script>
		<!-- DWR本体 -->
		<script src="/teamb/dwr/engine.js"></script>
	<!-- 評価 -->
	<script src="/teamb/js/goodbad.js"></script>
	<!-- フォロー -->
	<script src="/teamb/js/follow.js"></script>
	
	<!-- フォントサイズ -->
	<script src="/teamb/m/js/fontsize.js"></script>
	<!-- メニュー(ヘッダ) -->
	<script src="/teamb/m/js/menu.js"></script>
	<!-- 投稿詳細 -->
	<script src="/teamb/m/js/detail.js"></script>
	<!-- ユーザー情報閲覧画面のタイムライン -->
	<script src="/teamb/m/js/targettimeline.js"></script>
	<!-- 写真投稿 -->
	<script src="/teamb/m/js/post.js"></script>
	
	<script>
		var userId       = "<%= user.getUserId() %>";       // ユーザーID
		var targetUserId = "<%= targetUser.getUserId() %>"; // 対象ユーザーID
		var endPostId = 0; // 一番下の投稿ID
		
		function init() {
			setFontSize(); // 画面の大きさから文字の大きさを決める
			menuInit();    // メニューの初期設定
			detailInit();  // 投稿詳細の初期設定
			postInit(<%= user.getInitLat() %>,
			  <%= user.getInitLng() %>); // 写真投稿画面のマップの初期設定
		}
	</script>
</head>

<body onload="init()">

<!-- メニュー -->
<%@ include file="/m/jsp/menu.jsp" %>

<!--【ページセンター】ページ上の白ページ-->
<article>
	<div id="message">
<%
	// リクエストにメッセージがあれば取得
	String message = (String)request.getAttribute("message");
	if (message != null) {
		out.print(message);
	}
%>
	</div>
	
	<!-- 対象ユーザーのプロフィール -->
	<%@ include file="/m/jsp/userdetail/targetprofile.jsp" %>
	
	<div id="timelineTopDiv"><br /></div>
	
	<!-- 対象ユーザーのタイムライン -->
	<%@ include file="/m/jsp/userdetail/targettimeline.jsp" %>
</article>

<!-- 投稿詳細 -->
<%@ include file="/m/jsp/detail.jsp" %>

<!-- 写真投稿画面 -->
<%@ include file="/m/jsp/post.jsp" %>

</body>
</html>

<%@ page contentType="text/html; charset=UTF-8" %>

<%--
対象ユーザーのプロフィール

セッションの relation から対象ユーザーとの関係を取得する
関係によってフォローボタンの表示が変わる

対象ユーザーが自分なら、ボタン非表示
対象ユーザーをフォロー済みなら、フォロー解除
対象ユーザーをフォロー申請中なら、フォロー申請中(クリック不可)
対象ユーザーを未フォローで鍵なしなら、フォローする
対象ユーザーを未フォローで鍵つきなら、フォロー申請を送る

各ボタンを押したときに follow.js の処理を呼び出す
--%>

<%
	String button = ""; // フォローボタン
	
	// セッションから対象ユーザーとの関係を取得
	String relation = (String)session.getAttribute("relation");
	if (relation == null || targetUser.getUserId().equals("null")) {
		relation = "";
	}
	
	// フォロー済み
	if (relation.equals("follow")) {
		button = "<input id=\"btn\" type=\"button\" value=\"フォロー解除\" " +
		  "onclick=\"notfollow('" + user.getUserId() + "', '" + targetUser.getUserId() + "');\" />";
	}
	
	// フォロー申請中
	else if (relation.equals("followWait")) {
		button = "<input id=\"btn\" type=\"button\" value=\"フォロー申請中\" disabled />";
	}
	
	// 未フォロー鍵なし
	else if (relation.equals("notFollow")) {
		button = "<input id=\"btn\" type=\"button\" value=\"フォローする\" " +
		  "onclick=\"follow('" + user.getUserId() + "', '" + targetUser.getUserId() + "');\" />";
	}
	
	// 未フォロー鍵つき
	else if (relation.equals("locked")) {
		button = "<input id=\"btn\" type=\"button\" value=\"フォロー申請を送る\" " +
		  "onclick=\"sendRequest('" + user.getUserId() + "', '" + targetUser.getUserId() + "');\" />";
	}
%>

<!--ここからコードを書く-->
<figure class="photo-left">
	<div style="float: left; width: 20%;">
		<!--【アイコン】-->
		<!--プロフィール画像を表示させる-->
		<a id="icon" href="/teamb/pictsharemobile/user/<%= targetUser.getUserId() %>">
			<img src="/teamb/img/user/<%= targetUser.getIconFile() %>" />
		</a>
	</div>
	
	<div style="float: left; margin-left: 5%;">
		<div id="nameFollow">
			<!--【ユーザー名】-->
			<%= targetUser.getDispName() %><br/>
			
			<!--【Follow フォロー数】-->
			<a href="/teamb/pictsharemobile/user/<%= targetUser.getUserId() %>/follow">Follow</a>
			　　<%= targetUser.getFollowId().size() %><br />
			
			<!--【Follower フォロワー数】-->
			<a href="/teamb/pictsharemobile/user/<%= targetUser.getUserId() %>/follower">Follower</a>
			　<%= targetUser.getFollowerId().size() %>
		</div>
	</div>
	
	<div style="clear: both;">
		<%= button %>
	</div>
</figure>

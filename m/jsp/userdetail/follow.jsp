<%@ page contentType="text/html; charset=UTF-8" %>

<%--
フォロー/フォロワーリスト画面

/teamb/pictsharemobile/user/Piyohiko/follow
/teamb/pictsharemobile/user/Piyohiko/follower など

セッションの relation には URL に含まれる対象ユーザーとの関係が
セッションの targetUser には対象ユーザーの情報が
セッションの targetFollowList と targetFollowerList のどちらかには
対象ユーザーのフォロー/フォロワーリストがセットされている

セットされている方を表示する
フォロー/フォロワーがいなくても、null でなく、要素数が 0 の ArrayList がセットされる
--%>

<%@ page import="java.util.ArrayList" %>
<%@ page import="pictshare.UserBean" %>
<jsp:useBean id="user" class="pictshare.UserBean" scope="session" />
<jsp:useBean id="targetUser" class="pictshare.UserBean" scope="session" />
<%
	String listTitle = ""; // リストのタイトル
	
	// フォローリストを取得
	ArrayList<UserBean> list = (ArrayList<UserBean>)session.getAttribute("targetFollowList");
	
	// フォローリストがある
	if (list != null) {
		listTitle = targetUser.getDispName() + "さんのFollow";
	}
	
	// フォローリストがない
	else {
		// フォロワーリストを取得
		list = (ArrayList<UserBean>)session.getAttribute("targetFollowerList");
		listTitle = targetUser.getDispName() + "さんのFollower";
	}
%>

<!DOCTYPE html>
<html lang="ja">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
	
	<!-- タイトル -->
	<title>PictShare</title>
	
	<!-- CSS -->
	<!-- 友達閲覧画面 -->
	<link rel="stylesheet" href="/teamb/m/css/main.css">
	<!-- メニュー(ヘッダ) -->
	<link rel="stylesheet" href="/teamb/m/css/menu.css">
	<!-- プロフィールとおすすめタグ -->
	<link rel="stylesheet" href="/teamb/m/css/profile.css">
	<!-- フォロー -->
	<link rel="stylesheet" href="/teamb/m/css/follow.css">
	<!-- 写真投稿 -->
	<link rel="stylesheet" href="/teamb/m/css/post.css">
	
	<!-- JavaScript -->
	<!-- グーグルマップ -->
	<script src="http://maps.google.com/maps/api/js?v=3&libraries=places&sensor=false"></script>
	<!-- DWR -->
		<!-- ユーザー -->
		<script src="/teamb/dwr/interface/UserBean.js"></script>
		<!-- DWR本体 -->
		<script src="/teamb/dwr/engine.js"></script>
	<!-- フォロー -->
	<script src="/teamb/js/follow.js"></script>
	
	<!-- フォントサイズ -->
	<script src="/teamb/m/js/fontsize.js"></script>
	<!-- メニュー(ヘッダ) -->
	<script src="/teamb/m/js/menu.js"></script>
	<!-- 写真投稿 -->
	<script src="/teamb/m/js/post.js"></script>
	
	<script>
		var userId = "<%= user.getUserId() %>"; // ユーザーID
		
		function init() {
			setFontSize(); // 画面の大きさから文字の大きさを決める
			menuInit();    // メニューの初期設定
			postInit(<%= user.getInitLat() %>,
			  <%= user.getInitLng() %>); // 写真投稿画面のマップの初期設定
		}
	</script>
</head>

<body onload="init()">

<!-- メニュー -->
<%@ include file="/m/jsp/menu.jsp" %>

<!--【ページセンター】ページ上の白ページ-->
<article>
	<div id="message">
		<%= listTitle %>
	</div>
	
	<!-- 対象ユーザーのプロフィール -->
	<%@ include file="/m/jsp/userdetail/targetprofile.jsp" %>
	
	<div id="timelineTopDiv"><br /></div>
	
	<div>
<%
	// リストに含まれるユーザーの数だけループ
	if (list != null) {
		for (int i=0; i<list.size(); i++) {
%>
		<div class="userDiv">
			
			<!-- ユーザーアイコン -->
			<figure class="userSearch">
				<a class="userLink" href="/teamb/pictsharemobile/user/<%= list.get(i).getUserId() %>">
					<img class="userIcon" src="/teamb/img/user/<%= list.get(i).getIconFile() %>">
				</a>
			</figure>
			
			<!-- ユーザーID -->
			<div class="user-id">
				ユーザーID:<%= list.get(i).getUserId() %>
			</div>
			
			<br />
			
			<!-- ユーザー名 -->
			<div class="user-name">
				ユーザー名:<%= list.get(i).getDispName() %>
			</div>
			
			<br />
			
			<!-- 紹介文 -->
			<div class="profile">
				自己紹介文:<br />
				<%= list.get(i).getProfile() %>
			</div>
		</div>
<%
		}
	}
%>
	</div>
</article>

<!-- 写真投稿画面 -->
<%@ include file="/m/jsp/post.jsp" %>

</body>
</html>

<%@ page contentType="text/html; charset=UTF-8" %>

<%--
フォローリクエスト許可・拒否画面

自分が鍵つき設定にしていてフォローされると、
フォロワー申請中IDリストにそのユーザーが追加される
フォロワー申請中IDリストが 1件以上あると、
自分のユーザー情報閲覧画面(ヘッダのアカウントをクリックしたりすると行く画面)に、
「フォローリクエストがあります」というメッセージが表示される
そのメッセージのリンク(/teamb/pictsharemobile/followrequest)をクリックすると、
このフォローリクエスト許可・拒否画面が表示される

セッションの followerWaitList にはフォロワー申請中IDリスト、
つまり自分にフォロー申請してきたユーザーのリストがセットされている

許可ボタン(class="okBtn")を押すと、follow.js の approve() が実行され、フォロー状態になる
ボタンには承認しましたというメッセージが表示される

拒否ボタン(class="ngBtn")を押すと、follow.js の reject() が実行され、
自分のフォロー申請中IDリストから消える(相手は永遠にフォロー申請中のまま)
ボタンには拒否しましたというメッセージが表示される
--%>

<%@ page import="java.util.ArrayList" %>
<%@ page import="pictshare.UserBean" %>
<jsp:useBean id="user" class="pictshare.UserBean" scope="session" />
<jsp:useBean id="targetUser" class="pictshare.UserBean" scope="session" />
<%
	// フォロワー申請中リストを取得
	ArrayList<UserBean> list = (ArrayList<UserBean>)session.getAttribute("followerWaitList");
%>

<!DOCTYPE html>
<html lang="ja">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
	
	<!-- タイトル -->
	<title>PictShare</title>
	
	<!-- CSS -->
	<!-- 友達閲覧画面 -->
	<link rel="stylesheet" href="/teamb/m/css/main.css">
	<!-- メニュー(ヘッダ) -->
	<link rel="stylesheet" href="/teamb/m/css/menu.css">
	<!-- プロフィールとおすすめタグ -->
	<link rel="stylesheet" href="/teamb/m/css/profile.css">
	<!-- タイムライン -->
	<link rel="stylesheet" href="/teamb/m/css/timeline.css">
	<!-- 投稿詳細 -->
	<link rel="stylesheet" href="/teamb/m/css/detail.css">
	<!-- フォロー -->
	<link rel="stylesheet" href="/teamb/m/css/follow.css">
	<!-- 写真投稿 -->
	<link rel="stylesheet" href="/teamb/m/css/post.css">
	
	<!-- JavaScript -->
	<!-- グーグルマップ -->
	<script src="http://maps.google.com/maps/api/js?v=3&libraries=places&sensor=false"></script>
	<!-- DWR -->
		<!-- ユーザー -->
		<script src="/teamb/dwr/interface/UserBean.js"></script>
		<!-- DWR本体 -->
		<script src="/teamb/dwr/engine.js"></script>
	<!-- フォロー -->
	<script src="/teamb/js/follow.js"></script>
	
	<!-- フォントサイズ -->
	<script src="/teamb/m/js/fontsize.js"></script>
	<!-- メニュー(ヘッダ) -->
	<script src="/teamb/m/js/menu.js"></script>
	<!-- 写真投稿 -->
	<script src="/teamb/m/js/post.js"></script>
	
	<script>
		var userId = "<%= user.getUserId() %>"; // ユーザーID
		
		function init() {
			setFontSize(); // 画面の大きさから文字の大きさを決める
			menuInit();    // メニューの初期設定
			postInit(<%= user.getInitLat() %>,
			  <%= user.getInitLng() %>); // 写真投稿画面のマップの初期設定
		}
	</script>
</head>

<body onload="init()">

<!-- メニュー -->
<%@ include file="/m/jsp/menu.jsp" %>

<!--【ページセンター】ページ上の白ページ-->
<article>
	<div id="message">
			フォローリクエスト
	</div>
	
	<!-- 自分のプロフィール -->
	<%@ include file="/m/jsp/userdetail/targetprofile.jsp" %>
	
	<div id="timelineTopDiv"><br /></div>
	
	<div>
<%
	// リストに含まれるユーザーの数だけループ
	if (list != null) {
		for (int i=0; i<list.size(); i++) {
%>
		<div class="userDiv">
			
			<!-- ユーザーアイコン -->
			<figure class="userSearch">
				<a class="userLink" href="/teamb/pictsharemobile/user/<%= list.get(i).getUserId() %>">
					<img class="userIcon" src="/teamb/img/user/<%= list.get(i).getIconFile() %>">
				</a>
			</figure>
			
			<!-- ユーザーID -->
			<div class="user-id">
				ユーザーID:<%= list.get(i).getUserId() %>
			</div>
			
			<br />
			
			<!-- ユーザー名 -->
			<div class="user-name">
				ユーザー名:<%= list.get(i).getDispName() %>
			</div>
			
			<br />
			
			<!-- 紹介文 -->
			<div class="profile">
				自己紹介文:<br />
				<%= list.get(i).getProfile() %>
			</div>
			
			<div id="btnset<%= i %>">
				<input type="button" class="okBtn" value="許可" onclick="approve(<%= i %>, '<%= user.getUserId() %>', '<%= list.get(i).getUserId() %>');" />
				<input type="button" class="ngBtn" value="拒否" onclick="reject(<%= i %>, '<%= user.getUserId() %>', '<%= list.get(i).getUserId() %>');" />
			</div>
		</div>
<%
		}
	}
%>
	</div>
</article>

<!-- 写真投稿画面 -->
<%@ include file="/m/jsp/post.jsp" %>

</body>
</html>

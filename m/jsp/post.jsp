<%@ page contentType="text/html; charset=UTF-8" %>

<%--
写真投稿画面

投稿ボタンを押すと /teamb/pictsharemobile/postdone に POST される

最初はファイル選択ボタンは 1枚目の分だけ表示されている(id="picSet0"のまとまり)
写真を選ぶごとに post.js の処理で 5つまで表示される

端末の位置情報を取得し、取得できればその位置に地図を動かす
取得できなければユーザー設定の初期位置のままで、
地図の上(id="show_result")にエラーメッセージを表示する
--%>

<!-- 写真投稿時の背景 -->
<div id="postCover"></div>

<!-- 写真投稿画面 -->
<div id="postDialog" >
	<form method="POST" enctype="multipart/form-data" name="postForm" onsubmit="return false;">
		<!-- 写真選択 -->
		<div id="postDialogFileSelect" data-role="none">
			<div>
				<div id="picSet0" class="picSet" style="display: block;">
					<div id="preview0" class="preview"></div>
					<div id="inputFileDiv0" data-role="none">
						<input data-role="none" id="inputFile0" type="file" name="file0" onchange="postPrev(this, 0);" accept="image/*" />
					</div>
					<canvas data-role="none" id="canvas0" style="display:none" width="200" height="100"></canvas>
				</div>
				<div id="picSet1" class="picSet" data-role="none">
					<div id="preview1" class="preview"></div>
					<div id="inputFileDiv1">
						<input id="inputFile1" type="file" name="file1" onchange="postPrev(this, 1);" accept="image/*" />
					</div>
					<canvas id="canvas1" style="display:none" width="200" height="100"></canvas>
				</div>
				<div id="picSet2" class="picSet">
					<div id="preview2" class="preview"></div>
					<div id="inputFileDiv2">
						<input id="inputFile2" type="file" name="file2" onchange="postPrev(this, 2);" accept="image/*" />
					</div>
					<canvas id="canvas2" style="display:none" width="200" height="100"></canvas>
				</div>
				<div id="picSet3" class="picSet">
					<div id="preview3" class="preview"></div>
					<div id="inputFileDiv3">
						<input id="inputFile3" type="file" name="file3" onchange="postPrev(this, 3);" accept="image/*" />
					</div>
					<canvas id="canvas3" style="display:none" width="200" height="100"></canvas>
				</div>
				<div id="picSet4" class="picSet">
					<div id="preview4" class="preview"></div>
					<div id="inputFileDiv4">
						<input id="inputFile4" type="file" name="file4" onchange="postPrev(this, 4);" accept="image/*" />
					</div>
					<canvas id="canvas4" style="display:none" width="200" height="100"></canvas>
				</div>
			</div>
			
			<center>
				<!--テキスト投稿 -->
				<p data-role="none">投稿内容を入力してください。</p>
				<textarea id="postMsgTextArea" name="postmsg" style="width: 90%;" rows="5" maxlength="140" data-role="none" placeholder="投稿文 140文字まで 「&amp;」、「&lt;」、「&gt;」、「&quot;」、「&#39;」、「\」は使用できません"></textarea>
			</center>
			
			<center>
				<button class="next-button" data-role="none" onclick="mapSelect();">次へ</button>
			</center>
		</div>
		
		<!-- 地図選択 -->
		<div id="postDialogMapSelect">
			<div>
				写真の位置情報<span id="show_result"></span><br />
				<div id="postMapField">
					<div id="postMap"></div>
					<input id="postMapTextField" class="controls" type="text">
				</div>
			</div>
			<input type="hidden" id="postLat" name="lat" value="<%= user.getInitLat() %>" />
			<input type="hidden" id="postLng" name="lng" value="<%= user.getInitLng() %>" />
			
			<!-- エラーメッセージ -->
			<center>
				<span id="postErrorMsg" class="errorMsg"></span>
			</center>
			
			<!--戻るボタン-->
			<button class="back-button" onclick="fileSelect();">戻る</button>
			<!--投稿ボタン-->
			<input id="postSubmitButton" class="submit-button" type="button" value="投稿" onclick="ajaxPost();" />
		</div>
	</form>
</div>

<%@ page contentType="text/html; charset=UTF-8" %>

<%--
タイムライン

セッションの postList に投稿リストがセットされているので、
それを使ってタイムラインを表示する

スマホ版は 10件ずつ表示し、リロードできるなら一番下にリロード(id="reloadDiv")を表示する
リロードは DWR を使って行う
一番下の投稿の投稿IDを JavaScript の endPostId という変数に設定しておく
リロード時には、その投稿IDより古い投稿を取得する
--%>

<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="pictshare.mobile.MobilePostBean" %>

<div id="posts">
<%
	String reloadDivOn = ""; // リロード
	
	// YYYY-MM-DD hh:mm 形式
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	
	// セッションに投稿リストがあれば取得
	ArrayList<MobilePostBean> postList =
	  (ArrayList<MobilePostBean>)session.getAttribute("postList");
	
	// 投稿リストに含まれる投稿の数だけループ
	if (postList != null) {
		int endPostId = 0; // 一番下の投稿ID
		
		for (int i=0; i<postList.size(); i++) {
			String goodIsEvaluated = ""; // 高評価ボタンの色用
			String badIsEvaluated  = ""; // 低評価ボタンの色用
			String goodCancel      = ""; // 高評価ボタンキャンセル用
			String badCancel       = ""; // 低評価ボタンキャンセル用
			String badTrue  = "false"; // 高評価時に低評価のキャンセルを行う
			String goodTrue = "false"; // 低評価時に高評価のキャンセルを行う
			
			// ユーザーが高評価済み
			if (postList.get(i).getIsGood() == 1) {
				goodIsEvaluated = " isEvaluated";
				goodCancel = "Cancel";
				goodTrue = "true";
			}
			
			// ユーザーが低評価済み
			else if (postList.get(i).getIsGood() == 2) {
				badIsEvaluated = " isEvaluated";
				badCancel = "Cancel";
				badTrue = "true";
			}
%>
	<div>
		<!--【8.ユーザーアイコン】-->
		<figure class="user_photo_left">
			<a class="userLink" href="/teamb/pictsharemobile/user/<%= postList.get(i).getUserId() %>">
				<img class="userIcon" src="/teamb/img/user/<%= postList.get(i).getIconFile() %>" />
			</a>
		</figure>
		
		<!--【9.ユーザー名】-->
		<div class="user-name">
			<%= postList.get(i).getDispName() %>
		</div>
		
		<!--【投稿日時】-->
		<div class="postDate">
			<%= df.format(postList.get(i).getPostDate()) %>
		</div>
		
		<!--【10.文章】-->
		<div class="document">
			<%= postList.get(i).getMessage() %>
		</div>
		
		<!--【11.写真】-->
		<div class="postphoto">
			<img id="<%= postList.get(i).getPostId() %>" class="photoImg" src="/teamb/img/post/<%= postList.get(i).getImgFile().get(0) %>" />
		</div>
		
		<div class="Good_Bad_Comment">
			<span id="good<%= postList.get(i).getPostId() %>" class="good">Good:<%= postList.get(i).getGood() %></span>
			<span id="bad<%= postList.get(i).getPostId() %>" class="bad">Bad:<%= postList.get(i).getBad() %></span>
			<span class="comment">コメント:<%= postList.get(i).getCommentNum() %>件</span>
		</div>
		
		<div class="evaluateDiv">
			<div id="goodButton<%= postList.get(i).getPostId() %>" class="goodButton<%= goodIsEvaluated %>" onclick="good<%= goodCancel %>(<%= postList.get(i).getPostId() %>, <%= badTrue %>);"></div>
			<div id="badButton<%= postList.get(i).getPostId() %>" class="badButton<%= badIsEvaluated %>" onclick="bad<%= badCancel %>(<%= postList.get(i).getPostId() %>, <%= goodTrue %>);"></div>
			<div class="commentButton" onclick="showDetail(<%= postList.get(i).getPostId() %>);"></div>
		</div>
	</div>
<%
			endPostId = postList.get(i).getPostId();
			
			// 投稿数が10ならリロードを表示
			if (i >= 9) {
				reloadDivOn = "<script>reloadDivOn(\"timeline\");</script>";
			}
		}
%>
	<script>
		endPostId = <%= endPostId %>;
	</script>
<%
	}
%>
</div>

<!-- さらに読み込む -->
<div id="reloadDiv">▼</div>
<%= reloadDivOn %>

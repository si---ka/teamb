<%@ page contentType="text/html; charset=UTF-8" %>

<%--
検索画面

ヘッダの検索をクリックして
/teamb/pictsharemobile/search で検索画面に来る場合と、
投稿文のタグについているリンク(PC版スマホ版共通)をクリックして
/teamb/pictshare/searchtag?id=1 のようにタグID指定で検索画面に来る場合がある
その場合はリクエストの tag にタグ名が入っている

フォームのラジオボタンでユーザーID検索とタグ検索を切り替える
ラジオボタンをクリックすると search.js の処理が実行され、
検索ボックスの左に #(id="hash") がついたり、
検索結果が初期化されたりする

検索ボタンを押すと search.js の処理で、
ユーザー検索結果(id="users")またはタグ検索結果(id="posts")に結果が表示される
--%>

<jsp:useBean id="user" class="pictshare.UserBean" scope="session" />
<!DOCTYPE html>
<html lang="ja">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
	
	<!-- タイトル -->
	<title>PictShare</title>
	
	<!-- CSS -->
	<!-- 友達閲覧画面 -->
	<link rel="stylesheet" href="/teamb/m/css/main.css">
	<!-- 検索 -->
	<link rel="stylesheet" href="/teamb/m/css/search.css">
	<!-- メニュー(ヘッダ) -->
	<link rel="stylesheet" href="/teamb/m/css/menu.css">
	<!-- プロフィールとおすすめタグ -->
	<link rel="stylesheet" href="/teamb/m/css/profile.css">
	<!-- タイムライン -->
	<link rel="stylesheet" href="/teamb/m/css/timeline.css">
	<!-- 投稿詳細 -->
	<link rel="stylesheet" href="/teamb/m/css/detail.css">
	<!-- 写真投稿 -->
	<link rel="stylesheet" href="/teamb/m/css/post.css">
	
	<!-- JavaScript -->
	<!-- グーグルマップ -->
	<script src="http://maps.google.com/maps/api/js?v=3&libraries=places&sensor=false"></script>
	<!-- DWR -->
		<!-- コメント -->
		<script src="/teamb/dwr/interface/CommentBean.js"></script>
		<!-- 投稿(スマホ用) -->
		<script src="/teamb/dwr/interface/MobilePostBean.js"></script>
		<!-- 投稿 -->
		<script src="/teamb/dwr/interface/PostBean.js"></script>
		<!-- タグ -->
		<script src="/teamb/dwr/interface/TagBean.js"></script>
		<!-- 今月ランキング Good用 -->
		<script src="/teamb/dwr/interface/ThisRankingBean.js"></script>
		<!-- ユーザー -->
		<script src="/teamb/dwr/interface/UserBean.js"></script>
		<!-- DWR本体 -->
		<script src="/teamb/dwr/engine.js"></script>
	<!-- 評価 -->
	<script src="/teamb/js/goodbad.js"></script>
	
	<!-- フォントサイズ -->
	<script src="/teamb/m/js/fontsize.js"></script>
	<!-- 検索 -->
	<script src="/teamb/m/js/search.js"></script>
	<!-- メニュー(ヘッダ) -->
	<script src="/teamb/m/js/menu.js"></script>
	<!-- 投稿詳細 -->
	<script src="/teamb/m/js/detail.js"></script>
	<!-- 写真投稿 -->
	<script src="/teamb/m/js/post.js"></script>
	
	<script>
		var userId = "<%= user.getUserId() %>"; // ユーザーID
		var endPostId = 0; // 一番下の投稿ID
		
		function init() {
			setFontSize(); // 画面の大きさから文字の大きさを決める
			menuInit();    // メニューの初期設定
			detailInit();  // 投稿詳細の初期設定
			postInit(<%= user.getInitLat() %>,
			  <%= user.getInitLng() %>); // 写真投稿画面のマップの初期設定
<%
	// リクエストからタグを取得
	// タグがあれば検索
	String tag = (String)request.getAttribute("tag");
	if (tag != null) {
%>
			// タグ検索する
			tagRadio();
			document.getElementById("tagInput").checked = true;
			document.getElementById("inputSearch").value = "<%= tag %>";
			search();
<%
	}
%>
		}
	</script>
</head>

<body onload="init()">

<!-- メニュー -->
<%@ include file="/m/jsp/menu.jsp" %>

<!--【ページセンター】ページ上の白ページ-->
<article>
	<form>
		<input type="radio" name="searchType" value="user" onclick="userRadio();" checked />ユーザーID　
		<input id="tagInput" type="radio" name="searchType" value="tag" onclick="tagRadio();" />タグ<br />
		<span id="hash"></span><input type="text" id="inputSearch" maxlength="140" style="width: 200px;" />
		<input class="search-button" type="button" value="検索" onclick="search();" />
	</form>
	
	<br />
	
	<!-- ユーザー検索結果 -->
	<div id="users"></div>
	
	<!-- タグ検索結果 -->
	<div id="posts"></div>
	
	<!-- さらに読み込む -->
	<div id="reloadDiv">▼</div>
</article>

<!-- 投稿詳細 -->
<%@ include file="/m/jsp/detail.jsp" %>

<!-- 写真投稿画面 -->
<%@ include file="/m/jsp/post.jsp" %>

</body>
</html>

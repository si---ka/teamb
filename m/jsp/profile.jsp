<%@ page contentType="text/html; charset=UTF-8" %>

<%--
プロフィールとおすすめタグ

セッションの user に自分のユーザー情報がセットされているので、
それを使ってユーザーアイコン、ユーザー名、フォロー数、フォロワー数を表示する

アプリケーションスコープからおすすめタグを取得し、3つ表示する
おすすめタグをクリックすると、recommendedtag.js の処理が呼び出され、
タイムラインにそのタグのついた投稿が表示される
--%>

<!--ここからコードを書く-->
<!-- プロフィール -->
<figure class="photo-left" width='100%'>
	<!--【アイコン】-->
	<!--プロフィール画像を表示させる-->
	<div id="icon">
		<img src="/teamb/img/user/<%= user.getIconFile() %>" />
	</div>
	
	<div id="nameFollow">
		<!--【3.ユーザー名】-->
		【<%= user.getDispName() %>】<br/>
		
		<!--【4.Follow 5.フォロー数】-->
		<a href="/teamb/pictsharemobile/user/<%= user.getUserId() %>/follow">Follow</a>
		　　<%= user.getFollowId().size() %><br/>
		
		<!--【6.Follower 7.フォロワー数】-->
		<a href="/teamb/pictsharemobile/user/<%= user.getUserId() %>/follower">Follower</a>
		　<%= user.getFollowerId().size() %>
	</div>
	
</figure>

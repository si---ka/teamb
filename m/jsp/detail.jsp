<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>

<%--
投稿詳細

ふだんは非表示
タイムラインの写真(class="photoImg")を押すと、
背景の黒い画面(id="cover")と投稿詳細(id="postDetail")を表示する

右上の×(id="closeDetail")を押すと消える

メインの写真の場所(id="mainImgDiv")には、最初は写真を表示しておき、
クリックすると地図に切り替える

サブの写真(id="photo1～4")は、枚数によって表示・非表示を変える
--%>

<!-- 投稿詳細表示時の背景 -->
<div id="cover"></div>

<!-- 投稿詳細 -->
<div id="postDetail">
	<center>
		<article id="articleDetail">
			<!-- 投稿詳細を閉じるボタン -->
			<div id="closeDetail">×</div>
			
			<!-- メインの投稿写真 地図 -->
			<div id="mainImgDiv" href="#popupPhoto" data-rel="popup">
				<!-- 写真 -->
				<div id="detailPhoto">
					<img id="photo0" src="" />
				</div>
				<!-- 地図 -->
				<div id="detailMap"></div>
			</div>
			
			<!-- メイン以外の投稿写真 -->
			<div id="subImgDiv">
				<img id="photo1" class="subImg" src="" />
				<img id="photo2" class="subImg" src="" />
				<img id="photo3" class="subImg" src="" />
				<img id="photo4" class="subImg" src="" />
			</div>
			
			<!-- 投稿文 -->
			<div class="msgDetail">
				投稿文
			</div>
			
			<!-- Good Bad ||01/22画像を地図にするボタン追加 -->
			<div class="Good_Bad" >
				<!-- 画像を地図に変更する -->
				<button id="changeMapButton" data-role="none"
					onclick="changeMap();">地図</button>
				<!-- onclick で評価の処理 -->
				<span class="good">Good:0</span>
				<span class="bad">Bad:0</span>
			</div>
			
			<br>

			<!-- コメント -->
			<div class="ui-field-contain">
			<!-- コメント記入欄 -->
			<span id="error" class="errorMsg"></span>
			<form>
				<input type="text" id="inputComment" maxlength="140" style="width: 100%;" />
				<input type="hidden" id="postId" value="" /><br>
				<input type="button" value="コメント" id="commentButton" onclick="postComment();" /><br><br>
			</form>
			</div>

			コメント
			<div id="comment">
				<div class="comSet">
					<!-- ユーザーアイコン -->
					<figure class="user_photo_left">
						<!-- ユーザー情報へのリンク -->
						<a class="userLink" href="">
							<img class="userIcon" src="" />
						</a>
					</figure>
					
					<!-- ユーザー名 -->
					<div class="user-name">
						ユーザー名
					</div>
					
					<br />
					
					<!-- コメント文 -->
					<div class="msgCom">
						コメント文
					</div>
				</div>
			</div>
			
		</article>
	</center>
</div>

<div data-role="popup" id="popupPhoto" data-dismissible="false">

<%@ page contentType="text/html; charset=UTF-8" %>

<%--
友達閲覧画面

セッションの user に自分のユーザー情報が、
セッションの postList に自分とフォローの投稿がセットされているので、
それらを使ってプロフィールとタイムラインを表示する

実際には profile.jsp と timeline.jsp がほとんど処理する
--%>

<jsp:useBean id="user" class="pictshare.UserBean" scope="session" />

<!DOCTYPE html>
<html lang="ja">

<head>

	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />

	
	<!-- タイトル -->
	<title>PictShare</title>
	
	<!-- CSS -->
	<!-- 友達閲覧画面 -->
	<link rel="stylesheet" href="/teamb/m/css/main.css">
	<!-- メニュー(ヘッダ) -->
	<link rel="stylesheet" href="/teamb/m/css/menu.css">
	<!-- プロフィールとおすすめタグ -->
	<link rel="stylesheet" href="/teamb/m/css/profile.css">
	<!-- タイムライン -->
	<link rel="stylesheet" href="/teamb/m/css/timeline.css">
	<!-- 投稿詳細 -->
	<link rel="stylesheet" href="/teamb/m/css/detail.css">
	<!-- 写真投稿 -->
	<link rel="stylesheet" href="/teamb/m/css/post.css">
	
	<!-- JavaScript -->
	<!-- グーグルマップ -->
	<script src="http://maps.google.com/maps/api/js?v=3&libraries=places&sensor=false"></script>
	<!-- DWR -->
		<!-- コメント -->
		<script src="/teamb/dwr/interface/CommentBean.js"></script>
		<!-- 投稿(スマホ用) -->
		<script src="/teamb/dwr/interface/MobilePostBean.js"></script>
		<!-- 投稿 -->
		<script src="/teamb/dwr/interface/PostBean.js"></script>
		<!-- 今月ランキング Good用 -->
		<script src="/teamb/dwr/interface/ThisRankingBean.js"></script>
		<!-- DWR本体 -->
		<script src="/teamb/dwr/engine.js"></script>
	<!-- 評価 -->
	<script src="/teamb/js/goodbad.js"></script>
	
	<!-- フォントサイズ -->
	<script src="/teamb/m/js/fontsize.js"></script>
	<!-- メニュー(ヘッダ) -->
	<script src="/teamb/m/js/menu.js"></script>
	<!-- おすすめタグ -->
	<script src="/teamb/m/js/recommendedtag.js"></script>
	<!-- タイムライン -->
	<script src="/teamb/m/js/timeline.js"></script>
	<!-- 投稿詳細 -->
	<script src="/teamb/m/js/detail.js"></script>
	<!-- 写真投稿 -->
	<script src="/teamb/m/js/post.js"></script>
	<%-- <%@ include file="/m/jsp/import.jsp" %> --%>

	<script>
		var userId = "<%= user.getUserId() %>"; // ユーザーID
		var endPostId = 0; // 一番下の投稿ID
		
		function init() {
			setFontSize(); // 画面の大きさから文字の大きさを決める
			menuInit();    // メニューの初期設定
			detailInit();  // 投稿詳細の初期設定
			postInit(<%= user.getInitLat() %>,
			  <%= user.getInitLng() %>); // 写真投稿画面のマップの初期設定
			isFriend = true; // 投稿時にタイムラインをリロード
		}
	</script>
</head>

<body onload="init()">
<div id="mainPage" data-role="page" data-tap-toggle="false">

<!-- メニュー -->
<%@ include file="/m/jsp/menu.jsp" %>

<!--【ページセンター】ページ上の白ページ-->
<article data-role="content">
	<div id="message">
<%
	// リクエストにメッセージがあれば取得
	String message = (String)request.getAttribute("message");
	if (message != null) {
		out.print(message);
	}
%>
	</div>
	
	<div id="timelineTopDiv"><br /></div>
	
	<!-- タイムライン -->
	<%@ include file="/m/jsp/timeline.jsp" %>
</article>

<!-- 投稿詳細 -->
<%@ include file="/m/jsp/detail.jsp" %>

<!-- 写真投稿画面 -->
<%@ include file="/m/jsp/post.jsp" %>

</div>
<!-- footer -->
<%-- <%@ include file="footer.jsp" %> --%>
</body>
</html>

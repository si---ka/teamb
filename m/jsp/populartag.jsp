<%@ page contentType="text/html; charset=UTF-8" %>

<%--
プロフィールとおすすめタグ

セッションの user に自分のユーザー情報がセットされているので、
それを使ってユーザーアイコン、ユーザー名、フォロー数、フォロワー数を表示する

アプリケーションスコープからおすすめタグを取得し、3つ表示する
おすすめタグをクリックすると、recommendedtag.js の処理が呼び出され、
タイムラインにそのタグのついた投稿が表示される
--%>

<%@ page import="pictshare.TagBean" %>
<%
	// おすすめタグの取得
	TagBean[] recommendedTag = (TagBean[])application.getAttribute("recommendedTag");
%>

<div id="recommendedTag">
		<h3>おすすめ</h3>
		<% int maxlength = 14; %><!-- スライドになる文字数 -->
		
		No.1
		
		<span class="rTag" onclick="searchPost(<%= recommendedTag[0].getTagId() %>);">
			#<%= recommendedTag[0].getTagName() %>
		</span><br />
	

		No.2
		<span class="rTag" onclick="searchPost(<%= recommendedTag[1].getTagId() %>);">
			#<%= recommendedTag[1].getTagName() %>
		</span><br />
		

		No.3
		<span class="rTag" onclick="searchPost(<%= recommendedTag[2].getTagId() %>);">
			#<%= recommendedTag[2].getTagName() %>
		</span>
		
	</div>
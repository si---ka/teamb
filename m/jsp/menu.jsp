<%@ page contentType="text/html; charset=UTF-8" %>

<%--
メニュー(ヘッダ)

現在表示している画面によって、サーブレットがリクエストの selectMenu に値を設定する
それを見て、そこの場所だけ色を変える
画像ファイル名に 25 をつける
--%>

<%
	// メニューの色を変える場合用
	String homeSelected     = ""; // ホーム
	String postSelected     = ""; // 投稿
	String timelineSelected = ""; // タイムライン
	String searchSelected   = ""; // 検索
	String rankingSelected  = ""; // ランキング
	String accountSelected  = ""; // アカウント
	String settingSelected  = ""; // 設定
	
	// メニューの色を変えるかを取得
	String selectMenu = (String)request.getAttribute("selectMenu");
	
	if (selectMenu != null) {
		
		// ホーム
		if (selectMenu.equals("home")) {
			homeSelected = "25";
		}
		
		// 投稿
		else if (selectMenu.equals("post")) {
			postSelected = "25";
		}
		
		// タイムライン
		else if (selectMenu.equals("timeline")) {
			timelineSelected = "25";
		}
		
		// 検索
		else if (selectMenu.equals("search")) {
			searchSelected = "25";
		}
		
		// ランキング
		else if (selectMenu.equals("ranking")) {
			rankingSelected = "25";
		}
		
		// アカウント
		else if (selectMenu.equals("account")) {
			accountSelected = "25";
		}
		
		// 設定
		else if (selectMenu.equals("setting")) {
			settingSelected = "25";
		}
	}
%>

<div id="menu">
	<!-- ホーム -->
	<div id="homeButton" class="menuButton">
		<img src="/teamb/m/img/home<%= homeSelected %>.png" />
	</div>
	
	<!-- 投稿 -->
	<div id="postButton" class="menuButton">
		<img src="/teamb/m/img/post<%= postSelected %>.png" />
	</div>
	
	<!-- タイムライン -->
	<div id="timelineButton" class="menuButton">
		<img src="/teamb/m/img/timeline<%= timelineSelected %>.png" />
	</div>
	
	<!-- 検索 -->
	<div id="searchButton" class="menuButton">
		<img src="/teamb/m/img/search<%= searchSelected %>.png" />
	</div>
	
	<!-- ランキング -->
	<div id="rankingButton" class="menuButton">
		<img src="/teamb/m/img/ranking<%= rankingSelected %>.png" />
	</div>
	
	<!-- アカウント -->
	<div id="accountButton" class="menuButton">
		<img src="/teamb/m/img/account<%= accountSelected %>.png" />
	</div>
	
	<!-- 設定 -->
	<div id="settingButton" class="menuButton">
		<img src="/teamb/m/img/setting<%= settingSelected %>.png" />
	</div>
</div>

<!-- メニューの下の空白 -->
<div id="menuBottom"></div>

<%--
ヘッダのTIMELINE、SETTINGを押したとき、
別の画面(ヘッダダイアログ)を上に重ねる
--%>

<!-- ヘッダダイアログ -->
<div id="headerDialog">
	<div id="headerDialogContent">
		<!-- 【友達閲覧】ボタン -->
		<div id="friendButton" class="headerDialogButton headerDialogButtonLeft"></div>
		
		<!-- 【地図指定】ボタン -->
		<div id="mapButton" class="headerDialogButton headerDialogButtonRight"></div>
		
		<!-- 【アカウント設定】ボタン -->
		<div id="accountSettingButton" class="headerDialogButton headerDialogButtonLeft"></div>
		
		<!-- 【ログアウト】ボタン -->
		<div id="logoutButton" class="headerDialogButton headerDialogButtonRight"></div>
	</div>
</div>

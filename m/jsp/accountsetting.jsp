<%@ page contentType="text/html; charset=UTF-8" %>

<%--
アカウント設定画面

/teamb/pictsharemobile/setting にアクセスした後に表示される
セッションの UserBean の情報から各項目を取得する
設定変更ボタンを押すと /teamb/pictsharemobile/settingdone に POST される

奇数行を class="oddRow"、偶数行を class="evenRow" にして色分け

ユーザーID、ユーザー名、メールアドレスは変更不可

ユーザーアイコンは、既に設定されているものをプレビュー(id="preview")に表示しておく
アイコンが新たにアップロードされると、accountsetting.js の prev() を実行し、
見えないキャンバス(id="canvas" style="display:none")で処理した後に、
プレビューに表示する

地図の初期位置設定は、見えない input (id="lat", id="lng") に設定しておく
--%>

<jsp:useBean id="user" class="pictshare.UserBean" scope="session" />

<!DOCTYPE html>
<html lang="ja">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
	
	<!-- タイトル -->
	<title>PictShare</title>
	
	<!-- CSS -->
	<!-- メニュー(ヘッダ) -->
	<link rel="stylesheet" href="/teamb/m/css/menu.css">
	<!-- アカウント設定 -->
	<link rel="stylesheet" href="/teamb/m/css/accountsetting.css">
	<!-- 写真投稿 -->
	<link rel="stylesheet" href="/teamb/m/css/post.css">
	
	<!-- JavaScript -->
	<!-- グーグルマップ -->
	<script src="http://maps.google.com/maps/api/js?v=3&libraries=places&sensor=false"></script>
	<!-- アカウント設定 -->
	<script src="/teamb/js/accountsetting.js"></script>
	
	<!-- フォントサイズ -->
	<script src="/teamb/m/js/fontsize.js"></script>
	<!-- メニュー(ヘッダ) -->
	<script src="/teamb/m/js/menu.js"></script>
	<!-- 写真投稿 -->
	<script src="/teamb/m/js/post.js"></script>
	
	<script>
		var userId = "<%= user.getUserId() %>"; // ユーザーID
		
		// 画面読み込み完了後に行う初期設定
		function init() {
			setFontSize(); // 画面の大きさから文字の大きさを決める
			menuInit();    // メニューの初期設定
			accountSettingInit(<%= user.getInitLat() %>,
			  <%= user.getInitLng() %>); // アカウント設定画面のマップの初期設定
			postInit(<%= user.getInitLat() %>,
			  <%= user.getInitLng() %>); // 写真投稿画面のマップの初期設定
		}
	</script>
</head>

<body onload="init()">

<!-- メニュー -->
<%@ include file="/m/jsp/menu.jsp" %>

<article>
	<center>
		<%= user.getDispName() %>さんのアカウント設定
<%
	// リクエストにメッセージがあれば取得
	String message = (String)request.getAttribute("message");
	if (message != null) {
		out.print("<br /><span class=\"errorMsg\">" + message + "</span>");
	}
%>
	</center>
	
	<form method="POST" enctype="multipart/form-data" action="/teamb/pictsharemobile/settingdone">
	
		<div class="oddRow">
			ユーザーID<br />
			<%= user.getUserId() %>
		</div>
		
		<div class="evenRow">
			メールアドレス<br />
			<%= user.getMail() %>
		</div>
		
		<div class="oddRow">
			ユーザー名<br />
			<%= user.getDispName() %>
		</div>
		
		<div class="evenRow">
			パスワード<br />
			変更する場合のみ入力してください<br />
			<span style="font-size: 0.5em;">
				英数字のみ 20文字まで<br />
			</span>
			<input type="password" name="pass" maxlength="20" />
		</div>
		
		<div class="oddRow">
			ユーザーアイコン<br />
			<div id="preview">
				<img src="/teamb/img/user/<%= user.getIconFile() %>" />
			</div>
			<div>
				<input id="inputFile" type="file" name="file" onchange="prev(this);" accept="image/*" />
			</div>
			<canvas id="canvas" style="display:none" width="150" height="150"></canvas>
		</div>
		
		<div class="evenRow">
			プロフィール<br />
			140文字まで<br />
			<span style="font-size: 0.5em;">
				「&amp;」、「&lt;」、「&gt;」、「&quot;」、「&#39;」、「\」は使用できません<br />
			</span>
<%
	// プロフィールの取得
	String profile = (String)request.getAttribute("profile");
	if (profile == null) {
		profile = user.getProfile();
	}
%>
			<textarea name="profile" style="width: 90%;" maxlength="140"><%= profile %></textarea>
		</div>
		
		<div class="oddRow">
			鍵設定<br />
			鍵をかけると あなたの投稿は フォローを許可した人以外 閲覧不可能になります<br />
			ランキングの集計対象からも外れます<br />
<%
	// 鍵つきフラグの取得
	// 片方だけ checked を入れる
	String lockTrueChecked  = "";
	String lockFalseChecked = "";
	if (user.getIsLocked()) {
		lockTrueChecked = "checked ";
	} else {
		lockFalseChecked = "checked ";
	}
%>
			<input type="radio" name="lock" value="true" <%= lockTrueChecked %>/>かける
			<input type="radio" name="lock" value="false" <%= lockFalseChecked %>/>かけない
		</div>
		
		<div class="evenRow">
			ホーム画面設定<br />
<%
	// 画面設定の取得
	// 片方だけ checked を入れる
	String friendChecked = "";
	String mapChecked    = "";
	if (user.getIsMapMode()) {
		mapChecked = "checked ";
	} else {
		friendChecked = "checked ";
	}
%>
			<input type="radio" name="home" value="firend" <%= friendChecked %>/>タイムライン友達版<br />
			<input type="radio" name="home" value="map" <%= mapChecked %>/>タイムライン地図版
		</div>
		
		<div class="oddRow">
			タイムライン地図版で初めに表示する位置<br />
			<div class="mapField">
				<div id="map"></div>
				<input id="mapTextField" class="controls" type="text">
			</div>
		</div>
		<input type="hidden" id="lat" name="lat" value="<%= user.getInitLat() %>" />
		<input type="hidden" id="lng" name="lng" value="<%= user.getInitLng() %>" />
		
		<center>
			<input class="setting-button" type="button" value="設定変更" onclick="submit();" />
		</center>
	</form>
</article>

<!-- 写真投稿画面 -->
<%@ include file="/m/jsp/post.jsp" %>

</body>
</html>

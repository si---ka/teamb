// 画面の大きさから文字の大きさを決める
function setFontSize() {
	
	// 画面の高さ
	var h = screen.availHeight;
	
	// bodyのfont-sizeを画面の高さから決める
	document.body.style.fontSize = parseInt(h/35) + "px";
}

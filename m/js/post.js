/* 写真投稿 投稿編集・削除
   
   写真がアップロードされたとき、
   キャンバスで処理してプレビューに表示
   
   投稿時は端末の位置情報を取得して、
   取得できたらその位置を中心にグーグルマップを表示して、中心にマーカーを立てる
   
   編集・削除時は写真の位置情報を中心にグーグルマップが表示されているので、
   中心にマーカーを立てる
   
   グーグルマップがクリックされたら、その場所を中心にマーカーを立て、
   緯度と経度を、見えない input (id="postLat", id="postLng") に設定する
 */

var postMap    = null; // 投稿用のマップ
var postMarker = null; // 投稿用のマーカー

var isFriend = false; // 友達閲覧画面なら投稿時にリロードする

// 画像のプレビュー
function postPrev(e, num) {
	
	// ログインしていなければ終了
	if (userId == "null") {
		return;
	}
	
	var file   = e.files[0],
	    image  = new Image(),
	    reader = new FileReader(),
	    canvas = document.getElementById("canvas" + num),
	    ctx    = canvas.getContext("2d");
	
	// プレビューが既にあれば消去
	var preview = document.getElementById("preview" + num);
	while (preview.hasChildNodes()) {
		preview.removeChild(preview.firstChild);
	}
	
	// ファイル未選択
	if (!file) {
		return;
	}
	
	if (file.type.match(/image.*/)) {
		reader.onload = function() {
			image.onload = function() {
				
				// キャンバス初期化
				ctx.clearRect(0, 0, canvas.width, canvas.height);
				
				// キャンバスに描画
				ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
				
				// プレビュー生成
				var img = document.createElement("img");
				img.src = canvas.toDataURL("image/jpeg");
				preview.appendChild(img);
				
				// 次のファイル選択ボタンを表示
				if (num < 4) {
					document.getElementById("picSet" + (num+1)).style.display = "block";
				}
			}
			if (reader.result != "data:") {
				image.src = reader.result;
			}
		}
		reader.readAsDataURL(file);
	}
}

// 写真投稿画面のマップの初期設定
function postInit(lat, lng) {
	
	//マップのオプション
	var mapOption = {
		zoom: 10,                                 //初期半径を指定
		center: new google.maps.LatLng(lat, lng), //初期位置を指定
		zoomControl: true,                        //ズームのバー true:表示 false:非表示
		panControl: false,                        //移動の矢印
		mapTypeControl: true,                     //マップのタイプ
		overviewMapControl: false,                //ミニマップの表示
		rotateControl: false,                     //９０℃回転の有無
		streetViewControl: false,                 //ペグマンコントロールの有無
		
		mapTypeControlOptions:{
			style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR //横並びで切り替えボタンを表示
		},
		mapTypeId: google.maps.MapTypeId.ROADMAP, //mapの形式を指定
	}
	//html文のpostMapにmapOptionの規則に従ったマップを作成
	postMap = new google.maps.Map(document.getElementById("postMap"), mapOption);
	
	var mapTextField = document.getElementById("postMapTextField"); //テキストフィールドを受け取る
	postMap.controls[google.maps.ControlPosition.TOP_LEFT].push(mapTextField); //mapにテキストフィールドを入れる
	
	var searchBox = new google.maps.places.SearchBox(mapTextField); //オートコンプリート機能の有効化
	
	google.maps.event.addListener(searchBox, 'places_changed', function() { //場所移動のイベントが起こった場合
		var place = searchBox.getPlaces()[0];           //検索候補から場所を受け取る
		if(!place.geometry){
			return;
		}
		if(place.geometry.viewport){                    //viewportが存在した場合はこっち
			postMap.fitBounds(place.geometry.viewport); //移動場所の指定
			postMap.setZoom(17);                        //検索後の縮尺の指定
		} else {                                        //なかった場合はこっち
			postMap.setCenter(place.geometry.location); //移動場所の指定
			postMap.setZoom(17);                        //検索後の縮尺の指定
		}
	});
	
	// 中央にマーカーを立てる
	postMarker = new google.maps.Marker({
		position: postMap.getCenter(),
		map: postMap
	});
	
	// クリックした場所にマーカーを立てて画面の中央にする
	google.maps.event.addListener(postMap, 'click', function(event) {
		postSelectCenter(event.latLng);
	});
	
	// 写真選択と地図選択の高さを画面の高さより小さくなるように設定
	document.getElementById("postDialogFileSelect").style.maxHeight =
	  (window.innerHeight - menuHeight - 10) + "px";
	document.getElementById("postDialogMapSelect").style.maxHeight =
	  (window.innerHeight - menuHeight - 10) + "px";
}

// クリックした場所にマーカーを立てて画面の中央にする
function postSelectCenter(location) {
	
	// マーカーが既にあれば削除
	if (postMarker) {
		postMarker.setMap(null);
		postMarker = null;
	}
	
	// マーカーを立てる
	postMarker = new google.maps.Marker({
		position: location,
		map: postMap
	});
	
	// リクエストする緯度経度をセット
	document.getElementById("postLat").value = location.lat();
	document.getElementById("postLng").value = location.lng();
	
	// 画面中央にする
	postMap.setCenter(location);
}

// 端末の位置情報の取得
function getPos() {
	navigator.geolocation.getCurrentPosition(successCallback, errorCallback);
}

// 位置情報取得に成功
function successCallback(position) {
	
	var location =
	  new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
	
	// マーカーが既にあれば削除
	if (postMarker) {
		postMarker.setMap(null);
		postMarker = null;
	}
	
	// マーカーを立てる
	postMarker = new google.maps.Marker({
		position: location,
		map: postMap
	});
	
	// リクエストする緯度経度をセット
	document.getElementById("postLat").value = location.lat();
	document.getElementById("postLng").value = location.lng();
	
	// 画面中央にする
	postMap.setCenter(location);
}
// 位置情報取得に失敗
function errorCallback(error) {
	var err_msg = "";
	switch(error.code)
	{
	case 1:
		err_msg = " 位置情報の利用が許可されていません";
		break;
	case 2:
		err_msg = " デバイスの位置が判定できません";
		break;
	case 3:
		err_msg = " タイムアウトしました";
		break;
	}
	document.getElementById("show_result").innerHTML = err_msg;
}

// 地図選択
function mapSelect() {
	document.getElementById("postDialogFileSelect").style.visibility = "hidden";
	document.getElementById("postDialogMapSelect").style.visibility  = "visible";
}

// 写真選択
function fileSelect() {
	document.getElementById("postDialogFileSelect").style.visibility = "visible";
	document.getElementById("postDialogMapSelect").style.visibility  = "hidden";
}

/* AjaxでPOST */
function ajaxPost() {
	// ほぼコピペだから知らん
	var fd = new FormData(document.forms.namedItem("postForm"));
	var req = new XMLHttpRequest();
	req.open("POST", "/teamb/pictsharemobile/postdone", true);
	req.onload = function(e) {
		if (req.status == 200) {
			if (req.responseText == "ok") {
				
				// 背景を消す
				document.getElementById("postCover").style.opacity = "0";
				document.getElementById("postCover").style.visibility = "hidden";
				
				// 投稿画面を消す
				document.getElementById("postDialog").style.visibility = "hidden";
				document.getElementById("postDialogFileSelect").style.visibility = "hidden";
				document.getElementById("postDialogMapSelect").style.visibility = "hidden";
				
				postFlag = false;
				
				// 投稿画面を初期化
				resetPost();
				
				// 友達閲覧画面なら
				// timeline.js の friendReload() を呼び出す
				if (isFriend) {
					friendReload();
				}
			} else {
				// エラーメッセージを表示
				document.getElementById("postErrorMsg").innerHTML = req.responseText;
			}
		} else {
			console.log("Er "+req.status);
		}
	};
	req.send(fd);
}

// 投稿画面を初期化
function resetPost() {
	
	// エラーメッセージを消す
	document.getElementById("postErrorMsg").innerHTML = "";
	
	for (var i=0; i<5; i++) {
		
		// ファイルを未選択にする
		var oldInputFileDiv = document.getElementById("inputFileDiv" + i);
		var newInputFileDiv = oldInputFileDiv.innerHTML;
		oldInputFileDiv.innerHTML = newInputFileDiv;
		
		// プレビューを消去する
		var preview = document.getElementById("preview" + i);
		while (preview.hasChildNodes()) {
			preview.removeChild(preview.firstChild);
		}
		
		// 1つめ以外のファイル選択ボタンを非表示にする
		if (i != 0) {
			document.getElementById("picSet" + i).style.display = "none";
		}
		
		// 投稿文を初期化
		document.getElementById("postMsgTextArea").value = "";
	}
}

var tPopup;
function tPopupWindow(){
	tPopup = document.createElement("div");		//タイムラインのポップアップのdiv作成
	var style = tPopup.style;			//タイムラインのスタイル編集の準備

	//ポップアップのスタイル
	style.position = "fixed";
	style.left = "0%";
	style.top = "7%";
	style.right = "0%";
	style.bottom = "0%";
	style.border = "1px #A00 solid";		//ポップアップの外側の線
	//style.border-radius = "20px";
	style.background = "transparent";			//ポップアップの背景色
	style.width = "100%";
	style.height = "100%";
	

	//BODYのノードリストに登録
	document.body.appendChild(tPopup);

	tPopup.innerHTML = '' +
			//'<link rel="stylesheet" href="/teamb/m/css/popup.css">'
			'<article style="height:83%; overflow:scroll;">' +
			'<div id="scroll"></div>' +
			'</article>' +
			'<div id="buttonField" style="height:10%">' +
			'<button type="button" class="submit-button" onClick="deletePopup()">閉じる</button>' +
			'</div>';
};

function deletePopup(){
	document.body.removeChild(tPopup);
}

function mapUpdate(){
	//timelineDialog();							//デバグ用、ポップアップ表示
	setNowCenter(map.getCenter());				//nowCenterに現在の中心座標を格納
	zoom = map.getZoom();						//ズームレベルの取得
	
	// 500ミリ秒後に投稿を取得
	//clearTimeout(timer);
	//timer = setTimeout(getPostByPosition, 500);
	getPostByPosition();

	//ズームレベルが変更された時の処理
}

function timelineDisp(){
	tPopupWindow();
	makeTimeLine(mendoikarakokonioitoku);
}

var comTemplate; // コメントのテンプレート

var detailMap    = null;  // 投稿詳細用のマップ
var detailMarker = null;  // 投稿詳細用のマーカー
var isMapMode    = false; // 地図を表示しているかのフラグ

// コメントのテンプレートを生成する
makeComTemplate();

// コメントのテンプレートを生成する
function makeComTemplate() {
	comTemplate = document.createElement("div");
	comTemplate.setAttribute("class", "comSet");
	
	// ユーザーアイコン
	var user = document.createElement("figure");
	user.setAttribute("class", "user_photo_left");
	var userLink = document.createElement("a");
	userLink.setAttribute("class", "userLink");
	var userIcon = document.createElement("img");
	userIcon.setAttribute("class", "userIcon");
	userLink.appendChild(userIcon);
	user.appendChild(userLink);
	comTemplate.appendChild(user);
	
	// ユーザー名
	var userName = document.createElement("div");
	userName.setAttribute("class", "user-name");
	comTemplate.appendChild(userName);
	
	// 改行
	var br = document.createElement("br");
	comTemplate.appendChild(br);
	
	// コメント文
	var message = document.createElement("div");
	message.setAttribute("class", "msgCom");
	comTemplate.appendChild(message);
}

// 投稿詳細の初期設定
function detailInit() {
	
	//マップのオプション
	var mapOption = {
		zoom: 10,                                //初期半径を指定
		center: new google.maps.LatLng(35, 135), //初期位置を指定
		zoomControl: false,                      //ズームのバー true:表示 false:非表示
		panControl: false,                       //移動の矢印
		mapTypeControl: false,                   //マップのタイプ
		overviewMapControl: false,               //ミニマップの表示
		rotateControl: false,                    //９０℃回転の有無
		streetViewControl: false,                //ペグマンコントロールの有無
		mapTypeId: google.maps.MapTypeId.ROADMAP //mapの形式を指定
	}
	//html文のdetailMapにmapOptionの規則に従ったマップを作成
	detailMap = new google.maps.Map(document.getElementById("detailMap"), mapOption);
	
	// 投稿詳細表示 右上の×を押したときに投稿詳細を消す
	document.getElementById("closeDetail").addEventListener("click", function(e) {
		
		// 背景を消す
		document.getElementById("cover").style.opacity = "0.0";
		document.getElementById("cover").style.visibility = "hidden";
		
		// 投稿詳細を消す
		document.getElementById("postDetail").style.visibility = "hidden";
	});
	
	// メインの写真を押したときポップアップを開き画像を原寸大表示
	document.getElementById("detailPhoto").addEventListener("click", function(e) {
		popupWindow();

		
		/*	一応残し map表示
		isMapMode = true;
		document.getElementById("detailMap").style.display = "block";
		document.getElementById("detailPhoto").style.display = "none";
		*/
	});

	
	// 地図を押したときにメインの写真に切り替える
	document.getElementById("detailMap").addEventListener("click", function(e) {
		isMapMode = false;
		document.getElementById("detailMap").style.display = "none";
		document.getElementById("detailPhoto").style.display = "block";
	});
	
	// 小さい写真を押したときにメインと入れ替える
	document.getElementById("subImgDiv").addEventListener("click", function(e) {
		
		// 地図モードなら終了
		if (isMapMode) {
			return;
		}
		
		// 写真が押されたとき
		if (e.target.className == "subImg") {
			var id = e.target.id;
			
			// メインの写真
			var main = document.getElementById("photo0");
			
			// 小さい写真
			var sub = document.getElementById(id);
			
			// メインを透明にする
			main.style.opacity = "0.0";
			
			// 100ミリ秒後
			setTimeout(function() {
				
				// 入れ替え
				var temp = main.getAttribute("src");
				main.setAttribute("src", sub.getAttribute("src"));
				sub.setAttribute("src", temp);
				
				// メインを表示
				main.style.opacity = "1.0";
			}, 100);
		}
	});
}

// 投稿詳細を表示する
function showDetail(postId) {
	
	// 投稿詳細を取得して生成
	getPostByPostId(postId);
	
	// コメントを取得して生成
	getCommentByPostId(postId);

	//コメントを一番下にスクロールする
	document.getElementById("comment").scrollTop = document.getElementById("comment").scrollHeight;
	
	// 縦スクロール量を取得する
	var scroll = document.documentElement.scrollTop || document.body.scrollTop;
	
	// 投稿詳細の位置をスクロール量だけずらす
	document.getElementById("articleDetail").style.marginTop = (scroll + menuHeight) + "px";
	
	// 背景を暗くする
	document.getElementById("cover").style.opacity = "0.6";
	document.getElementById("cover").style.visibility = "visible";
	
	// 投稿詳細を写真モードにする
	isMapMode = false;
	document.getElementById("detailMap").style.display = "none";
	document.getElementById("detailPhoto").style.display = "block";
	
	// 投稿詳細を表示する
	document.getElementById("postDetail").style.visibility = "visible";

}

// 投稿を取得
function getPostByPostId(postId) {
	
	// 投稿IDで投稿を取得
	PostBean.getPostByPostId(postId, true,
		
		// 返ってきた値で処理
		function(result) {
			
			// 投稿詳細を生成する
			makeDetail(result);
		}
	);
}

// コメントを取得
function getCommentByPostId(postId) {
	
	// 投稿IDでコメントを取得
	CommentBean.getCommentByPostId(postId,
		
		// 返ってきた値で処理
		function(result) {
			
			// コメントを生成する
			makeComment(result);
		}
	);
}

// 投稿詳細を生成する
function makeDetail(result) {
	
	var ad = document.getElementById("articleDetail");
	
	// メインの投稿写真
	document.getElementById("photo0").setAttribute("src", "/teamb/img/post/" + result.imgFile[0]);
	
	// メイン以外の投稿写真
	var i;
	for (i=1; i<result.imgFile.length; i++) {
		var subImg = document.getElementById("photo" + i);
		
		// 写真をセットして表示
		subImg.setAttribute("src", "/teamb/img/post/" + result.imgFile[i]);
		subImg.style.display = "inline";
	}
	// 写真が5枚未満なら残りを非表示
	for ( ; i<5; i++) {
		var subImg = document.getElementById("photo" + i);
		subImg.setAttribute("src", "");
		subImg.style.display = "none";
	}
	
	// 投稿文
	var message = ad.querySelector(".msgDetail");
	message.innerHTML = result.message;
	
	// Good
	var good = ad.querySelector(".good");
	if (result.postId != 0) {
		good.replaceChild(document.createTextNode("Good:" + result.good), good.firstChild);
	} else {
		good.replaceChild(document.createTextNode("Good:-"), good.firstChild);
	}
	
	// Bad
	var bad = ad.querySelector(".bad");
	if (result.postId != 0) {
		bad.replaceChild(document.createTextNode("Bad:" + result.bad), bad.firstChild);
	} else {
		bad.replaceChild(document.createTextNode("Bad:-"), bad.firstChild);
	}
	
	// 投稿IDをセット
	document.getElementById("postId").value = result.postId;
	
	// 位置情報
	var location = new google.maps.LatLng(result.lat, result.lng);
	
	// マーカーが既にあれば削除
	if (detailMarker) {
		detailMarker.setMap(null);
		detailMarker = null;
	}
	
	// マーカーを立てる
	detailMarker = new google.maps.Marker({
		position: location,
		map: detailMap
	});
	
	// 画面中央にする
	detailMap.setCenter(location);
}

// コメントを生成する
function makeComment(result) {
	
	// コメントを初期化
	comment = document.getElementById("comment");
	while (comment.firstChild) {
		comment.removeChild(comment.firstChild);
	}
	
	// コメントリストに含まれるコメントの数だけループ
	for (var i = 0; i<result.length; i++) {
		
		// テンプレートからコピー
		var cm = comTemplate.cloneNode(true);
		
		// ユーザーアイコン
		cm.querySelector(".userLink").setAttribute("href", "/teamb/pictsharemobile/user/" + result[i].userId);
		cm.querySelector(".userIcon").setAttribute("src", "/teamb/img/user/" + result[i].iconFile);
		
		// ユーザー名
		var userName = cm.querySelector(".user-name");
		userName.appendChild(document.createTextNode(result[i].dispName));
		
		// コメント文
		var message = cm.querySelector(".msgCom");
		message.appendChild(document.createTextNode(result[i].message));
		
		// タイムラインに追加
		comment.appendChild(cm);

		//コメントを一番下にスクロールする
		document.getElementById("comment").scrollTop = document.getElementById("comment").scrollHeight;
	}
}

// コメントする
function postComment() {
	
	// ログインしていなければ終了
	if (userId == "null") {
		return;
	}
	
	// 投稿IDを取得
	var postId = document.getElementById("postId").value;
	
	// 投稿IDが 0 なら終了
	if (postId == 0) {
		return;
	}
	
	// コメントをデータベースに登録する
	CommentBean.registerComment(postId, userId,
	  document.getElementById("inputComment").value,
		
		// 返ってきた値で処理
		function(result) {
			
			// エラーなし
			if (!result) {
				// エラーメッセージとコメントを空に
				document.getElementById("error").innerHTML = "";
				document.getElementById("inputComment").value = "";
				
				// コメント再取得
				getCommentByPostId(postId);
			}
			
			// エラーメッセージを表示
			else {
				document.getElementById("error").innerHTML = result;
			}
		}
	);
	//コメントを一番下にスクロールする
	document.getElementById("comment").scrollTop = document.getElementById("comment").scrollHeight;
}

/*
 * 写真拡大表示のポップアップ設定
 */

function popupWindow(){
	popup = document.createElement("div");		//ポップアップのdiv作成
	var style = popup.style;			//スタイル編集の準備

	//ポップアップのスタイル
	style.position = "fixed";
	style.left = "0%";
	style.top = "7%";
	style.right = "0%";
	style.bottom = "0%";
	style.border = "1px #A00 solid";		//ポップアップの外側の線
	//style.border-radius = "20px";
	style.background = "transparent";			//ポップアップの背景色
	style.width = "100%";
	style.height = "100%";
	
	ad = document.getElementById("articleDetail");

	//BODYのノードリストに登録
	ad.appendChild(popup);
	var photo0 = document.getElementById("photo0");
	var image = photo0.getAttribute("src");
	var imgWid = image.width;
	var imgHgt = image.height;console.log(image);

	popup.innerHTML = '' +
			//'<link rel="stylesheet" href="/teamb/m/css/popup.css">'
			'<article style="height: 35em;">' +
				'<div id="closePicDetail">x</div>' +
				'<div style="clear: both; height: 30em; overflow: scroll;">' +
				'<img src="' + image + '" style="width: auto; height: auto; max-width: none;">' +
				'</div>' +
			'</article>';

	//右上の閉じるボタンの閉じるイベント
	closePicDetail.addEventListener("click", function(){
		document.getElementById("articleDetail").removeChild(popup);
	});
};

function changeMap(){
	if(isMapMode){
		isMapMode = false;
		document.getElementById("detailMap").style.display = "none";
		document.getElementById("detailPhoto").style.display = "block";
	} else {
		isMapMode = true;
		document.getElementById("detailMap").style.display = "block";
		document.getElementById("detailPhoto").style.display = "none";
	}
}

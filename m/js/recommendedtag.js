/* おすすめタグ
   
   おすすめタグは、タイムライン友達版に表示されている
   おすすめタグをクリックすると、searchPost が実行される
   
   searchPost は、DWR でタグIDをもとに投稿を検索する
   検索結果はタイムラインに表示される
   
   タイムラインの投稿情報は、テンプレートをコピーして作る
   コピーしたものを書き換えて、タイムラインに追加する
   
   画面の下までスクロールするとリロードする
 */

var nowTagId = 0; // 現在検索しているタグID
var postTemplate; // 投稿情報のテンプレート

// 投稿情報のテンプレートを生成する
makePostTemplate();

// 投稿情報のテンプレートを生成する
function makePostTemplate() {
	postTemplate = document.createElement("div");
	
	// ユーザーアイコン
	var user = document.createElement("figure");
	user.setAttribute("class", "user_photo_left");
	var userLink = document.createElement("a");
	userLink.setAttribute("class", "userLink");
	var userIcon = document.createElement("img");
	userIcon.setAttribute("class", "userIcon");
	userLink.appendChild(userIcon);
	user.appendChild(userLink);
	postTemplate.appendChild(user);
	
	// ユーザー名
	var userName = document.createElement("div");
	userName.setAttribute("class", "user-name");
	postTemplate.appendChild(userName);
	
	// 投稿日時
	var postDate = document.createElement("div");
	postDate.setAttribute("class", "postDate");
	postTemplate.appendChild(postDate);
	
	// 投稿文
	var message = document.createElement("div");
	message.setAttribute("class", "document");
	postTemplate.appendChild(message);
	
	// 写真
	var photo = document.createElement("div");
	photo.setAttribute("class", "postphoto");
	var photoImg = document.createElement("img");
	photoImg.setAttribute("class", "photoImg");
	photo.appendChild(photoImg);
	postTemplate.appendChild(photo);
	
	// Good Bad コメント数
	var eval = document.createElement("div");
	eval.setAttribute("class", "Good_Bad_Comment");
	
	// Good
	var good = document.createElement("span");
	good.setAttribute("class", "good");
	eval.appendChild(good);
	eval.appendChild(document.createTextNode(" "));
	
	// Bad
	var bad = document.createElement("span");
	bad.setAttribute("class", "bad");
	eval.appendChild(bad);
	eval.appendChild(document.createTextNode(" "));
	
	// コメント数
	var comment = document.createElement("span");
	comment.setAttribute("class", "comment");
	eval.appendChild(comment);
	postTemplate.appendChild(eval);
	
	// 評価ボタンとコメントボタン
	var evaluateDiv = document.createElement("div");
	evaluateDiv.setAttribute("class", "evaluateDiv");
	
	// Good ボタン
	var goodButton = document.createElement("div");
	goodButton.setAttribute("class", "goodButton");
	evaluateDiv.appendChild(goodButton);
	
	// Bad ボタン
	var badButton = document.createElement("div");
	badButton.setAttribute("class", "badButton");
	evaluateDiv.appendChild(badButton);
	
	// コメントボタン
	var commentButton = document.createElement("div");
	commentButton.setAttribute("class", "commentButton");
	evaluateDiv.appendChild(commentButton);
	postTemplate.appendChild(evaluateDiv);
}

// 日時を YYYY-MM-DD hh:mm 形式の文字列に変換する
function dateToString(date) {
	return date.getFullYear() + "-" + ("0" + (date.getMonth() + 1)).slice(-2) +
	  "-" + ("0" + date.getDate()).slice(-2) + " " + ("0" + date.getHours()).slice(-2) +
	  ":" + ("0" + date.getMinutes()).slice(-2);
}

// タグ検索する
function searchPost(tagId) {
	
	// 現在検索しているタグID
	nowTagId = tagId;
	
	MobilePostBean.getPostByTagId(userId, tagId, 0,
		// 返ってきた値で処理
		function(result) {
			// タグ検索結果を生成する
			makePosts(result);
		}
	);
}

// タグ検索結果を生成する
function makePosts(result) {
	
	// タグ検索結果を初期化
	var posts = document.getElementById("posts");
	while (posts.firstChild) {
		posts.removeChild(posts.firstChild);
	}
	
	// リロードを非表示
	reloadDivOff();
	
	// 検索結果0件
	if (result.length == 0) {
		posts.appendChild(document.createTextNode("指定のタグを含む投稿が見つかりませんでした"));
	}
	
	// 投稿リストに含まれる投稿の数だけループ
	for (var i = 0; i<result.length; i++) {
		
		// テンプレートからコピー
		var post = postTemplate.cloneNode(true);
		
		// ユーザーアイコン
		post.querySelector(".userLink").setAttribute("href", "/teamb/pictsharemobile/user/" + result[i].userId);
		post.querySelector(".userIcon").setAttribute("src", "/teamb/img/user/" + result[i].iconFile);
		
		// ユーザー名
		var userName = post.querySelector(".user-name");
		userName.appendChild(document.createTextNode(result[i].dispName));
		
		// 投稿日時
		var postDate = post.querySelector(".postDate");
		postDate.appendChild(document.createTextNode(dateToString(result[i].postDate)));
		
		// 投稿文
		var message = post.querySelector(".document");
		message.innerHTML = result[i].message;
		
		// 写真
		post.querySelector(".photoImg").setAttribute("id", result[i].postId);
		post.querySelector(".photoImg").setAttribute("src", "/teamb/img/post/" + result[i].imgFile[0]);
		
		// Good
		var good = post.querySelector(".good");
		good.setAttribute("id", "good" + result[i].postId);
		good.appendChild(document.createTextNode("Good:" + result[i].good));
		
		// Bad
		var bad = post.querySelector(".bad");
		bad.setAttribute("id", "bad" + result[i].postId);
		bad.appendChild(document.createTextNode("Bad:" + result[i].bad));
		
		// コメント数
		var comment = post.querySelector(".comment");
		comment.appendChild(document.createTextNode("コメント:" + result[i].commentNum + "件"));
		
		// Good ボタン
		var goodButton = post.querySelector(".goodButton");
		goodButton.setAttribute("id", "goodButton" + result[i].postId);
		var goodTrue   = "false";
		var goodCancel = "good";
		// ユーザーが高評価済み
		if (result[i].isGood == 1) {
			goodTrue   = "true";
			goodCancel = "goodCancel";
			goodButton.setAttribute("class", "goodButton isEvaluated");
		}
		
		// Bad ボタン
		var badButton = post.querySelector(".badButton");
		badButton.setAttribute("id", "badButton" + result[i].postId);
		var badTrue   = "false";
		var badCancel = "bad";
		// ユーザーが低評価済み
		if (result[i].isGood == 2) {
			badTrue   = "true";
			badCancel = "badCancel";
			badButton.setAttribute("class", "badButton isEvaluated");
		}
		
		goodButton.setAttribute("onclick", goodCancel + "(" + result[i].postId + ", " + badTrue + ");");
		badButton.setAttribute("onclick", badCancel + "(" + result[i].postId + ", " + goodTrue + ");");
		
		// コメントボタン
		var commentButton = post.querySelector(".commentButton");
		commentButton.setAttribute("onclick", "showDetail(" + result[i].postId + ");");
		
		// タグ検索結果に追加
		posts.appendChild(post);
		
		// 一番下の投稿ID
		endPostId = result[i].postId;
		
		// 投稿数が10ならリロードを表示
		if (i >= 9) {
			reloadDivOn("tag");
		}
	}
}

// リロードする
function reloadRecommendedTag() {
	MobilePostBean.getPostByTagId(userId, nowTagId, endPostId,
		// 返ってきた値で処理
		function(result) {
			// タグ検索結果に追加する
			addPosts(result);
		}
	);
}

// タグ検索結果に追加する
function addPosts(result) {
	
	var posts = document.getElementById("posts"); // タイムライン
	
	// 投稿リストに含まれる投稿の数だけループ
	for (var i = 0; i<result.length; i++) {
		
		// テンプレートからコピー
		var post = postTemplate.cloneNode(true);
		
		// ユーザーアイコン
		post.querySelector(".userLink").setAttribute("href", "/teamb/pictsharemobile/user/" + result[i].userId);
		post.querySelector(".userIcon").setAttribute("src", "/teamb/img/user/" + result[i].iconFile);
		
		// ユーザー名
		var userName = post.querySelector(".user-name");
		userName.appendChild(document.createTextNode(result[i].dispName));
		
		// 投稿日時
		var postDate = post.querySelector(".postDate");
		postDate.appendChild(document.createTextNode(dateToString(result[i].postDate)));
		
		// 投稿文
		var message = post.querySelector(".document");
		message.innerHTML = result[i].message;
		
		// 写真
		post.querySelector(".photoImg").setAttribute("id", result[i].postId);
		post.querySelector(".photoImg").setAttribute("src", "/teamb/img/post/" + result[i].imgFile[0]);
		
		// Good
		var good = post.querySelector(".good");
		good.setAttribute("id", "good" + result[i].postId);
		good.appendChild(document.createTextNode("Good:" + result[i].good));
		
		// Bad
		var bad = post.querySelector(".bad");
		bad.setAttribute("id", "bad" + result[i].postId);
		bad.appendChild(document.createTextNode("Bad:" + result[i].bad));
		
		// コメント数
		var comment = post.querySelector(".comment");
		comment.appendChild(document.createTextNode("コメント:" + result[i].commentNum + "件"));
		
		// Good ボタン
		var goodButton = post.querySelector(".goodButton");
		goodButton.setAttribute("id", "goodButton" + result[i].postId);
		var goodTrue   = "false";
		var goodCancel = "good";
		// ユーザーが高評価済み
		if (result[i].isGood == 1) {
			goodTrue   = "true";
			goodCancel = "goodCancel";
			goodButton.setAttribute("class", "goodButton isEvaluated");
		}
		
		// Bad ボタン
		var badButton = post.querySelector(".badButton");
		badButton.setAttribute("id", "badButton" + result[i].postId);
		var badTrue   = "false";
		var badCancel = "bad";
		// ユーザーが低評価済み
		if (result[i].isGood == 2) {
			badTrue   = "true";
			badCancel = "badCancel";
			badButton.setAttribute("class", "badButton isEvaluated");
		}
		
		goodButton.setAttribute("onclick", goodCancel + "(" + result[i].postId + ", " + badTrue + ");");
		badButton.setAttribute("onclick", badCancel + "(" + result[i].postId + ", " + goodTrue + ");");
		
		// コメントボタン
		var commentButton = post.querySelector(".commentButton");
		commentButton.setAttribute("onclick", "showDetail(" + result[i].postId + ");");
		
		// タグ検索結果に追加
		posts.appendChild(post);
		
		// 一番下の投稿ID
		endPostId = result[i].postId;
		
		// 投稿数が10ならリロードを表示
		if (i >= 9) {
			reloadDivOn("tag");
		}
	}
}

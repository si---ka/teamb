var zoom = 10;											//マップの拡大率
var center = new google.maps.LatLng(34.38581, 135);		//マップの初期位置
map = null;												//マップの変数宣言
var user = null;										//geoUserのクラス[User]の受け皿
var nowCenter = center;									//現在のマップの中心
var autocomplete = null;								//テキストボックス内で検索した時の場所候補
var searchBox = null;

//マップの初期設定
function initialize(lat, lng) { //<body onload="initialize()">で起動
	//マップのオプション
	var mapOption = {
		zoom: zoom,										//初期半径を指定
		center: new google.maps.LatLng(lat, lng),		//初期位置を指定
		zoomControl: true,								//ズームのバー	true:表示 false:非表示
		panControl: false,								//移動の矢印
		mapTypeControl: false,							//マップのタイプ
		overviewMapControl: false,						//ミニマップの表示
		rotateControl: false,							//９０℃回転の有無
		streetViewControl: false,						//ペグマンコントロールの有無

		mapTypeControlOptions:{
			style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR	//横並びで切り替えボタンを表示
		},
		mapTypeId: google.maps.MapTypeId.ROADMAP,		//mapの形式を指定
	}
	//html文のmap_canvasにmapOptionの規則に従ったマップを作成
	map = new google.maps.Map(document.getElementById("map"), mapOption);

	var mapTextField = document.getElementById("mapTextField");				//テキストフィールドを受け取る
	map.controls[google.maps.ControlPosition.TOP_LEFT].push(mapTextField);	//mapにテキストフィールドを入れる

	searchBox = new google.maps.places.SearchBox(mapTextField);			//オートコンプリート機能の有効化
	//autocomplete.bindTo('bounds', map);								

	google.maps.event.addListener(searchBox, 'places_changed', function(){//場所移動のイベントが起こった場合
		var place = searchBox.getPlaces()[0];								//検索候補から場所を受け取る
		if(!place.geometry){
			return;
		}
		if(place.geometry.viewport){										//viewportが存在した場合はこっち
			map.fitBounds(place.geometry.viewport);							//移動場所の指定
			map.setZoom(17);												//検索後の縮尺の指定
		} else {															//なかった場合はこっち
			map.setCenter(place.geometry.location);							//移動場所の指定
			map.setZoom(17);												//検索後の縮尺の指定
		}
	});
}
///////////////////////////////////////////////////////////////////////////initilize終了


function getNowCenter(){ return nowCenter; }				//nowCenterのgetter
function setNowCenter(latlng){ nowCenter = latlng; }		//nowCenterのsetter

function markerClear(marker){								//マーカーの削除
	for(var i = 0 ; i < (marker.length)-1 ; i++){			//マーカーの個数分ループする
		marker[i].setMap(null);								//マーカーの削除メソッド
	}
}

function userSet(User){										//Userクラスをこのクラスの変数に代入
	this.user = User;										//Userクラスをこのクラスの変数に格納
}


function debag(){				//デバグ用
	alert(nowCenter.lat());
}

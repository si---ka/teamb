var timer; // タイマー

// 投稿取得の初期設定
function getPostInit() {
	
	//マップ上でマウスを離した時の処理
		google.maps.event.addListener(map, 'mouseup', function(event){
		setNowCenter(map.getCenter());						//nowCenterに現在の中心座標を格納
		
		// 500ミリ秒後に投稿を取得
		clearTimeout(timer);
		timer = setTimeout(getPostByPosition, 500);
	});
	//ズームレベルが変更された時の処理
	google.maps.event.addListener(map, 'zoom_changed', function(){
		zoom = map.getZoom();								//ズームレベルの取得
		
		// 500ミリ秒後に投稿を取得
		clearTimeout(timer);
		timer = setTimeout(getPostByPosition, 500);
	});
}

// 投稿を取得
function getPostByPosition() {
	
	// 地図の端の緯度経度を取得
	var bounds = map.getBounds();
	var sw = bounds.getSouthWest();
	var ne = bounds.getNorthEast();
	
	// 緯度経度から投稿を取得
	PostBean.getPostByPosition(userId, sw.lat(), sw.lng(), ne.lat(), ne.lng(),
		
		// 返ってきた値で処理
		function(result) {
			
			// 投稿リストを初期化
			postArray = [];
			
			// 返ってきた個数ループ
			for (i = 0; i < result.length; i++) {
				postArray[i] = {
					id  : result[i].postId,
					lat : result[i].lat,
					lng : result[i].lng,
					meshcode: result[i].meshCode
				}
			}
			
			// ピンを生成する
			makePins();
			
			// 処理もっかい書くのめんどいからここに
			mendoikarakokonioitoku = result;
			
			// タイムラインを生成する
			//makeTimeLine(result);
		}
	);
	
}

/* 友達閲覧画面のタイムライン
   
   タイムラインの投稿情報は、テンプレートをコピーして作る
   コピーしたものを書き換えて、タイムラインに追加する
   
   画面の下までスクロールするとリロードする
 */

var reloadable = ""; // リロードするもの

// リロードを表示
function reloadDivOn(mode) {
	document.getElementById("reloadDiv").style.visibility = "visible";
	reloadable = mode;
}

// リロードを非表示
function reloadDivOff() {
	document.getElementById("reloadDiv").style.visibility = "hidden";
	reloadable = "";
}


// 画面のスクロールが一番下(上)までいけばリロード
window.onscroll = function() {
	
	if (reloadable == "") {
		return;
	}
	
	var scroll = document.body.scrollTop; // 縦スクロール量
	var height = window.innerHeight;      // 画面の縦サイズ
	
	var reloadDiv    = document.getElementById("reloadDiv"); // リロード表示
	var reloadTop    = reloadDiv.offsetTop;    // リロード表示の縦位置
	var reloadHeight = reloadDiv.clientHeight; // リロード表示の縦サイズ
	
	// リロード（下）
	if ((scroll + height) > (reloadTop + reloadHeight)) {
		
		// タイムライン
		if (reloadable == "timeline") {
			reloadDivOff();
			reloadTimeline();
		}
		
		// おすすめタグ
		else if (reloadable == "tag") {
			reloadDivOff();
			reloadRecommendedTag();
		}
	}
	// リロード（上）
	document.addEventListener("touchend", function(){
		if (navigator.userAgent.indexOf('iPhone') > 0){
			if (scroll < -50) {
				window.scrollTo(0,scroll);
				// タイムライン
				if (reloadable == "timeline") {
					reloadDivOff();
					location.reload();
					window.scrollTo(0,0);
				}
				
				// おすすめタグ
				else if (reloadable == "tag") {
					
				}
			}
		}
	})
}

// リロードする
function reloadTimeline() {
	MobilePostBean.getPostByFriend(userId, endPostId,
		// 返ってきた値で処理
		function(result) {
			// タイムラインに追加する
			addTimeline(result);
		}
	);
}

// タイムラインに追加する
function addTimeline(result) {
	
	var posts = document.getElementById("posts"); // タイムライン
	
	// 投稿リストに含まれる投稿の数だけループ
	for (var i = 0; i<result.length; i++) {
		
		// テンプレートからコピー
		var post = postTemplate.cloneNode(true);
		
		// ユーザーアイコン
		post.querySelector(".userLink").setAttribute("href", "/teamb/pictsharemobile/user/" + result[i].userId);
		post.querySelector(".userIcon").setAttribute("src", "/teamb/img/user/" + result[i].iconFile);
		
		// ユーザー名
		var userName = post.querySelector(".user-name");
		userName.appendChild(document.createTextNode(result[i].dispName));
		
		// 投稿日時
		var postDate = post.querySelector(".postDate");
		postDate.appendChild(document.createTextNode(dateToString(result[i].postDate)));
		
		// 投稿文
		var message = post.querySelector(".document");
		message.innerHTML = result[i].message;
		
		// 写真
		post.querySelector(".photoImg").setAttribute("id", result[i].postId);
		post.querySelector(".photoImg").setAttribute("src", "/teamb/img/post/" + result[i].imgFile[0]);
		
		// Good
		var good = post.querySelector(".good");
		good.setAttribute("id", "good" + result[i].postId);
		good.appendChild(document.createTextNode("Good:" + result[i].good));
		
		// Bad
		var bad = post.querySelector(".bad");
		bad.setAttribute("id", "bad" + result[i].postId);
		bad.appendChild(document.createTextNode("Bad:" + result[i].bad));
		
		// コメント数
		var comment = post.querySelector(".comment");
		comment.appendChild(document.createTextNode("コメント:" + result[i].commentNum + "件"));
		
		// Good ボタン
		var goodButton = post.querySelector(".goodButton");
		goodButton.setAttribute("id", "goodButton" + result[i].postId);
		var goodTrue   = "false";
		var goodCancel = "good";
		// ユーザーが高評価済み
		if (result[i].isGood == 1) {
			goodTrue   = "true";
			goodCancel = "goodCancel";
			goodButton.setAttribute("class", "goodButton isEvaluated");
		}
		
		// Bad ボタン
		var badButton = post.querySelector(".badButton");
		badButton.setAttribute("id", "badButton" + result[i].postId);
		var badTrue   = "false";
		var badCancel = "bad";
		// ユーザーが低評価済み
		if (result[i].isGood == 2) {
			badTrue   = "true";
			badCancel = "badCancel";
			badButton.setAttribute("class", "badButton isEvaluated");
		}
		
		goodButton.setAttribute("onclick", goodCancel + "(" + result[i].postId + ", " + badTrue + ");");
		badButton.setAttribute("onclick", badCancel + "(" + result[i].postId + ", " + goodTrue + ");");
		
		// コメントボタン
		var commentButton = post.querySelector(".commentButton");
		commentButton.setAttribute("onclick", "showDetail(" + result[i].postId + ");");
		
		// タグ検索結果に追加
		posts.appendChild(post);
		
		// 一番下の投稿ID
		endPostId = result[i].postId;
		
		// 投稿数が10ならリロードを表示
		if (i >= 9) {
			reloadDivOn("timeline");
		}
	}
}

// 投稿後に最新の10件のタイムラインを再取得する
function friendReload() {
	MobilePostBean.getPostByFriend(userId, 0,
		// 返ってきた値で処理
		function(result) {
			// タイムラインを再生成する
			remakeTimeline(result);
		}
	);
}

// タイムラインを再生成する
function remakeTimeline(result) {
	
	// タイムラインを初期化
	var posts = document.getElementById("posts");
	while (posts.firstChild) {
		posts.removeChild(posts.firstChild);
	}
	
	// リロードを非表示
	reloadDivOff();
	
	// 投稿リストに含まれる投稿の数だけループ
	for (var i = 0; i<result.length; i++) {
		
		// テンプレートからコピー
		var post = postTemplate.cloneNode(true);
		
		// ユーザーアイコン
		post.querySelector(".userLink").setAttribute("href", "/teamb/pictsharemobile/user/" + result[i].userId);
		post.querySelector(".userIcon").setAttribute("src", "/teamb/img/user/" + result[i].iconFile);
		
		// ユーザー名
		var userName = post.querySelector(".user-name");
		userName.appendChild(document.createTextNode(result[i].dispName));
		
		// 投稿日時
		var postDate = post.querySelector(".postDate");
		postDate.appendChild(document.createTextNode(dateToString(result[i].postDate)));
		
		// 投稿文
		var message = post.querySelector(".document");
		message.innerHTML = result[i].message;
		
		// 写真
		post.querySelector(".photoImg").setAttribute("id", result[i].postId);
		post.querySelector(".photoImg").setAttribute("src", "/teamb/img/post/" + result[i].imgFile[0]);
		
		// Good
		var good = post.querySelector(".good");
		good.setAttribute("id", "good" + result[i].postId);
		good.appendChild(document.createTextNode("Good:" + result[i].good));
		
		// Bad
		var bad = post.querySelector(".bad");
		bad.setAttribute("id", "bad" + result[i].postId);
		bad.appendChild(document.createTextNode("Bad:" + result[i].bad));
		
		// コメント数
		var comment = post.querySelector(".comment");
		comment.appendChild(document.createTextNode("コメント:" + result[i].commentNum + "件"));
		
		// Good ボタン
		var goodButton = post.querySelector(".goodButton");
		goodButton.setAttribute("id", "goodButton" + result[i].postId);
		var goodTrue   = "false";
		var goodCancel = "good";
		// ユーザーが高評価済み
		if (result[i].isGood == 1) {
			goodTrue   = "true";
			goodCancel = "goodCancel";
			goodButton.setAttribute("class", "goodButton isEvaluated");
		}
		
		// Bad ボタン
		var badButton = post.querySelector(".badButton");
		badButton.setAttribute("id", "badButton" + result[i].postId);
		var badTrue   = "false";
		var badCancel = "bad";
		// ユーザーが低評価済み
		if (result[i].isGood == 2) {
			badTrue   = "true";
			badCancel = "badCancel";
			badButton.setAttribute("class", "badButton isEvaluated");
		}
		
		goodButton.setAttribute("onclick", goodCancel + "(" + result[i].postId + ", " + badTrue + ");");
		badButton.setAttribute("onclick", badCancel + "(" + result[i].postId + ", " + goodTrue + ");");
		
		// コメントボタン
		var commentButton = post.querySelector(".commentButton");
		commentButton.setAttribute("onclick", "showDetail(" + result[i].postId + ");");
		
		// タグ検索結果に追加
		posts.appendChild(post);
		
		// 一番下の投稿ID
		endPostId = result[i].postId;
		
		// 投稿数が10ならリロードを表示
		if (i >= 9) {
			reloadDivOn("timeline");
		}
	}
}

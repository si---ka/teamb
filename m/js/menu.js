/* メニュー(ヘッダ)
   
   初期設定でメニュー(id="menu")にタッチイベントを追加する
   ホーム、投稿、検索、ランキング、アカウントボタンを押したときは、
   それぞれのURLに飛ぶ
   
   タイムラインを押したときは、友達閲覧と地図指定を表示・非表示する
   友達閲覧、地図指定を押すと、それぞれのURLに飛ぶ
   設定もタイムラインと同様である
   
   表示・非表示の状態を 0 か 1 で変数に入れて、
   クリック時に高さと縦位置を変えている
 */

var timelineFlag = false; // 友達と地図が表示されているかのフラグ
var settingFlag  = false; // アカウント設定とログアウトが表示されているかのフラグ
var postFlag     = false; // 写真投稿画面が表示されているかのフラグ

var menuHeight = 0; // メニューの縦サイズ

var headerDialogButtonSize = 0; // ヘッダのダイアログのボタンの大きさ

// メニューの初期設定
function menuInit() {
	
	// メニューの縦サイズ
	menuHeight = parseInt(document.body.clientWidth / 7);
	document.getElementById("menu").style.height = menuHeight + "px";
	document.getElementById("menuBottom").style.height = menuHeight + "px";
	
	// ヘッダのダイアログのボタンの大きさ
	var w = document.body.clientWidth / 2;
	var h = document.body.clientHeight;
	if (w > h) {
		headerDialogButtonSize = parseInt(h * 3 / 5);
	} else {
		headerDialogButtonSize = parseInt(w * 3 / 5);
	}
	document.getElementById("friendButton").style.width  = headerDialogButtonSize + "px";
	document.getElementById("friendButton").style.height = headerDialogButtonSize + "px";
	document.getElementById("mapButton").style.width  = headerDialogButtonSize + "px";
	document.getElementById("mapButton").style.height = headerDialogButtonSize + "px";
	document.getElementById("accountSettingButton").style.width  = headerDialogButtonSize + "px";
	document.getElementById("accountSettingButton").style.height = headerDialogButtonSize + "px";
	document.getElementById("logoutButton").style.width  = headerDialogButtonSize + "px";
	document.getElementById("logoutButton").style.height = headerDialogButtonSize + "px";
	document.getElementById("headerDialogContent").style.width =
	  (headerDialogButtonSize * 2.3) + "px";
	document.getElementById("headerDialogContent").style.height =
	  (headerDialogButtonSize * 1.1) + "px";
	document.getElementById("headerDialogContent").style.left =
	  (w - (headerDialogButtonSize * 1.15)) + "px";
	document.getElementById("headerDialogContent").style.top =
	  ((h / 2) - (headerDialogButtonSize * 0.55)) + "px";
	
	
	//タイムラインのダイアログ表示時に他の場所がクリックされると消える
	document.getElementById("headerDialog").addEventListener("touchend", function(e) {
		if (e.target.parentElement.id != "headerDialogContent") {
			if (timelineFlag) {
				document.getElementById("headerDialog").style.visibility = "hidden";
				document.getElementById("headerDialogContent").style.opacity = "0";
				document.getElementById("friendButton").style.visibility = "hidden";
				document.getElementById("friendButton").style.opacity = "0";
				document.getElementById("mapButton").style.visibility = "hidden";
				document.getElementById("mapButton").style.opacity = "0";
				timelineFlag = false;
			}
		}
	});
	//設定のダイアログ表示時に他の場所がクリックされると消える
	document.getElementById("headerDialog").addEventListener("touchend", function(e) {
		if (e.target.parentElement.id != "headerDialogContent") {
			if (settingFlag) {
				document.getElementById("headerDialog").style.visibility = "hidden";
				document.getElementById("headerDialogContent").style.opacity = "0";
				document.getElementById("accountSettingButton").style.visibility = "hidden";
				document.getElementById("accountSettingButton").style.opacity = "0";
				document.getElementById("logoutButton").style.visibility = "hidden";
				document.getElementById("logoutButton").style.opacity = "0";
				settingFlag = false;
			}
		}
	});
	
	
	// ヘッダーの各項目を押したときにそれぞれの処理
	document.getElementById("menu").addEventListener("touchend", function(e) {
		
		// ホームが押されたとき
		if (e.target.parentElement.id == "homeButton") {
			location.href = "/teamb/pictsharemobile/home";
		}
		
		// 投稿が押されたとき
		if (e.target.parentElement.id == "postButton") {
			// ログインしていなければ終了
			if (userId == "null") {
				return;
			}
			//投稿画面が表示されていた場合は消す
			if (postFlag) {
				
				// 背景を消す
				document.getElementById("postCover").style.opacity = "0";
				document.getElementById("postCover").style.visibility = "hidden";
				
				// 投稿画面を消す
				document.getElementById("postDialog").style.visibility = "hidden";
				document.getElementById("postDialogFileSelect").style.visibility = "hidden";
				document.getElementById("postDialogMapSelect").style.visibility  = "hidden";
				
				postFlag = false;
			} else {
				// 位置情報取得
				getPos();
				
				// 縦スクロール量を取得する
				var scroll = document.documentElement.scrollTop || document.body.scrollTop;
				
				// 投稿画面の位置をスクロール量だけずらす
				document.getElementById("postDialog").style.marginTop =
				  (scroll + menuHeight) + "px";
				
				// 背景を暗くする
				document.getElementById("postCover").style.opacity = "0.6";
				document.getElementById("postCover").style.visibility = "visible";
				
				// 投稿画面を表示する
				document.getElementById("postDialogFileSelect").style.visibility = "visible";
				document.getElementById("postDialogMapSelect").style.visibility  = "hidden";
				document.getElementById("postDialog").style.visibility = "visible";
				
				postFlag = true;
				//タイムラインダイアログが表示されていれば消す
				if (timelineFlag) {
					document.getElementById("headerDialog").style.visibility = "hidden";
					document.getElementById("headerDialogContent").style.opacity = "0";
					document.getElementById("friendButton").style.visibility = "hidden";
					document.getElementById("friendButton").style.opacity = "0";
					document.getElementById("mapButton").style.visibility = "hidden";
					document.getElementById("mapButton").style.opacity = "0";
					timelineFlag = false;
				}
				//設定ダイアログが表示されていれば消す
				if (settingFlag) {
					document.getElementById("headerDialog").style.visibility = "hidden";
					document.getElementById("headerDialogContent").style.opacity = "0";
					document.getElementById("accountSettingButton").style.visibility = "hidden";
					document.getElementById("accountSettingButton").style.opacity = "0";
					document.getElementById("logoutButton").style.visibility = "hidden";
					document.getElementById("logoutButton").style.opacity = "0";
					settingFlag = false;
				}
			}
		}
		
		// タイムラインが押されたとき
		if (e.target.parentElement.id == "timelineButton") {
			
			// アカウント設定とログアウトが表示されているなら
			// 友達と地図に変える
			if (settingFlag) {
				document.getElementById("friendButton").style.visibility = "visible";
				document.getElementById("friendButton").style.opacity = "1";
				document.getElementById("mapButton").style.visibility = "visible";
				document.getElementById("mapButton").style.opacity = "1";
				document.getElementById("accountSettingButton").style.visibility = "hidden";
				document.getElementById("accountSettingButton").style.opacity = "0";
				document.getElementById("logoutButton").style.visibility = "hidden";
				document.getElementById("logoutButton").style.opacity = "0";
				timelineFlag = true;
				settingFlag  = false;
			}
			
			// 友達と地図が表示されているなら消す
			else if (timelineFlag) {
				document.getElementById("headerDialog").style.visibility = "hidden";
				document.getElementById("headerDialogContent").style.opacity = "0";
				document.getElementById("friendButton").style.visibility = "hidden";
				document.getElementById("friendButton").style.opacity = "0";
				document.getElementById("mapButton").style.visibility = "hidden";
				document.getElementById("mapButton").style.opacity = "0";
				timelineFlag = false;
			}
			
			// 何も表示されていないなら友達と地図を表示
			else {
				document.getElementById("headerDialog").style.visibility = "visible";
				document.getElementById("headerDialogContent").style.opacity = "1";
				document.getElementById("friendButton").style.visibility = "visible";
				document.getElementById("friendButton").style.opacity = "1";
				document.getElementById("mapButton").style.visibility = "visible";
				document.getElementById("mapButton").style.opacity = "1";
				timelineFlag = true;
			}
		}
		
		// 検索が押されたとき
		if (e.target.parentElement.id == "searchButton") {
			location.href = "/teamb/pictsharemobile/search";
		}
		
		// ランキングが押されたとき
		if (e.target.parentElement.id == "rankingButton") {
			location.href = "/teamb/pictsharemobile/ranking";
		}
		
		// アカウントが押されたとき
		if (e.target.parentElement.id == "accountButton") {
			// ログインしていなければ終了
			if (userId == "null") {
				return;
			}
			location.href = "/teamb/pictsharemobile/user/"+userId;
		}
		
		// 設定が押されたとき
		if (e.target.parentElement.id == "settingButton") {
			
			// 友達と地図が表示されているなら
			// アカウント設定とログアウトに変える
			if (timelineFlag) {
				document.getElementById("accountSettingButton").style.visibility = "visible";
				document.getElementById("accountSettingButton").style.opacity = "1";
				document.getElementById("logoutButton").style.visibility = "visible";
				document.getElementById("logoutButton").style.opacity = "1";
				document.getElementById("friendButton").style.visibility = "hidden";
				document.getElementById("friendButton").style.opacity = "0";
				document.getElementById("mapButton").style.visibility = "hidden";
				document.getElementById("mapButton").style.opacity = "0";
				settingFlag  = true;
				timelineFlag = false;
			}
			
			// アカウント設定とログアウトが表示されているなら消す
			else if (settingFlag) {
				document.getElementById("headerDialog").style.visibility = "hidden";
				document.getElementById("headerDialogContent").style.opacity = "0";
				document.getElementById("accountSettingButton").style.visibility = "hidden";
				document.getElementById("accountSettingButton").style.opacity = "0";
				document.getElementById("logoutButton").style.visibility = "hidden";
				document.getElementById("logoutButton").style.opacity = "0";
				settingFlag = false;
			}
			
			// 何も表示されていないならアカウント設定とログアウトを表示
			else {
				document.getElementById("headerDialog").style.visibility = "visible";
				document.getElementById("headerDialogContent").style.opacity = "1";
				document.getElementById("accountSettingButton").style.visibility = "visible";
				document.getElementById("accountSettingButton").style.opacity = "1";
				document.getElementById("logoutButton").style.visibility = "visible";
				document.getElementById("logoutButton").style.opacity = "1";
				settingFlag = true;
			}
			
		}
	});
	
	// メニューの上でスクロールを止める
	document.getElementById("menu").addEventListener("touchstart", function(e) {
		e.preventDefault();
	});
	
	// 友達閲覧が押されたとき
	document.getElementById("friendButton").addEventListener("touchend", function(e) {
		location.href = "/teamb/pictsharemobile/friend";
	});
	
	// 地図指定が押されたとき
	document.getElementById("mapButton").addEventListener("touchend", function(e) {
		location.href = "/teamb/pictsharemobile/map";
	});
	
	// アカウント設定が押されたとき
	document.getElementById("accountSettingButton").addEventListener("touchend", function(e) {
		// ログインしていなければ終了
		if (userId == "null") {
			return;
		}
		location.href = "/teamb/pictsharemobile/setting";
	});
	
	// ログアウトが押されたとき
	document.getElementById("logoutButton").addEventListener("touchend", function(e) {
		location.href = "/teamb/pictsharemobile/logout";
	});
}

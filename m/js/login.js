/* ログイン画面
   
   切り替えボタン(id="changeButton")を押すと
   ログインと新規登録を切り替える
   
   left 値を変えて左右に動かし、見えなくなったほうの visibility は hidden にする
   
   新規登録でエラーがあった場合は、transition を 0秒にしてから新規登録に切り替える
   その後 transition を元に戻す
 */

var isLogin = true; // ログイン / 新規登録

// ログイン / 新規登録切り替え
function change() {
	
	var loginDiv     = document.getElementById("loginDiv");
	var registerDiv  = document.getElementById("registerDiv");
	var changeButton = document.getElementById("changeButton");
	
	// 新規登録に切り替え
	if (isLogin) {
		loginDiv.style.left = "-100%";
		loginDiv.style.visibility = "hidden";
		registerDiv.style.left = "0px";
		registerDiv.style.visibility = "visible";
		changeButton.innerHTML = "ログインはアチラ";
		isLogin = false;
	}
	
	// ログインに切り替え
	else {
		loginDiv.style.left = "0%";
		loginDiv.style.visibility = "visible";
		registerDiv.style.left = "100%";
		registerDiv.style.visibility = "hidden";
		changeButton.innerHTML = "新規登録はコチラ";
		isLogin = true;
	}
}

// 新規登録エラー時用
function regError() {
	
	var loginDiv     = document.getElementById("loginDiv");
	var registerDiv  = document.getElementById("registerDiv");
	var changeButton = document.getElementById("changeButton");
	
	// 新規登録に切り替え
	loginDiv.style.transitionDuration = "0s, 0s";
	loginDiv.style.left = "-100%";
	loginDiv.style.visibility = "hidden";
	registerDiv.style.transitionDuration = "0s, 0s";
	registerDiv.style.left = "0px";
	registerDiv.style.visibility = "visible";
	changeButton.innerHTML = "ログインはアチラ";
	isLogin = false;
	
	// トランジションを戻す
	setTimeout(function() {
		loginDiv.style.transitionDuration = "400ms, 400ms";
		registerDiv.style.transitionDuration = "400ms, 400ms";
	}, 10);
}

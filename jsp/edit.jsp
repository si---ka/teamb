<%@ page contentType="text/html; charset=UTF-8" %>

<%--
投稿編集・削除画面

/teamb/pictshare/edit にアクセスした後に表示される
セッションの UserBean と PostBean の情報から各項目を取得する
編集ボタンを押すと /teamb/pictshare/editdone に、
削除ボタンを押すと /teamb/pictshare/deletedone に POST される

写真は変更不可
写真が複数枚ある場合は 2枚目以降小さく表示

削除ボタンを押したときは、確認メッセージを出す
--%>

<%@ page import="java.util.ArrayList" %>
<jsp:useBean id="user" class="pictshare.UserBean" scope="session" />
<jsp:useBean id="editPost" class="pictshare.PostBean" scope="session" />

<!DOCTYPE html>
<html lang="ja">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	
	<!-- タイトル -->
	<title>PictShare</title>
	
	<!-- CSS -->
	<!-- ヘッダ -->
	<link rel="stylesheet" href="/teamb/css/header.css">
	<!-- 投稿編集・削除 -->
	<link rel="stylesheet" href="/teamb/css/edit.css">
	<!-- 写真投稿 -->
	<link rel="stylesheet" href="/teamb/css/post.css">
	
	<!-- JavaScript -->
	<!-- グーグルマップ -->
	<script src="http://maps.google.com/maps/api/js?v=3&libraries=places&sensor=false"></script>
	<!-- ヘッダ -->
	<script src="/teamb/js/header.js"></script>
	<!-- 投稿編集・削除 -->
	<script src="/teamb/js/edit.js"></script>
	<!-- 写真投稿 -->
	<script src="/teamb/js/post.js"></script>
	
	<script>
		var userId = "<%= user.getUserId() %>"; // ユーザーID
		
		// 画面読み込み完了後に行う初期設定
		function init() {
			editInit(<%= editPost.getLat() %>,
			  <%= editPost.getLng() %>); // 投稿編集用マップの初期設定
			postInit(<%= user.getInitLat() %>,
			  <%= user.getInitLng() %>); // 写真投稿画面のマップの初期設定
		}
		
		// 削除の確認
		function del() {
			if (window.confirm("本当に削除しますか？")) {
				document.delform.submit();
			}
			return false;
		}
	</script>
</head>

<body onload="init()">
<!--ページヘッダー【includeしています】-->
<%@ include file="/jsp/header.jsp" %>

<article>
	<center>
<%
	// リクエストにメッセージがあれば取得
	String message = (String)request.getAttribute("message");
	if (message != null) {
		out.print("<span class=\"errorMsg\">" + message + "</span>");
	}
%>
	</center>
	
	<form method="POST" action="/teamb/pictshare/editdone" onsubmit="return false;">
		
		<div class="row">
			投稿文 140文字まで<br />
			「&amp;」、「&lt;」、「&gt;」、「&quot;」、「&#39;」、「\」は使用できません<br />
<%
	// 投稿文の取得
	// 1回目は PostBean の投稿文、エラーで 2回目以降取得するときは直前の入力の投稿文
	String postMsg = (String)request.getAttribute("postmsg");
	if (postMsg == null) {
		postMsg = editPost.getMessage();
	}
%>
			<textarea name="postmsg" rows="4" cols="75" maxlength="140" style="width: 100%;"><%= postMsg %></textarea>
		</div>
		
		<div class="row">
			<div>写真は変更できません</div>
<%
	// 写真の取得
	ArrayList<String> imgFile = editPost.getImgFile();
%>
			<img class="mainImg" src="/teamb/img/post/<%= imgFile.get(0) %>" />
<%
	// 写真が 2枚以上あるときはサブの写真として小さく表示
	for (int i=1; i<imgFile.size(); i++) {
%>
			<img src="/teamb/img/post/<%= imgFile.get(i) %>" />
<%
	}
%>
		</div>
		
		<div class="row">
			写真の位置情報<br />
			<div class="mapField">
				<div id="editMap"></div>
				<input id="editMapTextField" class="controls" type="text">
			</div>
		</div>
		<input type="hidden" id="editLat" name="lat" value="<%= editPost.getLat() %>" />
		<input type="hidden" id="editLng" name="lng" value="<%= editPost.getLng() %>" />
		
			<!--編集ボタン-->
			<input class="edit-button"  type="button" value="編集" onclick="submit();" />
	</form>
	
	<form name="delform" method="POST" action="/teamb/pictshare/deletedone">
			<!--削除ボタン-->
			<input class="delete-button" type="button" value="削除" onclick="del();" />

	</form>
</article>

<!-- 写真投稿画面 -->
<%@ include file="/jsp/post.jsp" %>

</body>
</html>

<%------------------------------------------------------------------------------------------------------------------------------------------------------------
ファイル名 : profile.jsp
作  成  日 : 2014/12/10
更  新  日 : 2014/12/15
作  成  者 : 0J01027 吉満  隆次
※プロフィール画面
-------------------------------------------------------------------------------------------------------------------------------------------------------------%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="pictshare.TagBean" %>
<%
	// おすすめタグの取得
	TagBean[] recommendedTag = (TagBean[])application.getAttribute("recommendedTag");
%>

<!--ここからコードを書く-->
<figure class="photo-left">
	<div class="profileDiv">
		<div id="profileDivTop">
			<!--【ユーザーアイコン】-->
			<div id="profileUserIcon">
				<a href="/teamb/pictshare/user/<%= user.getUserId() %>"><img src="/teamb/img/user/<%= user.getIconFile() %>" /></a>
			</div>
			
			<!--【ユーザー名】-->
			<div id="profileDispName">
				<%= user.getDispName() %>
			</div>
			
			<!--【ユーザーID】-->
			<div id="profileUserId">
				<%= user.getUserId() %>
			</div>
			
			<!--【紹介文】-->
			<div id="profileProfile">
				<%= user.getProfile() %>
			</div>
		</div>
		
		<div id="profileDivBottom">
			<!--【Follow フォロー数】-->
			<div id="profileFollow">
				<a href="/teamb/pictshare/user/<%= user.getUserId() %>/follow" class="followLink">
					Follow<br />
					<%= user.getFollowId().size() %>
				</a>
			</div>
			
			<!--【Follower フォロワー数】-->
			<div id="profileFollower">
				<a href="/teamb/pictshare/user/<%= user.getUserId() %>/follower" class="followLink">
					Follower<br />
					<%= user.getFollowerId().size() %>
				</a>
			</div>
		</div>
	</div>
	
	<div class="profileDiv" style="padding: 30px;">
		<h3>HOT TAG</h3>
		<p class="tagtext">
			<div class="rTagNo">
				No.1&nbsp;
			</div>
			<div class="rTag" onclick="searchPost(<%= recommendedTag[0].getTagId() %>);">
				#<%= recommendedTag[0].getTagName() %>
			</div>
			<div class="rTagNo">
				No.2&nbsp;
			</div>
			<div class="rTag" onclick="searchPost(<%= recommendedTag[1].getTagId() %>);">
				#<%= recommendedTag[1].getTagName() %>
			</div>
			<div class="rTagNo">
				No.3&nbsp;
			</div>
			<div class="rTag" onclick="searchPost(<%= recommendedTag[2].getTagId() %>);">
				#<%= recommendedTag[2].getTagName() %>
			</div>
		</p>
	</div>
</figure>

<%@ page contentType="text/html; charset=UTF-8" %>

<%--
地図指定閲覧画面

最初は、ユーザーの初期位置周辺の投稿(PostBean)のリストが
サーブレットによってセッションにセットされている
その PostBean の情報から投稿ID、緯度、経度、メッシュコードを取得する

投稿ID、緯度、経度、メッシュコードは、
JavaScript の postArray という変数にセットして、
geoPin.js でピンを立てる時に利用する
--%>

<%@ page import="java.util.ArrayList" %>
<%@ page import="pictshare.PostBean" %>
<jsp:useBean id="user" class="pictshare.UserBean" scope="session" />
<!DOCTYPE html>
<html lang="ja">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	
	<!-- タイトル -->
	<title>PictShare</title>
	
	<!-- CSS -->
	<!-- ページ全体 ユーザーや投稿 地図 -->
	<link rel="stylesheet" href="/teamb/css/style.css">
	<!-- ヘッダ -->
	<link rel="stylesheet" href="/teamb/css/header.css">
	<!-- 投稿詳細 -->
	<link rel="stylesheet" href="/teamb/css/detail.css">
	<!-- 写真投稿 -->
	<link rel="stylesheet" href="/teamb/css/post.css">
	
	<!-- JavaScript -->
	<!-- グーグルマップ関係 -->
	<script type="text/javascript" src="/teamb/js/import.js"></script>
	<!-- DWR -->
		<!-- コメント -->
		<script src="/teamb/dwr/interface/CommentBean.js"></script>
		<!-- 投稿 -->
		<script src="/teamb/dwr/interface/PostBean.js"></script>
		<!-- 今月ランキング Good用 -->
		<script src="/teamb/dwr/interface/ThisRankingBean.js"></script>
		<!-- DWR本体 -->
		<script src="/teamb/dwr/engine.js"></script>
	<!-- ヘッダ -->
	<script src="/teamb/js/header.js"></script>
	<!-- 投稿詳細 -->
	<script src="/teamb/js/detail.js"></script>
	<!-- 評価 -->
	<script src="/teamb/js/goodbad.js"></script>
	<!-- 地図指定閲覧用 ピン -->
	<script src="/teamb/js/geoPin.js"></script>
	<!-- 地図指定閲覧用 投稿取得 -->
	<script src="/teamb/js/geoGetPost.js"></script>
	<!-- 地図指定閲覧用 タイムライン -->
	<script src="/teamb/js/geoTimeline.js"></script>
	<!-- 写真投稿 -->
	<script src="/teamb/js/post.js"></script>
	
	<script>
		var userId = "<%= user.getUserId() %>"; // ユーザーID
		var postArray = []; // 投稿リスト
<%
	// セッションに投稿リストがあれば取得
	ArrayList<PostBean> postList = (ArrayList<PostBean>)session.getAttribute("postList");
	
	// 投稿リストに含まれる投稿の数だけループ
	if (postList != null) {
		for (int i=0; i<postList.size(); i++) {
%>
		// 投稿リストに投稿ID、緯度、経度、メッシュコードをセット
		postArray[<%= i %>] = {
			id  : <%= postList.get(i).getPostId() %>,
			lat : <%= postList.get(i).getLat() %>,
			lng : <%= postList.get(i).getLng() %>,
			meshcode : <%= postList.get(i).getMeshCode() %>
		}
<%
		}
	}
%>
		// 画面読み込み完了後に行う初期設定
		function init() {
			initialize(<%= user.getInitLat() %>,
			  <%= user.getInitLng() %>); // マップの初期設定
			getPostInit(); // 投稿取得の初期設定
			pinInit();     // ピンを使う場合の初期設定
			detailInit();  // 投稿詳細の初期設定
			postInit(<%= user.getInitLat() %>,
			  <%= user.getInitLng() %>); // 写真投稿画面のマップの初期設定
		}
	</script>
</head>

<body onload="init()" style='overflow: hidden;'>
<!--ページヘッダー【includeしています】-->
<%@ include file="/jsp/header.jsp" %>

<div class="mapField">
	<div id="map"></div>
	<input id="mapTextField" class="controls" type="text">
	<input class="submit-button-follow" id="mapsettingbutton" type="button" value="詳細検索" onclick="pushsyousai();" >
	<div id="timelineBox">
		<!-- 地図指定閲覧用タイムライン -->
		<%@ include file="/jsp/timeline.jsp" %>
	</div>
</div>

<!-- 投稿詳細 -->
<%@ include file="/jsp/detail.jsp" %>

<!-- 写真投稿画面 -->
<%@ include file="/jsp/post.jsp" %>

<!-- マップ詳細設定Dialog -->
<%@ include file="/jsp/timesearch.jsp" %>

</body>
</html>

<%------------------------------------------------------------------------------------------------------------------------------------------------------------
ファイル名 : header.jsp
作  成  日 : 2014/12/04
更  新  日 : 2014/12/15
作  成  者 : 0J01027 吉満  隆次
※ヘッダー画面
-------------------------------------------------------------------------------------------------------------------------------------------------------------%>
<%@ page contentType="text/html; charset=UTF-8" %>

<!--ヘッダー部分-->
<header>
	<h1>
		<div id="header">
			<!-- ロゴ -->
			<div id="logoDiv">
				<img id="logoImg" src="/teamb/img/login/icon.png" />
			</div>
			
			<!-- ヘッダー部分の【HOME】ボタン -->
			<div id="homeButton" class="headerButton"></div>
			
			<!-- ヘッダー部分の【POST】ボタン -->
			<div id="postButton" class="headerButton"></div>
			
			<!-- ヘッダー部分の【TIMELINE】ボタン -->
			<div id="timelineButton" class="headerButton"></div>
			
			<!-- ヘッダー部分の【SEARCH】ボタン -->
			<div id="searchButton" class="headerButton"></div>
			
			<!-- ヘッダー部分の【RANKING】ボタン -->
			<div id="rankingButton" class="headerButton"></div>
			
			<!-- ヘッダー部分の【SETTING】ボタン -->
			<div id="settingButton" class="headerButton"></div>
			
			<!-- ヘッダー部分の【ACCOUNT】ボタン -->
			<div id="accountButton" class="headerButton"></div>
		</div>
	</h1>
	
	<br/>
</header>

<%--
ヘッダのTIMELINE、SETTINGを押したとき、
別の画面(ヘッダダイアログ)を上に重ねる
--%>

<!-- ヘッダダイアログ -->
<div id="headerDialog">
	<div id="headerDialogContent">
		<!-- 【友達閲覧】ボタン -->
		<div id="friendButton" class="headerDialogButton headerDialogButtonLeft"></div>
		
		<!-- 【地図指定】ボタン -->
		<div id="mapButton" class="headerDialogButton headerDialogButtonRight"></div>
		
		<!-- 【アカウント設定】ボタン -->
		<div id="accountSettingButton" class="headerDialogButton headerDialogButtonLeft"></div>
		
		<!-- 【ログアウト】ボタン -->
		<div id="logoutButton" class="headerDialogButton headerDialogButtonRight"></div>
	</div>
</div>

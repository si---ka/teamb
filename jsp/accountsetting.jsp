<%@ page contentType="text/html; charset=UTF-8" %>

<%--
アカウント設定画面

/teamb/pictshare/setting にアクセスした後に表示される
セッションの UserBean の情報から各項目を取得する
設定変更ボタンを押すと /teamb/pictshare/settingdone に POST される

奇数行を class="oddRow"、偶数行を class="evenRow" にして色分け
行の左は class="leftRow"、行の右は class="rightRow"

ユーザーID、ユーザー名、メールアドレスは変更不可

ユーザーアイコンは、既に設定されているものをプレビュー(id="preview")に表示しておく
アイコンが新たにアップロードされると、accountsetting.js の prev() を実行し、
見えないキャンバス(id="canvas" style="display:none")で処理した後に、
プレビューに表示する

地図の初期位置設定は、見えない input (id="lat", id="lng") に設定しておく
--%>

<jsp:useBean id="user" class="pictshare.UserBean" scope="session" />

<!DOCTYPE html>
<html lang="ja">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	
	<!-- タイトル -->
	<title>PictShare</title>
	
	<!-- CSS -->
	<!-- アカウント設定 -->
	<link rel="stylesheet" href="/teamb/css/accountsetting.css">
	<!-- ヘッダ -->
	<link rel="stylesheet" href="/teamb/css/header.css">
	<!-- 写真投稿 -->
	<link rel="stylesheet" href="/teamb/css/post.css">
	
	<!-- JavaScript -->
	<!-- グーグルマップ -->
	<script src="http://maps.google.com/maps/api/js?v=3&libraries=places&sensor=false"></script>
	<!-- ヘッダ -->
	<script src="/teamb/js/header.js"></script>
	<!-- アカウント設定 -->
	<script src="/teamb/js/accountsetting.js"></script>
	<!-- 写真投稿 -->
	<script src="/teamb/js/post.js"></script>
	
	<script>
		var userId = "<%= user.getUserId() %>"; // ユーザーID
		
		// 画面読み込み完了後に行う初期設定
		function init() {
			accountSettingInit(<%= user.getInitLat() %>,
			  <%= user.getInitLng() %>); // アカウント設定画面のマップの初期設定
			postInit(<%= user.getInitLat() %>,
			  <%= user.getInitLng() %>); // 写真投稿画面のマップの初期設定
		}
	</script>
</head>

<body onload="init()">
<!--ページヘッダー【includeしています】-->
<%@ include file="/jsp/header.jsp" %>

<article>
	<center>
		<h1><%= user.getDispName() %>さんのアカウント設定</h1>
<%
	// リクエストにメッセージがあれば取得
	String message = (String)request.getAttribute("message");
	if (message != null) {
		out.print("<span class=\"errorMsg\">" + message + "</span>");
	}
%>
	</center>
	
	<form method="POST" enctype="multipart/form-data" action="/teamb/pictshare/settingdone" onsubmit="return false;">
	
		<div class="oddRow">
			<div class="leftRow">ユーザーID</div>
			<div class="rightRow"><%= user.getUserId() %></div>
		</div>
		
		<div class="evenRow">
			<div class="leftRow">メールアドレス</div>
			<div class="rightRow"><%= user.getMail() %></div>
		</div>
		
		<div class="oddRow">
			<div class="leftRow">ユーザー名</div>
			<div class="rightRow"><%= user.getDispName() %></div>
		</div>
		
		<div class="evenRow">
			<div class="leftRow">パスワード</div>
			<div class="rightRow">
				変更する場合のみ入力してください 英数字のみ 20文字まで<br />
				<input type="password" name="pass" maxlength="20" />
			</div>
		</div>
		
		<div class="oddRow">
			<div class="leftRow">ユーザーアイコン</div>
			<div class="rightRow">
				<div id="preview">
					<img src="/teamb/img/user/<%= user.getIconFile() %>" />
				</div>
				<div>
					<input id="inputFile" type="file" name="file" onchange="prev(this);" accept="image/*" />
				</div>
				<canvas id="canvas" style="display:none" width="150" height="150"></canvas>
			</div>
		</div>
		
		<div class="evenRow">
			<div class="leftRow">プロフィール</div>
			<div class="rightRow">
				140文字まで<br />
				「&amp;」、「&lt;」、「&gt;」、「&quot;」、「&#39;」、「\」は使用できません<br />
<%
	// プロフィールの取得
	String profile = (String)request.getAttribute("profile");
	if (profile == null) {
		profile = user.getProfile();
	}
%>
				<textarea name="profile" rows="5" cols="60" maxlength="140"><%= profile %></textarea>
			</div>
		</div>
		
		<div class="oddRow">
			<div class="leftRow">鍵設定</div>
			<div class="rightRow">
				鍵をかけると あなたの投稿は<br />
				フォローを許可した人以外 閲覧不可能になります<br />
				ランキングの集計対象からも外れます<br />
<%
	// 鍵つきフラグの取得
	// 片方だけ checked を入れる
	String lockTrueChecked  = "";
	String lockFalseChecked = "";
	if (user.getIsLocked()) {
		lockTrueChecked = "checked ";
	} else {
		lockFalseChecked = "checked ";
	}
%>
				<input type="radio" name="lock" value="true" <%= lockTrueChecked %>/>かける
				<input type="radio" name="lock" value="false" <%= lockFalseChecked %>/>かけない
			</div>
		</div>
		
		<div class="evenRow">
			<div class="leftRow">ホーム画面設定</div>
			<div class="rightRow">
<%
	// 画面設定の取得
	// 片方だけ checked を入れる
	String friendChecked = "";
	String mapChecked    = "";
	if (user.getIsMapMode()) {
		mapChecked = "checked ";
	} else {
		friendChecked = "checked ";
	}
%>
				<input type="radio" name="home" value="firend" <%= friendChecked %>/>タイムライン友達版
				<input type="radio" name="home" value="map" <%= mapChecked %>/>タイムライン地図版
			</div>
		</div>
		
		<div class="oddRow">
			<div class="leftRow">
				タイムライン地図版で<br />
				初めに表示する位置
			</div>
			<div class="rightRow">
				<div class="mapField">
					<div id="map"></div>
					<input id="mapTextField" class="controls" type="text">
				</div>
			</div>
		</div>
		<input type="hidden" id="lat" name="lat" value="<%= user.getInitLat() %>" />
		<input type="hidden" id="lng" name="lng" value="<%= user.getInitLng() %>" />
		
		<center>
			<input class="setting-button" type="button" value="設定変更" onclick="submit();" />
		</center>
	</form>
</article>

<!-- 写真投稿画面 -->
<%@ include file="/jsp/post.jsp" %>

</body>
</html>

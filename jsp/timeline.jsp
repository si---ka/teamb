<%@ page contentType="text/html; charset=UTF-8" %>

<%--
地図指定閲覧用タイムライン

postdetail.jsp とほぼ同じだけど、
こっちはサイズが決まっていて、はみ出したらスクロールバーをつける
--%>

<%@ page import="java.text.SimpleDateFormat" %>

<div id="posts" class="keisen">
	<div id="scroll" class='postline'>
<%
	// YYYY-MM-DD hh:mm 形式
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	
	// 投稿リストに含まれる投稿の数だけループ
	if (postList != null) {
		for (int i=0; i<postList.size(); i++) {
			String goodIsEvaluated = ""; // 高評価ボタンの色用
			String badIsEvaluated  = ""; // 低評価ボタンの色用
			String goodCancel      = ""; // 高評価ボタンキャンセル用
			String badCancel       = ""; // 低評価ボタンキャンセル用
			String badTrue  = "false"; // 高評価時に低評価のキャンセルを行う
			String goodTrue = "false"; // 低評価時に高評価のキャンセルを行う
			
			// ユーザーが高評価済み
			if (postList.get(i).getIsGood() == 1) {
				goodIsEvaluated = " isEvaluated";
				goodCancel = "Cancel";
				goodTrue = "true";
			}
			
			// ユーザーが低評価済み
			else if (postList.get(i).getIsGood() == 2) {
				badIsEvaluated = " isEvaluated";
				badCancel = "Cancel";
				badTrue = "true";
			}
%>
		<!-- geoTimeline.js の narrow() で絞る時用の投稿ID -->
		
		<div id="p<%= postList.get(i).getPostId() %>" class='postline'>
			<!--【8.ユーザーアイコン】-->
			<figure class="user_photo_left">
				<!-- ユーザー情報へのリンク -->
				<a class="userLink" href="/teamb/pictshare/user/<%= postList.get(i).getUserId() %>">
					<img class="userIcon" src="/teamb/img/user/<%= postList.get(i).getIconFile() %>" />
				</a>
			</figure>
			
			<!--【9.ユーザー名】-->
			<div class="user-name">
				<%= postList.get(i).getDispName() %>
			</div>
			
			<!--【投稿日時】-->
			<div class="postDate">
				<%= df.format(postList.get(i).getPostDate()) %>
			</div>
			
			<!--【10.文章】-->
			<div class="document">
				<%= postList.get(i).getMessage() %>
			</div>
			
			<!--【11.写真】-->
			<div class="postphoto" >
				<!-- クリックしたときの処理のため id に投稿IDをもたせる -->
				<img id="<%= postList.get(i).getPostId() %>" class="photoImg" src="/teamb/img/post/<%= postList.get(i).getImgFile().get(0) %>" />
			</div>
			
			<div class="Good_Bad_Coment">
				<span id="good<%= postList.get(i).getPostId() %>" class="good">Good:<%= postList.get(i).getGood() %></span>
				<span id="bad<%= postList.get(i).getPostId() %>" class="bad">Bad:<%= postList.get(i).getBad() %></span>
				<span class="comment">コメント:<%= postList.get(i).getCommentNum() %>件</span>
			</div>
			
			<div class="evaluateDiv" style="width: 476px; height: 36px;">
				<div id="goodButton<%= postList.get(i).getPostId() %>" class="goodButton<%= goodIsEvaluated %>" onclick="good<%= goodCancel %>(<%= postList.get(i).getPostId() %>, <%= badTrue %>);"></div>
				<div id="badButton<%= postList.get(i).getPostId() %>" class="badButton<%= badIsEvaluated %>" onclick="bad<%= badCancel %>(<%= postList.get(i).getPostId() %>, <%= goodTrue %>);"></div>
				<div class="commentButton" onclick="showDetail(<%= postList.get(i).getPostId() %>);"></div>
			</div>
		</div>
<%
		}
	}
%>
	</div>
</div>

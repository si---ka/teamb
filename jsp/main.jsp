<%------------------------------------------------------------------------------------------------------------------------------------------------------------
ファイル名 : main.jsp(画面設計書D03トップページ友達閲覧)
作  成  日 : 2014/12/02
更  新  日 : 2014/12/20
作  成  者 : 0J01027 吉満  隆次
※各画面は別ファイル画面設計書画面設計書D03トップページ友達閲覧を参照してください。
-------------------------------------------------------------------------------------------------------------------------------------------------------------%>

<%@ page contentType="text/html; charset=UTF-8" %>
<jsp:useBean id="user" class="pictshare.UserBean" scope="session" />
<!DOCTYPE html>
<html lang="ja">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />

	<!-- タイトル -->
	<title>PictShare</title>

	<!-- CSS -->
	<!-- ページ全体 ユーザーや投稿 地図 -->
	<link rel="stylesheet" href="/teamb/css/style.css">
	<!-- ヘッダ -->
	<link rel="stylesheet" href="/teamb/css/header.css">
	<!-- 投稿詳細 -->
	<link rel="stylesheet" href="/teamb/css/detail.css">

	<!-- 写真投稿 -->
	<link rel="stylesheet" href="/teamb/css/post.css">
	
	<!-- JavaScript -->
	<!-- グーグルマップ -->
	<script src="http://maps.google.com/maps/api/js?v=3&libraries=places&sensor=false"></script>
	<!-- DWR -->
		<!-- コメント -->
		<script src="/teamb/dwr/interface/CommentBean.js"></script>
		<!-- 投稿 -->
		<script src="/teamb/dwr/interface/PostBean.js"></script>
		<!-- 今月ランキング Good用 -->
		<script src="/teamb/dwr/interface/ThisRankingBean.js"></script>
		<!-- DWR本体 -->
		<script src="/teamb/dwr/engine.js"></script>
	<!-- ヘッダ -->
	<script src="/teamb/js/header.js"></script>
	<!-- 投稿詳細 -->
	<script src="/teamb/js/detail.js"></script>
	<!-- 評価 -->
	<script src="/teamb/js/goodbad.js"></script>
	<!-- おすすめタグ -->
	<script src="/teamb/js/recommendedtag.js"></script>
	<!-- 写真投稿 -->
	<script src="/teamb/js/post.js"></script>
	
	<script>
		var userId = "<%= user.getUserId() %>"; // ユーザーID
		
		// 画面読み込み完了後に行う初期設定
		
		function init() {
			detailInit(); // 投稿詳細の初期設定
			postInit(<%= user.getInitLat() %>,
			  <%= user.getInitLng() %>); // 写真投稿画面のマップの初期設定
			isFriend = true; // 投稿時にタイムラインをリロード
		}
	</script>
</head>

<body onload="init()">
<!--ページヘッダー【includeしています】-->
<%@ include file="/jsp/header.jsp" %>

<!--【ページセンター】ページ上の白ページ-->
<article>
	
	<!--【Heading1】-->
	<center>
		<h2 style="color: white;">
<%
	// リクエストにメッセージがあれば取得
	String message = (String)request.getAttribute("message");
	if (message != null) {
		out.print(message);
	}
%>
		</h2>
	</center>
	
 	<!-- プロフィール -->
 	<%@ include file="/jsp/profile.jsp" %>
 	
 	<!-- 投稿 -->
 	<%@ include file="/jsp/postdetail.jsp" %>
	
</article>

<!-- 投稿詳細 -->
<%@ include file="/jsp/detail.jsp" %>

<!-- 写真投稿画面 -->
<%@ include file="/jsp/post.jsp" %>

</body>
</html>

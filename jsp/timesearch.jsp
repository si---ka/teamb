<%@ page contentType="text/html; charset=UTF-8" %>

<%--
日付ごとに検索できるタイアログ

--%>
<!-- 写真投稿時の背景 -->
<div id="settingCover"></div>

<!-- 写真投稿画面 -->
<div id="settingDialog">
	<div id="setting_article">
		<form method="POST" name="settingForm" onsubmit="return false;">
			<input type="radio" name="searchType" onclick="kikanclick();" />期間検索<br />
			<input id="todaystart" type="date" name="start" disabled />～<input id="todayend" type="date" name="end" disabled /><br /><br />
			<input type="radio" name="searchType" onclick="dayclick();" />日付検索<br />
			<input id="today" type="date" name="day" disabled><br /><br />
			<input type="radio" name="searchType" onclick="noneclick();" checked />指定なし<br /><br />
			
			<!--詳細検索ボタン-->
			<input id= "syousai-button" class="submit-button-follow" type="button" value="詳細検索" onclick="timeSearch();" />
		</form>
	</div>
</div>

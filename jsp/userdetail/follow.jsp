<%@ page contentType="text/html; charset=UTF-8" %>

<%--
フォロー/フォロワーリスト画面

/teamb/pictshare/user/Piyohiko/follow
/teamb/pictshare/user/Piyohiko/follower など

セッションの relation には URL に含まれる対象ユーザーとの関係が
セッションの targetUser には対象ユーザーの情報が
セッションの targetFollowList と targetFollowerList のどちらかには
対象ユーザーのフォロー/フォロワーリストがセットされている

セットされている方を表示する
フォロー/フォロワーがいなくても、null でなく、要素数が 0 の ArrayList がセットされる
--%>

<%@ page import="java.util.ArrayList" %>
<%@ page import="pictshare.UserBean" %>
<jsp:useBean id="user" class="pictshare.UserBean" scope="session" />
<jsp:useBean id="targetUser" class="pictshare.UserBean" scope="session" />
<%
	String listTitle = ""; // リストのタイトル
	
	// フォローリストを取得
	ArrayList<UserBean> list = (ArrayList<UserBean>)session.getAttribute("targetFollowList");
	
	// フォローリストがある
	if (list != null) {
		listTitle = targetUser.getDispName() + "さんのFollow";
	}
	
	// フォローリストがない
	else {
		// フォロワーリストを取得
		list = (ArrayList<UserBean>)session.getAttribute("targetFollowerList");
		listTitle = targetUser.getDispName() + "さんのFollower";
	}
%>

<!DOCTYPE html>
<html lang="ja">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	
	<!-- タイトル -->
	<title>PictShare</title>
	
	<!-- CSS -->
	<!-- ページ全体 ユーザーや投稿 地図 -->
	<link rel="stylesheet" href="/teamb/css/style.css">
	<!-- ヘッダ -->
	<link rel="stylesheet" href="/teamb/css/header.css">
	<!-- フォロー/フォロワーリスト フォローリクエスト -->
	<link rel="stylesheet" href="/teamb/css/follow.css">
	<!-- 写真投稿 -->
	<link rel="stylesheet" href="/teamb/css/post.css">
	
	<!-- JavaScript -->
	<!-- グーグルマップ -->
	<script src="http://maps.google.com/maps/api/js?v=3&libraries=places&sensor=false"></script>
	<!-- DWR -->
		<!-- ユーザー -->
		<script src="/teamb/dwr/interface/UserBean.js"></script>
		<!-- DWR本体 -->
		<script src="/teamb/dwr/engine.js"></script>
	<!-- ヘッダ -->
	<script src="/teamb/js/header.js"></script>
	<!-- フォロー -->
	<script src="/teamb/js/follow.js"></script>
	<!-- 写真投稿 -->
	<script src="/teamb/js/post.js"></script>
	
	<script>
		var userId = "<%= user.getUserId() %>"; // ユーザーID
		
		// 画面読み込み完了後に行う初期設定
		function init() {
			postInit(<%= user.getInitLat() %>,
			  <%= user.getInitLng() %>); // 写真投稿画面のマップの初期設定
		}
	</script>
</head>

<body onload="init()">
<!--ページヘッダー【includeしています】-->
<%@ include file="/jsp/header.jsp" %>

<!--【ページセンター】ページ上の白ページ-->
<article>
	
	<!--【Heading1】-->
	<center>
		<h2 style="color: white;">
			<%= listTitle %>
		</h2>
	</center>
	
	<!-- 対象ユーザーのプロフィール -->
	<%@ include file="/jsp/userdetail/targetprofile.jsp" %>
	
	<div class="keisen">
<%
	// リストに含まれるユーザーの数だけループ
	if (list != null) {
		for (int i=0; i<list.size(); i++) {
%>
		<div class="userDiv">
			
			<!-- ユーザーアイコン -->
			<figure class="userSearch">
				<a class="userLink" href="/teamb/pictshare/user/<%= list.get(i).getUserId() %>">
					<img class="userIcon" src="/teamb/img/user/<%= list.get(i).getIconFile() %>">
				</a>
			</figure>
			
			<!-- ユーザーID -->
			<div class="user-id">
				ユーザーID:<%= list.get(i).getUserId() %>
			</div>
			
			<br />
			
			<!-- ユーザー名 -->
			<div class="user-name">
				ユーザー名:<%= list.get(i).getDispName() %>
			</div>
			
			<br />
			
			<!-- 紹介文 -->
			<div class="profile">
				自己紹介文:<br />
				<%= list.get(i).getProfile() %>
			</div>
		</div>
<%
		}
	}
%>
	</div>
</article>

<!-- 写真投稿画面 -->
<%@ include file="/jsp/post.jsp" %>

</body>
</html>

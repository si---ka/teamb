<%@ page contentType="text/html; charset=UTF-8" %>

<%--
対象ユーザーのプロフィール

セッションの relation から対象ユーザーとの関係を取得する
関係によってフォローボタンの表示が変わる

対象ユーザーが自分なら、ボタン非表示
対象ユーザーをフォロー済みなら、フォロー解除
対象ユーザーをフォロー申請中なら、フォロー申請中(クリック不可)
対象ユーザーを未フォローで鍵なしなら、フォローする
対象ユーザーを未フォローで鍵つきなら、フォロー申請を送る

各ボタンを押したときに follow.js の処理を呼び出す
--%>

<%
	String button = ""; // フォローボタン
	
	// セッションから対象ユーザーとの関係を取得
	String relation = (String)session.getAttribute("relation");
	if (relation == null || targetUser.getUserId().equals("null")) {
		relation = "";
	}
	
	// フォロー済み
	if (relation.equals("follow")) {
		button = "<input class=\"submit-button-follow\" id=\"btn\" type=\"button\" value=\"フォロー解除\" " +
		  "onclick=\"notfollow('" + user.getUserId() + "', '" + targetUser.getUserId() + "');\" />";
	}
	
	// フォロー申請中
	else if (relation.equals("followWait")) {
		button = "<input class=\"submit-button-follow\" id=\"btn\" type=\"button\" value=\"フォロー申請中\" disabled />";
	}
	
	// 未フォロー鍵なし
	else if (relation.equals("notFollow")) {
		button = "<input class=\"submit-button-follow\" id=\"btn\" type=\"button\" value=\"フォローする\" " +
		  " onclick=\"follow('" + user.getUserId() + "', '" + targetUser.getUserId() + "');\" />";
	}
	
	// 未フォロー鍵つき
	else if (relation.equals("locked")) {
		button = "<input class=\"submit-button-follow\" id=\"btn\" type=\"button\" value=\"フォロー申請を送る\" " +
		  "onclick=\"sendRequest('" + user.getUserId() + "', '" + targetUser.getUserId() + "');\" />";
	}
%>

<!--ここからコードを書く-->
<figure class="photo-left">
	<div class="profileDiv">
		<div id="profileDivTop">
			<!--【ユーザーアイコン】-->
			<div id="profileUserIcon">
				<a href="/teamb/pictshare/user/<%= targetUser.getUserId() %>"><img src="/teamb/img/user/<%= targetUser.getIconFile() %>" /></a>
			</div>
			
			<!--【ユーザー名】-->
			<div id="profileDispName">
				<%= targetUser.getDispName() %>
			</div>
			
			<!--【ユーザーID】-->
			<div id="profileUserId">
				<%= targetUser.getUserId() %>
			</div>
			
			<!--【紹介文】-->
			<div id="profileProfile">
				<%= targetUser.getProfile() %>
			</div>
		</div>
		
		<div id="profileDivBottom">
			<!--【Follow フォロー数】-->
			<div id="profileFollow">
				<a href="/teamb/pictshare/user/<%= targetUser.getUserId() %>/follow" class="followLink">
					Follow<br />
					<%= targetUser.getFollowId().size() %>
				</a>
			</div>
			
			<!--【Follower フォロワー数】-->
			<div id="profileFollower">
				<a href="/teamb/pictshare/user/<%= targetUser.getUserId() %>/follower" class="followLink">
					Follower<br />
					<%= targetUser.getFollowerId().size() %>
				</a>
			</div>
		</div>
	</div>
	<br />
	<%= button %>
</figure>

<%@ page contentType="text/html; charset=UTF-8" %>

<%
	String initMode = ""; // エラーがあった時にフォームを表示する
	
	// リクエストにID（ログイン用）があれば取得
	String id = (String)request.getAttribute("id");
	if (id == null) {
		id = "";
	}
	
	// リクエストにID（登録用）があれば取得
	String rId = (String)request.getAttribute("rId");
	if (rId == null) {
		rId = "";
	}
	
	// リクエストにメールアドレスがあれば取得
	String rMail = (String)request.getAttribute("rMail");
	if (rMail == null) {
		rMail = "";
	}
	
	// リクエストに表示名があれば取得
	String rName = (String)request.getAttribute("rName");
	if (rName == null) {
		rName = "";
	}
	
	// ログインエラーがあれば色を変える
	String loginError = (String)request.getAttribute("loginError");
	if (loginError == null) {
		loginError = "";
	} else {
		initMode = "showLogIn();";
	}
	
	// IDのエラーがあれば色を変える
	String rIdError = (String)request.getAttribute("rIdError");
	if (rIdError == null) {
		rIdError = "";
	} else {
		initMode = "showSignUp();";
	}
	
	// パスワードのエラーがあれば色を変える
	String rPassError = (String)request.getAttribute("rPassError");
	if (rPassError == null) {
		rPassError = "";
	} else {
		initMode = "showSignUp();";
	}
	
	// メールアドレスのエラーがあれば色を変える
	String rMailError = (String)request.getAttribute("rMailError");
	if (rMailError == null) {
		rMailError = "";
	} else {
		initMode = "showSignUp();";
	}
	
	// 表示名のエラーがあれば色を変える
	String rNameError = (String)request.getAttribute("rNameError");
	if (rNameError == null) {
		rNameError = "";
	} else {
		initMode = "showSignUp();";
	}
%>

<!DOCTYPE html>
<html lang="ja">

<head>
	<meta charset="UTF-8" />
	
	<!-- タイトル -->
	<title>Pictshare</title>
	
	<!-- CSS -->
	<link rel="stylesheet" href="/teamb/css/login.css" />
	<link rel="icon" href="/teamb/favicon.ico" />
	
	<!-- JavaScript -->
	<script src="/teamb/js/login.js"></script>
	
	<script>
		// 画面読み込み完了後に行う初期設定
		function init() {
			loginInit();    // ログイン画面の初期設定
			<%= initMode %> // エラーがあった時にフォームを表示する
		}
	</script>
</head>

<body onload="init()">
	<!-- 背景画像 -->
	<div id="back0" class="backImage absDiv">
		<div id="word0" class="words">
			Pictshare<hr />
		</div>
		<div id="subWord0" class="subWords" style="font-size: 1.5em;">
			Share and connect with the Pictshare Community.<br />
			Discover billions of beautiful pictures.
		</div>
	</div>
	<div id="back1" class="backImage absDiv">
		<div id="word1" class="words">
			Pictshare<hr />
		</div>
		<div id="subWord1" class="subWords">
			All your pictures in one place.
		</div>
	</div>
	<div id="back2" class="backImage absDiv">
		<div id="word2" class="words">
			Pictshare<hr />
		</div>
		<div id="subWord2" class="subWords">
			Share Your Pictures.
		</div>
	</div>
	
	<!-- 背景画像以外の div -->
	<div id="front0" class="absDiv">
		<!-- メニュー -->
		<div id="miniMenuDiv">
			<!-- スマートフォン版へ -->
			<a id="mobile" href="/teamb/pictshare/mobile" class="miniMenu">Smartphone site</a>
			<!-- 新規登録 -->
			<span class="miniMenu floatRight" onclick="signUp();">Sign up</span>
			<!-- ログイン -->
			<span class="miniMenu floatRight" onclick="logIn();">Log in</span>
		</div>
		
		<!-- ロゴ -->
		<div><img src="/teamb/img/login/logowhite.png"></div>
		
		<!-- 左ボタン -->
		<div id="leftButton" onclick="prevBack();"></div>
		
		<!-- 右ボタン -->
		<div id="rightButton" onclick="nextBack();"></div>
		
		<!-- 現在の背景画像を表す丸 -->
		<div id="dot0" class="dots" onclick="selectBack(0);"></div>
		<div id="dot1" class="dots" onclick="selectBack(1);"></div>
		<div id="dot2" class="dots" onclick="selectBack(2);"></div>
		
		<!-- 新規登録 -->
		<div id="signUpDiv" class="appearDiv">
			<form method="POST" action="/teamb/pictshare/register">
				<input type="text" name="UserName" class="form-field<%= rNameError %>" value="<%= rName %>" placeholder="ユーザー名" /><br />
				<input type="text" name="UserID" class="form-field<%= rIdError %>" value="<%= rId %>" placeholder="ユーザーID" /><br />
				<input type="password" name="PASS" class="form-field<%= rPassError %>" placeholder="パスワード" /><br />
				<input type="text" name="Mail" class="form-field<%= rMailError %>" value="<%= rMail %>" placeholder="メールアドレス" /><br />
				<button type="submit" class="submit-button">登録</button>
			</form>
		</div>
		
		<!-- ログイン -->
		<div id="logInDiv" class="appearDiv">
			<form method="POST" action="/teamb/pictshare/login">
				<input type="text" name="loginUserID" class="form-field<%= loginError %>" value="<%= id %>" placeholder="ユーザーID" /><br />
				<input type="password" name="loginPASS" class="form-field<%= loginError %>" placeholder="パスワード" /><br />
				<button type="submit" class="submit-button">ログイン</button>
			</form>
		</div>
	</div>
</body>

</html>

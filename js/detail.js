/* 投稿詳細
   
   タイムラインの写真(class="photoImg")を押したときに、
   背景の黒い画面(id="cover")と、投稿詳細(id="postDetail")を表示する
   
   それと同時に DWR で投稿詳細とコメントを取得する
   タイムラインの写真には id に投稿IDがついているので、それを利用する
   取得した投稿詳細の情報で各HTMLエレメントの中身を書き換える
   
   コメントは数が変わるので、テンプレートを用意しておく(高速らしい)
   コメントの数だけテンプレートからコピーし、中身を書いて追加する
   
   メインの写真をクリックすると、地図に切り替える
   地図をクリックすると、メインの写真に切り替える
   
   サブの写真をクリックすると、メインの写真と入れ替える
   
   コメント投稿ボタンを押すと、DWR でコメントをデータベースに登録する
   エラーメッセージがあれば表示する
   
   右上の×(id="closeDetail")をクリックすると、背景の黒い画面と投稿詳細を消す
 */

var comTemplate; // コメントのテンプレート

var detailMap    = null;  // 投稿詳細用のマップ
var detailMarker = null;  // 投稿詳細用のマーカー
var isMapMode    = false; // 地図を表示しているかのフラグ

// コメントのテンプレートを生成する
makeComTemplate();

// コメントのテンプレートを生成する
function makeComTemplate() {
	comTemplate = document.createElement("div");
	comTemplate.setAttribute("class", "comSet");
	
	// ユーザーアイコン
	var user = document.createElement("figure");
	user.setAttribute("class", "user_photo_left");
	var userLink = document.createElement("a");
	userLink.setAttribute("class", "userLink");
	var userIcon = document.createElement("img");
	userIcon.setAttribute("class", "userIcon");
	userLink.appendChild(userIcon);
	user.appendChild(userLink);
	comTemplate.appendChild(user);
	
	// ユーザー名
	var userName = document.createElement("div");
	userName.setAttribute("class", "user-name");
	comTemplate.appendChild(userName);
	
	// 改行
	var br = document.createElement("br");
	comTemplate.appendChild(br);
	
	// コメント文
	var message = document.createElement("div");
	message.setAttribute("class", "msgCom");
	comTemplate.appendChild(message);
}

// 投稿詳細の初期設定
function detailInit() {
	
	//マップのオプション
	var mapOption = {
		zoom: 10,                                //初期半径を指定
		center: new google.maps.LatLng(35, 135), //初期位置を指定
		zoomControl: true,                       //ズームのバー true:表示 false:非表示
		panControl: false,                       //移動の矢印
		mapTypeControl: true,                    //マップのタイプ
		overviewMapControl: false,               //ミニマップの表示
		rotateControl: false,                    //９０℃回転の有無
		streetViewControl: false,                //ペグマンコントロールの有無
		
		mapTypeControlOptions:{
			style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR //横並びで切り替えボタンを表示
		},
		mapTypeId: google.maps.MapTypeId.ROADMAP, //mapの形式を指定
	}
	//html文のdetailMapにmapOptionの規則に従ったマップを作成
	detailMap = new google.maps.Map(document.getElementById("detailMap"), mapOption);
	
	// タイムラインの写真を押したときに投稿詳細を表示する
	document.getElementById("posts").addEventListener("click", function(e) {
		
		// 写真が押されたとき
		if (e.target.className == "photoImg") {
			// 投稿詳細を表示する
			showDetail(e.target.id);
		}
	});
	
	// 投稿詳細表示 右上の×を押したときに投稿詳細を消す
	document.getElementById("closeDetail").addEventListener("click", function(e) {
		
		// 背景を消す
		document.getElementById("cover").style.opacity = "0.0";
		document.getElementById("cover").style.visibility = "hidden";
		
		// 投稿詳細を消す
		document.getElementById("postDetail").style.visibility = "hidden";
	});
	
	// 小さい写真を押したときにメインと入れ替える
	document.getElementById("subImgDiv").addEventListener("click", function(e) {
		
		// 地図モードなら終了
		if (isMapMode) {
			return;
		}
		
		// 写真が押されたとき
		if (e.target.className == "subImg") {
			var id = e.target.id;
			
			// メインの写真
			var main = document.getElementById("photo0");
			
			// 小さい写真
			var sub = document.getElementById(id);
			
			// メインを透明にする
			main.style.opacity = "0.0";
			
			// 100ミリ秒後
			setTimeout(function() {
				
				// 入れ替え
				var temp = main.getAttribute("src");
				main.setAttribute("src", sub.getAttribute("src"));
				sub.setAttribute("src", temp);
				
				// メインを表示
				main.style.opacity = "1.0";
			}, 100);
		}
	});
	
	// 投稿詳細の高さを画面の高さより小さくなるように設定
	document.getElementById("detailScroll").style.maxHeight =
	  (document.body.clientHeight - 145) + "px";
}

// 投稿詳細を表示する
function showDetail(postId) {
	
	// 投稿詳細を取得して生成
	getPostByPostId(postId);
	
	// コメントを取得して生成
	getCommentByPostId(postId);
	
	// 縦スクロール量を取得する
	var scroll = document.documentElement.scrollTop || document.body.scrollTop;
	
	// 投稿詳細の位置をスクロール量だけずらす
	document.getElementById("articleDetail").style.marginTop = (scroll + 20) + "px";
	
	// 背景を暗くする
	document.getElementById("cover").style.opacity = "0.6";
	document.getElementById("cover").style.visibility = "visible";
	
	// 投稿詳細を写真モードにする
	isMapMode = false;
	document.getElementById("detailMap").style.display = "none";
	document.getElementById("detailPhoto").style.display = "block";
	
	// 投稿詳細を表示する
	document.getElementById("postDetail").style.visibility = "visible";
}

// 地図の切り替え
function changeMap(){
	if(isMapMode){
		isMapMode = false;
		document.getElementById("detailMap").style.display = "none";
		document.getElementById("detailPhoto").style.display = "block";
	} else {
		isMapMode = true;
		document.getElementById("detailMap").style.display = "block";
		document.getElementById("detailPhoto").style.display = "none";
	}
}

// 投稿を取得
function getPostByPostId(postId) {
	
	// 投稿IDで投稿を取得
	PostBean.getPostByPostId(postId, true,
		
		// 返ってきた値で処理
		function(result) {
			
			// 投稿詳細を生成する
			makeDetail(result);
		}
	);
}

// コメントを取得
function getCommentByPostId(postId) {
	
	// 投稿IDでコメントを取得
	CommentBean.getCommentByPostId(postId,
		
		// 返ってきた値で処理
		function(result) {
			
			// コメントを生成する
			makeComment(result);
		}
	);
}

// 投稿詳細を生成する
function makeDetail(result) {
	
	var ad = document.getElementById("articleDetail");
	
	// メインの投稿写真
	document.getElementById("photo0").setAttribute("src", "/teamb/img/post/" + result.imgFile[0]);
	
	// メイン以外の投稿写真
	var i;
	for (i=1; i<result.imgFile.length; i++) {
		var subImg = document.getElementById("photo" + i);
		
		// 写真をセットして表示
		subImg.setAttribute("src", "/teamb/img/post/" + result.imgFile[i]);
		subImg.style.display = "inline";
	}
	// 写真が5枚未満なら残りを非表示
	for ( ; i<5; i++) {
		var subImg = document.getElementById("photo" + i);
		subImg.setAttribute("src", "");
		subImg.style.display = "none";
	}
	
	// 投稿文
	var message = ad.querySelector(".msgDetail");
	message.innerHTML = result.message;
	
	// Good
	var good = ad.querySelector(".good");
	if (result.postId != 0) {
		good.replaceChild(document.createTextNode("Good:" + result.good), good.firstChild);
	} else {
		good.replaceChild(document.createTextNode("Good:-"), good.firstChild);
	}
	
	// Bad
	var bad = ad.querySelector(".bad");
	if (result.postId != 0) {
		bad.replaceChild(document.createTextNode("Bad:" + result.bad), bad.firstChild);
	} else {
		bad.replaceChild(document.createTextNode("Bad:-"), bad.firstChild);
	}
	
	// 投稿IDをセット
	document.getElementById("postId").value = result.postId;
	
	// 位置情報
	var location = new google.maps.LatLng(result.lat, result.lng);
	
	// マーカーが既にあれば削除
	if (detailMarker) {
		detailMarker.setMap(null);
		detailMarker = null;
	}
	
	// マーカーを立てる
	detailMarker = new google.maps.Marker({
		position: location,
		map: detailMap
	});
	
	// 画面中央にする
	detailMap.setCenter(location);
}

// コメントを生成する
function makeComment(result) {
	
	// コメントを初期化
	comment = document.getElementById("comment");
	while (comment.firstChild) {
		comment.removeChild(comment.firstChild);
	}
	
	// コメントリストに含まれるコメントの数だけループ
	for (var i = 0; i<result.length; i++) {
		
		// テンプレートからコピー
		var cm = comTemplate.cloneNode(true);
		
		// ユーザーアイコン
		cm.querySelector(".userLink").setAttribute("href", "/teamb/pictshare/user/" + result[i].userId);
		cm.querySelector(".userIcon").setAttribute("src", "/teamb/img/user/" + result[i].iconFile);
		
		// ユーザー名
		var userName = cm.querySelector(".user-name");
		userName.appendChild(document.createTextNode(result[i].dispName));
		
		// コメント文
		var message = cm.querySelector(".msgCom");
		message.appendChild(document.createTextNode(result[i].message));
		
		// タイムラインに追加
		comment.appendChild(cm);
	}
}

// コメントする
function postComment() {
	
	// ログインしていなければ終了
	if (userId == "null") {
		return;
	}
	
	// 投稿IDを取得
	var postId = document.getElementById("postId").value;
	
	// 投稿IDが 0 なら終了
	if (postId == 0) {
		return;
	}
	
	// コメントをデータベースに登録する
	CommentBean.registerComment(postId, userId,
	  document.getElementById("inputComment").value,
		
		// 返ってきた値で処理
		function(result) {
			
			// エラーなし
			if (!result) {
				// エラーメッセージとコメントを空に
				document.getElementById("error").innerHTML = "";
				document.getElementById("inputComment").value = "";
				
				// コメント再取得
				getCommentByPostId(postId);
			}
			
			// エラーメッセージを表示
			else {
				document.getElementById("error").innerHTML = result;
			}
		}
	);
}

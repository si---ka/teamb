/* 投稿編集・削除
   
   アイコンがアップロードされたとき、
   キャンバスで処理してプレビューに表示
   ファイルサイズのチェックも行う
   
   写真の位置情報を中心にグーグルマップが表示されているので、
   中心にマーカーを立てる
   
   グーグルマップがクリックされたら、その場所を中心にマーカーを立て、
   緯度と経度を、見えない input (id="editLat", id="editLng") に設定する
 */

var editMap    = null; // 投稿編集用のマップ
var editMarker = null; // 投稿編集用のマーカー

// 投稿編集・削除画面のマップの初期設定
function editInit(lat, lng) {
	
	//マップのオプション
	var mapOption = {
		zoom: 10,                                 //初期半径を指定
		center: new google.maps.LatLng(lat, lng), //初期位置を指定
		zoomControl: true,                        //ズームのバー true:表示 false:非表示
		panControl: false,                        //移動の矢印
		mapTypeControl: true,                     //マップのタイプ
		overviewMapControl: false,                //ミニマップの表示
		rotateControl: false,                     //９０℃回転の有無
		streetViewControl: false,                 //ペグマンコントロールの有無
		
		mapTypeControlOptions:{
			style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR //横並びで切り替えボタンを表示
		},
		mapTypeId: google.maps.MapTypeId.ROADMAP, //mapの形式を指定
	}
	//html文のeditMapにmapOptionの規則に従ったマップを作成
	editMap = new google.maps.Map(document.getElementById("editMap"), mapOption);
	
	var mapTextField = document.getElementById("editMapTextField"); //テキストフィールドを受け取る
	editMap.controls[google.maps.ControlPosition.TOP_LEFT].push(mapTextField); //mapにテキストフィールドを入れる
	
	var searchBox = new google.maps.places.SearchBox(mapTextField); //オートコンプリート機能の有効化
	
	google.maps.event.addListener(searchBox, 'places_changed', function() { //場所移動のイベントが起こった場合
		var place = searchBox.getPlaces()[0];           //検索候補から場所を受け取る
		if(!place.geometry){
			return;
		}
		if(place.geometry.viewport){                    //viewportが存在した場合はこっち
			editMap.fitBounds(place.geometry.viewport); //移動場所の指定
			editMap.setZoom(17);                        //検索後の縮尺の指定
		} else {                                        //なかった場合はこっち
			editMap.setCenter(place.geometry.location); //移動場所の指定
			editMap.setZoom(17);                        //検索後の縮尺の指定
		}
	});
	
	// 中央にマーカーを立てる
	editMarker = new google.maps.Marker({
		position: editMap.getCenter(),
		map: editMap
	});
	
	// クリックした場所にマーカーを立てて画面の中央にする
	google.maps.event.addListener(editMap, 'click', function(event) {
		selectCenter(event.latLng);
	});
}

// クリックした場所にマーカーを立てて画面の中央にする
function selectCenter(location) {
	
	// マーカーが既にあれば削除
	if (editMarker) {
		editMarker.setMap(null);
		editMarker = null;
	}
	
	// マーカーを立てる
	editMarker = new google.maps.Marker({
		position: location,
		map: editMap
	});
	
	// リクエストする緯度経度をセット
	document.getElementById("editLat").value = location.lat();
	document.getElementById("editLng").value = location.lng();
	
	// 画面中央にする
	editMap.setCenter(location);
}

/* 地図指定閲覧用 投稿取得
   
   グーグルマップを動かしたときに投稿を取得する
   
   マウスを離したときと、拡大率を変えたときに
   タイマーを起動して、500ミリ秒だけ待つ
   その間にもう一度マウスを離したり、拡大率を変えると、さらに500ミリ秒だけ待つ
   時間がきたら地図の範囲の投稿を取得する
   
   geoPin.js の makePins() を呼び出してピンを立て、
   geoTimeline.js の makeTimeLine() でタイムラインをつくる
 */

var timer; // タイマー

var searchMode = "none"; // 検索モード "oneDay": 日付検索 "span": 期間検索

// 投稿取得の初期設定
function getPostInit() {
	
	// マップ上でマウスを押した時の処理
	google.maps.event.addListener(map, 'mousedown', function(event) {
		// タイマーリセット
		clearTimeout(timer);
	});
	
	// マップ上でマウスを離した時の処理
	google.maps.event.addListener(map, 'mouseup', function(event) {
		setNowCenter(map.getCenter()); //nowCenterに現在の中心座標を格納
		
		// 500ミリ秒後に投稿を取得
		timer = setTimeout(getPostByPosition, 500);
	});
	
	// ズームレベルが変更された時の処理
	google.maps.event.addListener(map, 'zoom_changed', function() {
		zoom = map.getZoom(); //ズームレベルの取得
		
		// タイマーリセット
		clearTimeout(timer);
		
		// 500ミリ秒後に投稿を取得
		timer = setTimeout(getPostByPosition, 500);
	});
	
	// 今日のカレンダーを取得
	var today = new Date();
	today = today.getFullYear() + "-" +
	  ("0" + (today.getMonth() + 1)).slice(-2) + "-" +
	  ("0" + today.getDate()).slice(-2);
	document.getElementById("today").value = today;
	document.getElementById("todaystart").value = today;
	document.getElementById("todayend").value = today;
	
	// 詳細検索画面が表示されている上で、違う場所が押された時
	document.getElementById("settingCover").addEventListener("click", function(e) {
		
		// 背景を消す
		document.getElementById("settingCover").style.opacity = "0";
		document.getElementById("settingCover").style.visibility = "hidden";
		
		// 投稿画面を消す
		document.getElementById("settingDialog").style.visibility = "hidden";
	});
}

// 地図検索画面の詳細検索を押したとき
function pushsyousai() {
	
	// 背景を暗くする
	document.getElementById("settingCover").style.opacity = "0.6";
	document.getElementById("settingCover").style.visibility = "visible";
	
	// 詳細検索画面を表示する
	document.getElementById("settingDialog").style.visibility = "visible";
}

// 期間検索のラジオボタン
function kikanclick(){
	//日付指定の入力フォームを無効にする
	document.settingForm.day.disabled = true;
	//期間指定を有効にする
	document.settingForm.start.disabled = false;
	document.settingForm.end.disabled = false;
	
	// 検索モードを期間検索にする
	searchMode = "span";
}

// 日付検索のラジオボタン
function dayclick(){
	//日付指定の入力フォームを有効にする
	document.settingForm.day.disabled = false;
	//期間指定を無効にする
	document.settingForm.start.disabled = true;
	document.settingForm.end.disabled = true;
	
	// 検索モードを日付検索にする
	searchMode = "oneDay";
}

// 指定なしのラジオボタン
function noneclick(){
	//日付指定の入力フォームを無効にする
	document.settingForm.day.disabled = true;
	//期間指定を無効にする
	document.settingForm.start.disabled = true;
	document.settingForm.end.disabled = true;
	
	// 検索モードを指定なしにする
	searchMode = "none";
}

// 詳細検索画面の詳細検索ボタンが押されたとき
function timeSearch() {
	// 期間検索の場合 開始日と終了日が逆じゃないか確認
	if (searchMode == "span") {
		if (document.getElementById('todaystart').value > document.getElementById('todayend').value) {
			alert('日付指定がおかしいです。');
			return false;
		}
	}
	
	// 投稿を取得
	getPostByPosition();
	
	// 背景を消す
	document.getElementById("settingCover").style.opacity = "0";
	document.getElementById("settingCover").style.visibility = "hidden";
	
	// 詳細検索画面を消す
	document.getElementById("settingDialog").style.visibility = "hidden";
	
	return false;
}

// 投稿を取得
function getPostByPosition() {
	
	// 地図の端の緯度経度を取得
	var bounds = map.getBounds();
	var sw = bounds.getSouthWest();
	var ne = bounds.getNorthEast();
	
	// 緯度経度 ＋ 日付検索
	if (searchMode == "oneDay") {
		var searchDate = document.getElementById('today').value;
		PostBean.getPostByPositionStartDate(userId, sw.lat(), sw.lng(), ne.lat(), ne.lng(),
		  searchDate, resultOutput);
	}
	
	// 緯度経度 ＋ 期間検索
	else if (searchMode == "span") {
		var startDate = document.getElementById('todaystart').value;
		var endDate   = document.getElementById('todayend').value;
		PostBean.getPostByPositionStartEndDate(userId, sw.lat(), sw.lng(), ne.lat(), ne.lng(),
		  startDate, endDate, resultOutput);
	}
	
	// 緯度経度のみ
	else {
		PostBean.getPostByPosition(userId, sw.lat(), sw.lng(), ne.lat(), ne.lng(), resultOutput);
	}
}

// 返ってきた値を処理
function resultOutput(result) {
	
	// 投稿リストを初期化
	postArray = [];
	
	// 返ってきた個数ループ
	// geoPin.js でピンを立てる時用に
	// postArray に投稿ID、緯度経度、メッシュコードを設定
	for (i = 0; i < result.length; i++) {
		postArray[i] = {
			id  : result[i].postId,
			lat : result[i].lat,
			lng : result[i].lng,
			meshcode: result[i].meshCode
		}
	}
	
	// ピンを生成する
	// geoPin.js の処理
	makePins();
	
	// タイムラインを生成する
	// geoTimeline.js の処理
	makeTimeLine(result);
}

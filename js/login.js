/* ログイン画面
   
 */

var signUpFlag = false; // 新規登録のフォームが表示されているか
var logInFlag  = false; // ログインのフォームが表示されているか
var nowBackIdx = 0;     // 現在の背景画像番号
var timer;              // タイマー

// ログイン画面の初期設定
function loginInit() {
	
	// 最初の背景画像、言葉、サブの言葉、丸を表示
	showSet(0);
	
	// 8 秒後に背景画像を変える
	timer = setTimeout(changeBack, 8000);
}

// 背景画像を変える
function changeBack() {
	
	// 現在の背景画像、言葉、サブの言葉、丸を非表示
	hideSet(nowBackIdx);
	
	// 次の番号を計算
	nowBackIdx++;
	if (nowBackIdx == 3) {
		nowBackIdx = 0;
	}
	
	// 次の背景画像、言葉、サブの言葉、丸を表示
	showSet(nowBackIdx);
	
	// 8 秒後に背景画像を変える
	timer = setTimeout(changeBack, 8000);
}

// 左ボタン
function prevBack() {
	
	// タイマーを止める
	clearTimeout(timer);
	
	// 新規登録・ログインのフォームを非表示
	hideSignUp();
	hideLogIn();
	
	// 現在の背景画像、言葉、サブの言葉、丸を非表示
	hideSet(nowBackIdx);
	
	// 前の番号を計算
	nowBackIdx--;
	if (nowBackIdx == -1) {
		nowBackIdx = 2;
	}
	
	// 前の背景画像、言葉、サブの言葉、丸を表示
	showSet(nowBackIdx);
}

// 右ボタン
function nextBack() {
	
	// タイマーを止める
	clearTimeout(timer);
	
	// 新規登録・ログインのフォームを非表示
	hideSignUp();
	hideLogIn();
	
	// 現在の背景画像、言葉、サブの言葉、丸を非表示
	hideSet(nowBackIdx);
	
	// 次の番号を計算
	nowBackIdx++;
	if (nowBackIdx == 3) {
		nowBackIdx = 0;
	}
	
	// 次の背景画像、言葉、サブの言葉、丸を表示
	showSet(nowBackIdx);
}

// 背景画像を表す丸が押されたとき
function selectBack(idx) {
	
	// タイマーを止める
	clearTimeout(timer);
	
	// 新規登録・ログインのフォームを非表示
	hideSignUp();
	hideLogIn();
	
	// 押された番号と現在の番号が異なる
	if (idx != nowBackIdx) {
		
		// 現在の背景画像、言葉、サブの言葉、丸を非表示
		hideSet(nowBackIdx);
		
		// 押された番号にする
		nowBackIdx = idx;
		
		// 押された番号の背景画像、言葉、サブの言葉、丸を表示
		showSet(nowBackIdx);
	}
}

// 背景画像、言葉、サブの言葉、丸を表示
function showSet(idx) {
	// 背景画像を表示
	document.getElementById("back" + idx).style.opacity = "1";
	// 言葉を表示
	document.getElementById("word" + idx).style.opacity = "1";
	// サブの言葉を表示
	document.getElementById("subWord" + idx).style.opacity = "1";
	// 丸を表示
	document.getElementById("dot" + idx).style.backgroundPosition = "left bottom";
}

// 背景画像、言葉、サブの言葉、丸を非表示
function hideSet(idx) {
	// 背景画像を非表示
	document.getElementById("back" + idx).style.opacity = "0";
	// 言葉を非表示
	document.getElementById("word" + idx).style.opacity = "0";
	// サブの言葉を非表示
	document.getElementById("subWord" + idx).style.opacity = "0";
	// 丸を非表示
	document.getElementById("dot" + idx).style.backgroundPosition = "left top";
}

// 新規登録
function signUp() {
	
	// 新規登録のフォームが表示されている
	if (signUpFlag) {
		
		// 新規登録のフォームを非表示
		hideSignUp();
		
		// 8 秒後に背景画像を変える
		timer = setTimeout(changeBack, 8000);
	} else {
		
		// 新規登録のフォームを表示
		showSignUp();
		
		// ログインのフォームを非表示
		hideLogIn();
		
		// タイマーを止める
		clearTimeout(timer);
	}
}

// ログイン
function logIn() {
	
	// ログインのフォームが表示されている
	if (logInFlag) {
		
		// ログインのフォームを非表示
		hideLogIn();
		
		// 8 秒後に背景画像を変える
		timer = setTimeout(changeBack, 8000);
	} else {
		
		// ログインのフォームを表示
		showLogIn();
		
		// 新規登録のフォームを非表示
		hideSignUp();
		
		// タイマーを止める
		clearTimeout(timer);
	}
}

// 新規登録のフォームを表示
function showSignUp() {
	var signUpDiv = document.getElementById("signUpDiv");
	signUpDiv.style.top        = "50%";
	signUpDiv.style.opacity    = "1";
	signUpDiv.style.visibility = "visible";
	signUpFlag = true;
}

// ログインのフォームを表示
function showLogIn() {
	var logInDiv = document.getElementById("logInDiv");
	logInDiv.style.top        = "50%";
	logInDiv.style.opacity    = "1";
	logInDiv.style.visibility = "visible";
	logInFlag = true;
}

// 新規登録のフォームを非表示
function hideSignUp() {
	var signUpDiv = document.getElementById("signUpDiv");
	signUpDiv.style.top        = "60%";
	signUpDiv.style.opacity    = "0";
	signUpDiv.style.visibility = "hidden";
	signUpFlag = false;
}

// ログインのフォームを非表示
function hideLogIn() {
	var logInDiv = document.getElementById("logInDiv");
	logInDiv.style.top        = "60%";
	logInDiv.style.opacity    = "0";
	logInDiv.style.visibility = "hidden";
	logInFlag = false;
}

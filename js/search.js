/* 検索
   
   ユーザーIDのラジオボタンを選ぶと、ユーザーID検索の準備をする
   検索ボックスの左の # を消し、
   タグ検索の結果があれば、それも消す
   
   タグ検索のラジオボタンを選ぶとタグ検索の準備をする
   検索ボックスの左に # をつけて、
   ユーザーID検索の結果があれば消す
   
   それぞれの検索は DWR を使ってする
   
   検索結果のユーザー情報または投稿情報は、テンプレートをコピーして作る
   コピーしたものを書き換えて、検索結果に表示していく
 */

var userTemplate; // ユーザー情報のテンプレート
var postTemplate; // 投稿情報のテンプレート

var isUser = true; // ユーザーID検索なら true タグ検索なら false

// ユーザー情報のテンプレートを生成する
makeUserTemplate();

// 投稿情報のテンプレートを生成する
makePostTemplate();

// ユーザー情報のテンプレートを生成する
function makeUserTemplate() {
	userTemplate = document.createElement("div");
	userTemplate.setAttribute("class", "userDiv");
	
	// ユーザーアイコン
	var figure = document.createElement("figure");
	figure.setAttribute("class", "userSearch");
	var userLink = document.createElement("a");
	userLink.setAttribute("class", "userLink");
	var userIcon = document.createElement("img");
	userIcon.setAttribute("class", "userIcon");
	userLink.appendChild(userIcon);
	figure.appendChild(userLink);
	userTemplate.appendChild(figure);
	
	// ユーザーID
	var userId = document.createElement("div");
	userId.setAttribute("class", "user-id");
	userTemplate.appendChild(userId);
	
	// 改行
	var br = document.createElement("br");
	userTemplate.appendChild(br);
	
	// ユーザー名
	var userName = document.createElement("div");
	userName.setAttribute("class", "user-name");
	userTemplate.appendChild(userName);
	
	// 改行
	br = document.createElement("br");
	userTemplate.appendChild(br);
	
	// 紹介文
	var profile = document.createElement("div");
	profile.setAttribute("class", "profile");
	userTemplate.appendChild(profile);
}

// 投稿情報のテンプレートを生成する
function makePostTemplate() {
	template = document.createElement("div");
	template.setAttribute("class", "postline");
	
	// ユーザーアイコン
	var user = document.createElement("figure");
	user.setAttribute("class", "user_photo_left");
	var userLink = document.createElement("a");
	userLink.setAttribute("class", "userLink");
	var userIcon = document.createElement("img");
	userIcon.setAttribute("class", "userIcon");
	userLink.appendChild(userIcon);
	user.appendChild(userLink);
	template.appendChild(user);
	
	// ユーザー名
	var userName = document.createElement("div");
	userName.setAttribute("class", "user-name");
	template.appendChild(userName);
	
	// 投稿日時
	var postDate = document.createElement("div");
	postDate.setAttribute("class", "postDate");
	template.appendChild(postDate);
	
	// 投稿文
	var message = document.createElement("div");
	message.setAttribute("class", "document");
	template.appendChild(message);
	
	// 写真
	var photo = document.createElement("div");
	photo.setAttribute("class", "postphoto");
	var photoImg = document.createElement("img");
	photoImg.setAttribute("class", "photoImg");
	photo.appendChild(photoImg);
	template.appendChild(photo);
	
	// Good Bad コメント数
	var eval = document.createElement("div");
	eval.setAttribute("class", "Good_Bad_Coment");
	
	// Good
	var good = document.createElement("span");
	good.setAttribute("class", "good");
	eval.appendChild(good);
	eval.appendChild(document.createTextNode(" "));
	
	// Bad
	var bad = document.createElement("span");
	bad.setAttribute("class", "bad");
	eval.appendChild(bad);
	eval.appendChild(document.createTextNode(" "));
	
	// コメント数
	var comment = document.createElement("span");
	comment.setAttribute("class", "comment");
	eval.appendChild(comment);
	template.appendChild(eval);
	
	// 評価ボタンとコメントボタン
	var evaluateDiv = document.createElement("div");
	evaluateDiv.setAttribute("class", "evaluateDiv");
	
	// Good ボタン
	var goodButton = document.createElement("div");
	goodButton.setAttribute("class", "goodButton");
	evaluateDiv.appendChild(goodButton);
	
	// Bad ボタン
	var badButton = document.createElement("div");
	badButton.setAttribute("class", "badButton");
	evaluateDiv.appendChild(badButton);
	
	// コメントボタン
	var commentButton = document.createElement("div");
	commentButton.setAttribute("class", "commentButton");
	evaluateDiv.appendChild(commentButton);
	template.appendChild(evaluateDiv);
}

// 検索
function search() {
	if (isUser) {
		// ユーザーを検索する
		searchUser(document.getElementById("inputSearch").value);
	} else {
		// タグ検索する
		searchPost(document.getElementById("inputSearch").value);
	}
}

// ユーザーIDのラジオボタンをクリック
function userRadio() {
	
	// ユーザーID検索
	isUser = true;
	
	// 入力フォームの"#"を消す
	document.getElementById("hash").innerHTML = "";
	
	// タグ検索結果を初期化
	var posts = document.getElementById("posts");
	while (posts.firstChild) {
		posts.removeChild(posts.firstChild);
	}
	posts.style.display = "none";
}

// タグのラジオボタンをクリック
function tagRadio() {
	
	// タグ検索
	isUser = false;
	
	// 入力フォームに"#"をつける
	document.getElementById("hash").innerHTML = "#";
	
	// ユーザー検索結果を初期化
	var users = document.getElementById("users");
	while (users.firstChild) {
		users.removeChild(users.firstChild);
	}
	users.style.display = "none";
}

// ユーザーを検索する
function searchUser(userId) {
	
	// ユーザーを検索する
	UserBean.searchUserListByUserId(userId,
		
		// 返ってきた値で処理
		function(result) {
			
			// ユーザー情報の検索結果を生成する
			makeUsers(result);
		}
	);
}

// タグ検索する
function searchPost(tag) {
	
	TagBean.getTagIdByTagName(tag,
		function(tagId) {
			
			// タグ検索結果を表示
			var posts = document.getElementById("posts");
			posts.style.display = "block";
			
			// タグIDがあれば検索
			if (tagId != 0) {
				PostBean.getPostByTagId(userId, tagId,
					// 返ってきた値で処理
					function(result) {
						// タグ検索結果を生成する
						makePosts(result);
					}
				);
			}
			
			// タグが登録されていない
			else {
				// タグ検索結果を初期化
				while (posts.firstChild) {
					posts.removeChild(posts.firstChild);
				}
				posts.appendChild(document.createTextNode(
				  "指定のタグを含む投稿が見つかりませんでした"));
			}
		}
	);
}

// ユーザー情報の検索結果を生成する
function makeUsers(result) {
	
	// ユーザー検索結果を初期化
	var users = document.getElementById("users");
	while (users.firstChild) {
		users.removeChild(users.firstChild);
	}
	users.style.display = "block";
	
	// 検索結果0件
	if (result.length == 0) {
		users.appendChild(document.createTextNode("ユーザーが見つかりませんでした"));
	}
	
	// ユーザーリストに含まれるユーザーの数だけループ
	for (var i = 0; i<result.length; i++) {
		
		// テンプレートからコピー
		var us = userTemplate.cloneNode(true);
		
		// ユーザーアイコン
		us.querySelector(".userLink").setAttribute("href", "/teamb/pictshare/user/" + result[i].userId);
		us.querySelector(".userIcon").setAttribute("src", "/teamb/img/user/" + result[i].iconFile);
		
		// ユーザーID
		var userId = us.querySelector(".user-id");
		userId.appendChild(document.createTextNode("ユーザーID:" + result[i].userId));
		
		// ユーザー名
		var userName = us.querySelector(".user-name");
		userName.appendChild(document.createTextNode("ユーザー名:" + result[i].dispName));
		
		// 紹介文
		var profile = us.querySelector(".profile");
		profile.appendChild(document.createTextNode("自己紹介文:"));
		profile.appendChild(document.createElement("br"));
		profile.appendChild(document.createTextNode(result[i].profile));
		
		// ユーザー検索結果に追加
		users.appendChild(us);
	}
}
// タグ検索結果を生成する
function makePosts(result) {
	
	// タグ検索結果を初期化
	var posts = document.getElementById("posts");
	while (posts.firstChild) {
		posts.removeChild(posts.firstChild);
	}
	
	// 検索結果0件
	if (result.length == 0) {
		posts.appendChild(document.createTextNode("指定のタグを含む投稿が見つかりませんでした"));
		return;
	}
	
	// 投稿リストに含まれる投稿の数だけループ
	for (var i = 0; i<result.length; i++) {
		
		// テンプレートからコピー
		var post = template.cloneNode(true);
		
		// ユーザーアイコン
		post.querySelector(".userLink").setAttribute("href", "/teamb/pictshare/user/" + result[i].userId);
		post.querySelector(".userIcon").setAttribute("src", "/teamb/img/user/" + result[i].iconFile);
		
		// ユーザー名
		var userName = post.querySelector(".user-name");
		userName.appendChild(document.createTextNode(result[i].dispName));
		
		// 投稿日時
		var postDate = post.querySelector(".postDate");
		postDate.appendChild(document.createTextNode(dateToString(result[i].postDate)));
		
		// 投稿文
		var message = post.querySelector(".document");
		message.innerHTML = result[i].message;
		
		// 写真
		post.querySelector(".photoImg").setAttribute("id", result[i].postId);
		post.querySelector(".photoImg").setAttribute("src", "/teamb/img/post/" + result[i].imgFile[0]);
		
		// Good
		var good = post.querySelector(".good");
		good.setAttribute("id", "good" + result[i].postId);
		good.appendChild(document.createTextNode("Good:" + result[i].good));
		
		// Bad
		var bad = post.querySelector(".bad");
		bad.setAttribute("id", "bad" + result[i].postId);
		bad.appendChild(document.createTextNode("Bad:" + result[i].bad));
		
		// コメント数
		var comment = post.querySelector(".comment");
		comment.appendChild(document.createTextNode("コメント:" + result[i].commentNum + "件"));
		
		// Good ボタン
		var goodButton = post.querySelector(".goodButton");
		goodButton.setAttribute("id", "goodButton" + result[i].postId);
		var goodTrue   = "false";
		var goodCancel = "good";
		// ユーザーが高評価済み
		if (result[i].isGood == 1) {
			goodTrue   = "true";
			goodCancel = "goodCancel";
			goodButton.setAttribute("class", "goodButton isEvaluated");
		}
		
		// Bad ボタン
		var badButton = post.querySelector(".badButton");
		badButton.setAttribute("id", "badButton" + result[i].postId);
		var badTrue   = "false";
		var badCancel = "bad";
		// ユーザーが低評価済み
		if (result[i].isGood == 2) {
			badTrue   = "true";
			badCancel = "badCancel";
			badButton.setAttribute("class", "badButton isEvaluated");
		}
		
		goodButton.setAttribute("onclick", goodCancel + "(" + result[i].postId + ", " + badTrue + ");");
		badButton.setAttribute("onclick", badCancel + "(" + result[i].postId + ", " + goodTrue + ");");
		
		// コメントボタン
		var commentButton = post.querySelector(".commentButton");
		commentButton.setAttribute("onclick", "showDetail(" + result[i].postId + ");");
		
		// タグ検索結果に追加
		posts.appendChild(post);
	}
}

// 日時を YYYY-MM-DD hh:mm 形式の文字列に変換する
function dateToString(date) {
	return date.getFullYear() + "-" + ("0" + (date.getMonth() + 1)).slice(-2) +
	  "-" + ("0" + date.getDate()).slice(-2) + " " + ("0" + date.getHours()).slice(-2) +
	  ":" + ("0" + date.getMinutes()).slice(-2);
}

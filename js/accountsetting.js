/* アカウント設定
   
   アイコンがアップロードされたとき、
   キャンバスで処理してプレビューに表示
   ファイルサイズのチェックも行う
   
   既に設定されている位置情報を中心にグーグルマップが表示されているので、
   中心にマーカーを立てる
   
   グーグルマップがクリックされたら、その場所を中心にマーカーを立て、
   緯度と経度を、見えない input (id="lat", id="lng") に設定する
 */

var accountSettingMap    = null;  // アカウント設定用のマップ
var accountSettingMarker = null;  // アカウント設定用のマーカー

// 画像のプレビュー
function prev(e) {
	
	// ログインしていなければ終了
	if (userId == "null") {
		return;
	}
	
	var file   = e.files[0],
	    image  = new Image(),
	    reader = new FileReader(),
	    canvas = document.getElementById("canvas"),
	    ctx    = canvas.getContext("2d");
	
	// プレビューが既にあれば消去
	var preview = document.getElementById("preview");
	while (preview.hasChildNodes()) {
		preview.removeChild(preview.firstChild);
	}
	
	// ファイル未選択
	if (!file) {
		return;
	}
	
	// ファイルが 500KB 以上ならエラー
	if (file.size >= (500 * 1024)) {
		
		// エラーメッセージ
		var error = document.createTextNode("500KB以上のファイルはアイコンにできません");
		preview.appendChild(error);
		
		// ファイル選択を初期化
		var inputFile = document.getElementById("inputFile");
		inputFile.parentNode.innerHTML = inputFile.parentNode.innerHTML;
		
		return;
	}
	
	if (file.type.match(/image.*/)) {
		reader.onload = function() {
			image.onload = function() {
				
				// キャンバス初期化
				ctx.clearRect(0, 0, canvas.width, canvas.height);
				
				// キャンバスに描画
				ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
				
				// プレビュー生成
				var img = document.createElement("img");
				img.src = canvas.toDataURL("image/jpeg");
				preview.appendChild(img);
			}
			if (reader.result != "data:") {
				image.src = reader.result;
			}
		}
		reader.readAsDataURL(file);
	}
}

// アカウント設定画面のマップの初期設定
function accountSettingInit(lat, lng) {
	
	//マップのオプション
	var mapOption = {
		zoom: 10,                                 //初期半径を指定
		center: new google.maps.LatLng(lat, lng), //初期位置を指定
		zoomControl: true,                        //ズームのバー true:表示 false:非表示
		panControl: false,                        //移動の矢印
		mapTypeControl: true,                     //マップのタイプ
		overviewMapControl: false,                //ミニマップの表示
		rotateControl: false,                     //９０℃回転の有無
		streetViewControl: false,                 //ペグマンコントロールの有無
		
		mapTypeControlOptions:{
			style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR //横並びで切り替えボタンを表示
		},
		mapTypeId: google.maps.MapTypeId.ROADMAP, //mapの形式を指定
	}
	//html文のmapにmapOptionの規則に従ったマップを作成
	accountSettingMap = new google.maps.Map(document.getElementById("map"), mapOption);
	
	var mapTextField = document.getElementById("mapTextField"); //テキストフィールドを受け取る
	accountSettingMap.controls[google.maps.ControlPosition.TOP_LEFT].push(mapTextField); //mapにテキストフィールドを入れる
	
	var searchBox = new google.maps.places.SearchBox(mapTextField); //オートコンプリート機能の有効化
	
	google.maps.event.addListener(searchBox, 'places_changed', function() { //場所移動のイベントが起こった場合
		var place = searchBox.getPlaces()[0];           //検索候補から場所を受け取る
		if(!place.geometry){
			return;
		}
		if(place.geometry.viewport){                    //viewportが存在した場合はこっち
			accountSettingMap.fitBounds(place.geometry.viewport); //移動場所の指定
			accountSettingMap.setZoom(17);                        //検索後の縮尺の指定
		} else {                                        //なかった場合はこっち
			accountSettingMap.setCenter(place.geometry.location); //移動場所の指定
			accountSettingMap.setZoom(17);                        //検索後の縮尺の指定
		}
	});
	
	// 中央にマーカーを立てる
	accountSettingMarker = new google.maps.Marker({
		position: accountSettingMap.getCenter(),
		map: accountSettingMap
	});
	
	// クリックした場所にマーカーを立てて画面の中央にする
	google.maps.event.addListener(accountSettingMap, 'click', function(event) {
		selectCenter(event.latLng);
	});
}

// クリックした場所にマーカーを立てて画面の中央にする
function selectCenter(location) {
	
	// マーカーが既にあれば削除
	if (accountSettingMarker) {
		accountSettingMarker.setMap(null);
		accountSettingMarker = null;
	}
	
	// マーカーを立てる
	accountSettingMarker = new google.maps.Marker({
		position: location,
		map: accountSettingMap
	});
	
	// リクエストする緯度経度をセット
	document.getElementById("lat").value = location.lat();
	document.getElementById("lng").value = location.lng();
	
	// 画面中央にする
	accountSettingMap.setCenter(location);
}

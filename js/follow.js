/* フォロー
   
   他のユーザーのユーザー情報閲覧画面では、
   フォローする、フォロー解除、フォロー申請を送る等のボタンがある
   
   フォローリクエスト許可・拒否画面では、
   承認ボタンと拒否ボタンがある
 */

// フォローする
function follow(userId, followId) {
	
	// ログインしていなければ終了
	if (userId == "null") {
		return;
	}
	
	// フォローIDを追加する
	UserBean.addFollowId(userId, followId);
	
	// ボタンを取得
	var btn = document.getElementById("btn");
	
	// 表示を変更
	btn.value = "フォローしました";
	
	// 押せなくする
	btn.disabled = true;
}

// フォロー解除
function notfollow(userId, followId) {
	
	// フォローIDから削除する
	UserBean.removeFollowId(userId, followId);
	
	// ボタンを取得
	var btn = document.getElementById("btn");
	
	// 表示を変更
	btn.value = "解除しました";
	
	// 押せなくする
	btn.disabled = true;
}

// フォロー申請を送る
function sendRequest(userId, followWaitId) {
	
	// ログインしていなければ終了
	if (userId == "null") {
		return;
	}
	
	// フォロー申請中IDを追加する
	UserBean.addFollowWaitId(userId, followWaitId);
	
	// ボタンを取得
	var btn = document.getElementById("btn");
	
	// 表示を変更
	btn.value = "フォロー申請中";
	
	// 押せなくする
	btn.disabled = true;
}

// リクエストを承認する
function approve(num, userId, followerWaitId) {
	
	// フォロワー申請中IDを承認する
	UserBean.approveFollowerWaitId(userId, followerWaitId);
	
	// divを取得
	var btnset = document.getElementById("btnset" + num);
	
	// ボタンを消して メッセージ付きのボタンを追加
	while (btnset.firstChild) {
		btnset.removeChild(btnset.firstChild);
	}
	var btn = document.createElement("input");
	btn.setAttribute("type", "button");
	btn.setAttribute("class", "okBigBtn");
	btn.setAttribute("value", "承認しました");
	btn.disabled = true;
	btnset.appendChild(btn);
}

// リクエストを拒否する
function reject(num, userId, followerWaitId) {
	
	// フォロワー申請中IDを拒否する
	UserBean.rejectFollowerWaitId(userId, followerWaitId);
	
	// divを取得
	var btnset = document.getElementById("btnset" + num);
	
	// ボタンを消して メッセージ付きのボタンを追加
	while (btnset.firstChild) {
		btnset.removeChild(btnset.firstChild);
	}
	var btn = document.createElement("input");
	btn.setAttribute("type", "button");
	btn.setAttribute("class", "ngBigBtn");
	btn.setAttribute("value", "拒否しました");
	btn.disabled = true;
	btnset.appendChild(btn);
}

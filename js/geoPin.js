/* 地図指定閲覧用 ピン
   
   座標、投稿ID、マーカー(ピンの絵)、メッシュを管理
   
   makePins() でピンを生成する
   postArray という変数に投稿の位置情報などがあれば、
   それをもとにピンを立てていく
   
   近くにあるピンをまとめるために、地域メッシュを利用する
   地図の拡大率でメッシュの細かさを決める
   投稿を 1つずつ処理していき、もし既にあるピンにまとめる場合、
   そのピンに投稿IDだけ追加する
   まとめずに新たにピンを立てる場合は、
   ピンに緯度経度とメッシュコードをセットし、
   マーカーとメッシュも生成する
   ピン(マーカー)にはクリックイベントをつける
   
   ピンがクリックされたとき、マーカーをアニメーションさせ、
   geoTimeline.js の narrow() でタイムラインを絞る
   そのピンに 1つの投稿しかない場合は投稿詳細を表示する
 */

var pinArray = []; // ピンの配列
var animationPin = null; // アニメーション中のピン

// ピンの定義
function Pin(meshcode, lat, lng) {
	this.meshcode = meshcode; // メッシュコード
	this.lat = lat;           // 緯度
	this.lng = lng;           // 経度
	this.postIdArray = [];    // 投稿ID
	this.marker = null;       // マーカー
}

Pin.prototype = {
	
	// 投稿IDを追加
	addPostId: function(postId) {
		this.postIdArray.push(postId);
	},
	
	// マーカーをセット
	setMarker: function() {
		this.marker = new google.maps.Marker({
			position: new google.maps.LatLng(this.lat, this.lng),
			map: map
		});
		
		// クリックイベント
		var pin = this;
		google.maps.event.addListener(this.marker, 'click', function(e) {
			pinClick(pin);
		});
	},
	
	// マーカーを削除
	removeMarker: function() {
		this.marker.setMap(null);
		this.marker = null;
	}
}

// ピンを使う場合の初期設定
function pinInit() {
	
	// ピンを生成する
	makePins();
}

// ピンを生成する
function makePins() {
	
	// アニメーションしているピンがあれば停止させる
	if (animationPin) {
		animationPin.marker.setAnimation(null);
		animationPin = null;
	}
	
	// 既に立っているマーカーを削除
	for (var i = 0; i<pinArray.length; i++) {
		pinArray[i].removeMarker();
	}
	
	// ピンの配列を初期化
	pinArray = [];
	
	// 地図の拡大率でメッシュコードの細かさを決める
	var meshLevel;
	var zoom = map.getZoom();
	if (zoom < 10) {
		meshLevel = 10000;
	} else if (zoom < 13) {
		meshLevel = 100;
	} else if (zoom < 15) {
		meshLevel = 1;
	} else {
		meshLevel = 0;
	}
	
	// 投稿リストがあればピンを生成
	loop: for (var i = 0; i<postArray.length; i++) {
		
		var pin;
		
		// メッシュコードを使う
		if (meshLevel > 0) {
			// メッシュコード
			var mc = parseInt(postArray[i].meshcode/meshLevel)
			
			// 同じメッシュコードのPinを探す
			for (var j = 0; j<pinArray.length; j++) {
				if (mc == pinArray[j].meshcode) {
					// 投稿IDを追加
					pinArray[j].addPostId(postArray[i].id);
					continue loop;
				}
			}
			
			// Pinクラスを生成 投稿リストのメッシュコードと緯度経度をセット
			pin = new Pin(mc, postArray[i].lat, postArray[i].lng);
		}
		
		// メッシュコードを使わない
		else {
			
			// 同じ緯度経度のPinを探す
			for (var j = 0; j<pinArray.length; j++) {
				if (postArray[i].lat == pinArray[j].lat &&
				    postArray[i].lng == pinArray[j].lng)
				{
					// 投稿IDを追加
					pinArray[j].addPostId(postArray[i].id);
					continue loop;
				}
			}
			
			// Pinクラスを生成 投稿リストの緯度経度をセット
			pin = new Pin(0, postArray[i].lat, postArray[i].lng);
		}
		
		// 投稿IDを追加
		pin.addPostId(postArray[i].id);
		
		// マーカーをセット
		pin.setMarker();
		
		// 配列に追加
		pinArray.push(pin);
	}
}

// ピンがクリックされたときの処理
function pinClick(pin) {
	
	// アニメーションしているピンがあれば停止させる
	if (animationPin) {
		animationPin.marker.setAnimation(null);
		animationPin = null;
	}
	
	// マーカーをアニメーションさせる
	pin.marker.setAnimation(google.maps.Animation.BOUNCE);
	animationPin = pin;
	
	// タイムラインを絞る
	// geoTimeline.js の処理
	narrow(pin.postIdArray);
	
	// 1つの投稿しかないとき
	if (pin.postIdArray.length == 1) {
		
		var id = pin.postIdArray[0];
		
		// 投稿詳細を取得して生成
		// detail.js の処理
		getPostByPostId(id);
		
		// コメントを取得して生成
		// detail.js の処理
		getCommentByPostId(id);
		
		// 縦スクロール量を取得する
		var scroll = document.documentElement.scrollTop || document.body.scrollTop;
		
		// 投稿詳細の位置をスクロール量だけずらす
		document.getElementById("articleDetail").style.marginTop = (scroll + 20) + "px";
		
		// 背景を暗くする
		document.getElementById("cover").style.opacity = "0.6";
		document.getElementById("cover").style.visibility = "visible";
		
		// 投稿詳細を写真モードにする
		isMapMode = false;
		document.getElementById("detailMap").style.display = "none";
		document.getElementById("detailPhoto").style.display = "block";
		
		// 投稿詳細を表示する
		document.getElementById("postDetail").style.visibility = "visible";
	}
}

/* ヘッダ
   
   画面読み込み完了後に、ヘッダ(id="header")にクリックイベントを追加する
   ホーム、検索、ランキング、アカウントボタンを押したときは、
   それぞれのURLに飛ぶ
   
   タイムラインを押したときは、友達閲覧と地図指定を、
   別の画面(ダイアログ)にして表示・非表示する
   友達閲覧、地図指定を押すと、それぞれのURLに飛ぶ
   
   設定を押したときも、タイムラインと同様に
   アカウント設定とログアウトが出る
   
   ブラウザの画面の幅が小さくて、ヘッダが収まらないときは、
   ブラウザの横スクロールに連動してヘッダもスクロールさせる
 */

var timelineFlag = false; // 友達と地図が表示されているかのフラグ
var settingFlag  = false; // アカウント設定とログアウトが表示されているかのフラグ
var postFlag     = false; // 写真投稿画面が表示されているかのフラグ

var headerDialogButtonSize = 0; // ヘッダのダイアログのボタンの大きさ

// 読み込み完了後にクリックイベントを追加
window.addEventListener("load", function() {
	
	// ヘッダのダイアログのボタンの大きさ
	var w = document.body.clientWidth / 2;
	var h = document.body.clientHeight;
	if (w > h) {
		headerDialogButtonSize = parseInt(h * 3 / 10);
	} else {
		headerDialogButtonSize = parseInt(w * 3 / 10);
	}
	document.getElementById("friendButton").style.width  = headerDialogButtonSize + "px";
	document.getElementById("friendButton").style.height = headerDialogButtonSize + "px";
	document.getElementById("mapButton").style.width  = headerDialogButtonSize + "px";
	document.getElementById("mapButton").style.height = headerDialogButtonSize + "px";
	document.getElementById("accountSettingButton").style.width  = headerDialogButtonSize + "px";
	document.getElementById("accountSettingButton").style.height = headerDialogButtonSize + "px";
	document.getElementById("logoutButton").style.width  = headerDialogButtonSize + "px";
	document.getElementById("logoutButton").style.height = headerDialogButtonSize + "px";
	document.getElementById("headerDialogContent").style.width =
	  (headerDialogButtonSize * 2.3) + "px";
	document.getElementById("headerDialogContent").style.height =
	  (headerDialogButtonSize * 1.1) + "px";
	document.getElementById("headerDialogContent").style.top =
	  ((h / 2) - (headerDialogButtonSize * 0.55)) + "px";
	document.getElementById("headerDialogContent").style.left =
	  (w - (headerDialogButtonSize * 1.15)) + "px";
	
	// 投稿画面が表示されている上で、違う場所が押された時
	document.getElementById("postCover").addEventListener("click", function(e) {
		
		if (e.target.id != "postDialog") {
			//投稿画面が表示されていた場合は消す
			if (postFlag) {
				
				// 背景を消す
				document.getElementById("postCover").style.opacity = "0";
				document.getElementById("postCover").style.visibility = "hidden";
				
				// 投稿画面を消す
				document.getElementById("postDialog").style.visibility = "hidden";
				document.getElementById("postDialogFileSelect").style.opacity    = "0";
				document.getElementById("postDialogFileSelect").style.visibility = "hidden";
				document.getElementById("postDialogFileSelect").style.transform  =
				  "translate(50%, 0) rotateY(90deg)";
				document.getElementById("postDialogMapSelect").style.opacity    = "0";
				document.getElementById("postDialogMapSelect").style.visibility = "hidden";
				document.getElementById("postDialogMapSelect").style.transform  =
				  "translate(50%, 0) rotateY(90deg)";
				
				postFlag = false;
			}
		}
	});
	
	//タイムラインのダイアログ表示時に他の場所がクリックされると消える
	document.getElementById("headerDialog").addEventListener("click", function(e) {
		if (e.target.id != "headerDialogContent") {
			if (timelineFlag) {
				document.getElementById("headerDialog").style.visibility = "hidden";
				document.getElementById("headerDialogContent").style.opacity = "0";
				document.getElementById("friendButton").style.visibility = "hidden";
				document.getElementById("friendButton").style.opacity = "0";
				document.getElementById("mapButton").style.visibility = "hidden";
				document.getElementById("mapButton").style.opacity = "0";
				timelineFlag = false;
			}
		}
	});
	
	//設定のダイアログ表示時に他の場所がクリックされると消える
	document.getElementById("headerDialog").addEventListener("click", function(e) {
		if (e.target.id != "headerDialogContent") {
			if (settingFlag) {
				document.getElementById("headerDialog").style.visibility = "hidden";
				document.getElementById("headerDialogContent").style.opacity = "0";
				document.getElementById("accountSettingButton").style.visibility = "hidden";
				document.getElementById("accountSettingButton").style.opacity = "0";
				document.getElementById("logoutButton").style.visibility = "hidden";
				document.getElementById("logoutButton").style.opacity = "0";
				settingFlag = false;
			}
		}
	});
	
	// ヘッダーの各項目を押したときにそれぞれの処理
	document.getElementById("header").addEventListener("click", function(e) {
		
		// ホームが押されたとき
		if (e.target.id == "homeButton") {
			location.href = "/teamb/pictshare/home";
		}
		
		// 投稿が押されたとき
		if (e.target.id == "postButton") {
			// ログインしていなければ終了
			if (userId == "null") {
				return;
			}
			//投稿画面が表示されていた場合は消す
			if (postFlag) {
				
				// 背景を消す
				document.getElementById("postCover").style.opacity = "0";
				document.getElementById("postCover").style.visibility = "hidden";
				
				// 投稿画面を消す
				document.getElementById("postDialog").style.visibility = "hidden";
				document.getElementById("postDialogFileSelect").style.opacity    = "0";
				document.getElementById("postDialogFileSelect").style.visibility = "hidden";
				document.getElementById("postDialogFileSelect").style.transform  =
				  "translate(50%, 0) rotateY(90deg)";
				document.getElementById("postDialogMapSelect").style.opacity    = "0";
				document.getElementById("postDialogMapSelect").style.visibility = "hidden";
				document.getElementById("postDialogMapSelect").style.transform  =
				  "translate(50%, 0) rotateY(90deg)";
				
				postFlag = false;
			} else {
				// 位置情報取得
				getPos();
				
				// 縦スクロール量を取得する
				var scroll = document.documentElement.scrollTop || document.body.scrollTop;
				
				// 投稿画面の位置をスクロール量だけずらす
				document.getElementById("post_article").style.marginTop = (scroll + 20) + "px";
				
				// 背景を暗くする
				document.getElementById("postCover").style.opacity = "0.6";
				document.getElementById("postCover").style.visibility = "visible";
				
				// 投稿画面を表示する
				document.getElementById("postDialogFileSelect").style.opacity    = "1";
				document.getElementById("postDialogFileSelect").style.visibility = "visible";
				document.getElementById("postDialogFileSelect").style.transform  =
				  "translate(0, 0) rotateY(0)";
				document.getElementById("postDialogMapSelect").style.opacity     = "0";
				document.getElementById("postDialogMapSelect").style.visibility  = "hidden";
				document.getElementById("postDialogMapSelect").style.transform   =
				  "translate(50%, 0) rotateY(90deg)";
				document.getElementById("postDialog").style.visibility = "visible";
				
				postFlag = true;
				//タイムラインダイアログが表示されていれば消す
				if (timelineFlag) {
					document.getElementById("headerDialog").style.visibility = "hidden";
					document.getElementById("headerDialogContent").style.opacity = "0";
					document.getElementById("friendButton").style.visibility = "hidden";
					document.getElementById("friendButton").style.opacity = "0";
					document.getElementById("mapButton").style.visibility = "hidden";
					document.getElementById("mapButton").style.opacity = "0";
					timelineFlag = false;
				}
				//設定ダイアログが表示されていれば消す
				if (settingFlag) {
					document.getElementById("headerDialog").style.visibility = "hidden";
					document.getElementById("headerDialogContent").style.opacity = "0";
					document.getElementById("accountSettingButton").style.visibility = "hidden";
					document.getElementById("accountSettingButton").style.opacity = "0";
					document.getElementById("logoutButton").style.visibility = "hidden";
					document.getElementById("logoutButton").style.opacity = "0";
					settingFlag = false;
				}
			}
		}
		
		// タイムラインが押されたとき
		if (e.target.id == "timelineButton") {
			
			// アカウント設定とログアウトが表示されているなら
			// 友達と地図に変える
			if (settingFlag) {
				document.getElementById("friendButton").style.visibility = "visible";
				document.getElementById("friendButton").style.opacity = "1";
				document.getElementById("mapButton").style.visibility = "visible";
				document.getElementById("mapButton").style.opacity = "1";
				document.getElementById("accountSettingButton").style.visibility = "hidden";
				document.getElementById("accountSettingButton").style.opacity = "0";
				document.getElementById("logoutButton").style.visibility = "hidden";
				document.getElementById("logoutButton").style.opacity = "0";
				timelineFlag = true;
				settingFlag  = false;
			}
			
			// 友達と地図が表示されているなら消す
			else if (timelineFlag) {
				document.getElementById("headerDialog").style.visibility = "hidden";
				document.getElementById("headerDialogContent").style.opacity = "0";
				document.getElementById("friendButton").style.visibility = "hidden";
				document.getElementById("friendButton").style.opacity = "0";
				document.getElementById("mapButton").style.visibility = "hidden";
				document.getElementById("mapButton").style.opacity = "0";
				timelineFlag = false;
			}
			
			// 何も表示されていないなら友達と地図を表示
			else {
				document.getElementById("headerDialog").style.visibility = "visible";
				document.getElementById("headerDialogContent").style.opacity = "1";
				document.getElementById("friendButton").style.visibility = "visible";
				document.getElementById("friendButton").style.opacity = "1";
				document.getElementById("mapButton").style.visibility = "visible";
				document.getElementById("mapButton").style.opacity = "1";
				timelineFlag = true;
			}
			//投稿画面が表示されていた場合は消す
			if (postFlag) {
				
				// 背景を消す
				document.getElementById("postCover").style.opacity = "0";
				document.getElementById("postCover").style.visibility = "hidden";
				
				// 投稿画面を消す
				document.getElementById("postDialog").style.visibility = "hidden";
				document.getElementById("postDialogFileSelect").style.opacity    = "0";
				document.getElementById("postDialogFileSelect").style.visibility = "hidden";
				document.getElementById("postDialogFileSelect").style.transform  =
				  "translate(50%, 0) rotateY(90deg)";
				document.getElementById("postDialogMapSelect").style.opacity    = "0";
				document.getElementById("postDialogMapSelect").style.visibility = "hidden";
				document.getElementById("postDialogMapSelect").style.transform  =
				  "translate(50%, 0) rotateY(90deg)";
				
				postFlag = false;
			} 
		}
		
		// 検索が押されたとき
		if (e.target.id == "searchButton") {
			location.href = "/teamb/pictshare/search";
		}
		
		// ランキングが押されたとき
		if (e.target.id == "rankingButton") {
			location.href = "/teamb/pictshare/ranking";
		}
		
		// アカウントが押されたとき
		if (e.target.id == "accountButton") {
			// ログインしていなければ終了
			if (userId == "null") {
				return;
			}
			location.href = "/teamb/pictshare/user/"+userId;
		}
		
		// 設定が押されたとき
		if (e.target.id == "settingButton") {
			
			// 友達と地図が表示されているなら
			// アカウント設定とログアウトに変える
			if (timelineFlag) {
				document.getElementById("accountSettingButton").style.visibility = "visible";
				document.getElementById("accountSettingButton").style.opacity = "1";
				document.getElementById("logoutButton").style.visibility = "visible";
				document.getElementById("logoutButton").style.opacity = "1";
				document.getElementById("friendButton").style.visibility = "hidden";
				document.getElementById("friendButton").style.opacity = "0";
				document.getElementById("mapButton").style.visibility = "hidden";
				document.getElementById("mapButton").style.opacity = "0";
				settingFlag  = true;
				timelineFlag = false;
			}
			
			// アカウント設定とログアウトが表示されているなら消す
			else if (settingFlag) {
				document.getElementById("headerDialog").style.visibility = "hidden";
				document.getElementById("headerDialogContent").style.opacity = "0";
				document.getElementById("accountSettingButton").style.visibility = "hidden";
				document.getElementById("accountSettingButton").style.opacity = "0";
				document.getElementById("logoutButton").style.visibility = "hidden";
				document.getElementById("logoutButton").style.opacity = "0";
				settingFlag = false;
			}
			
			// 何も表示されていないならアカウント設定とログアウトを表示
			else {
				document.getElementById("headerDialog").style.visibility = "visible";
				document.getElementById("headerDialogContent").style.opacity = "1";
				document.getElementById("accountSettingButton").style.visibility = "visible";
				document.getElementById("accountSettingButton").style.opacity = "1";
				document.getElementById("logoutButton").style.visibility = "visible";
				document.getElementById("logoutButton").style.opacity = "1";
				settingFlag = true;
			}
			//投稿画面が表示されていた場合は消す
			if (postFlag) {
				
				// 背景を消す
				document.getElementById("postCover").style.opacity = "0";
				document.getElementById("postCover").style.visibility = "hidden";
				
				// 投稿画面を消す
				document.getElementById("postDialog").style.visibility = "hidden";
				document.getElementById("postDialogFileSelect").style.opacity    = "0";
				document.getElementById("postDialogFileSelect").style.visibility = "hidden";
				document.getElementById("postDialogFileSelect").style.transform  =
				  "translate(50%, 0) rotateY(90deg)";
				document.getElementById("postDialogMapSelect").style.opacity    = "0";
				document.getElementById("postDialogMapSelect").style.visibility = "hidden";
				document.getElementById("postDialogMapSelect").style.transform  =
				  "translate(50%, 0) rotateY(90deg)";
				
				postFlag = false;
			} 
		}
		
	});
	
	// 友達閲覧が押されたとき
	document.getElementById("friendButton").addEventListener("click", function(e) {
		location.href = "/teamb/pictshare/friend";
	});
	
	// 地図指定が押されたとき
	document.getElementById("mapButton").addEventListener("click", function(e) {
		location.href = "/teamb/pictshare/map";
	});
	
	// アカウント設定が押されたとき
	document.getElementById("accountSettingButton").addEventListener("click", function(e) {
		// ログインしていなければ終了
		if (userId == "null") {
			return;
		}
		location.href = "/teamb/pictshare/setting";
	});
	
	// ログアウトが押されたとき
	document.getElementById("logoutButton").addEventListener("click", function(e) {
		location.href = "/teamb/pictshare/logout";
	});
}, false);
 
// 画面を横にスクロールしたときにヘッダーもスクロールさせる
window.onscroll = function() {
	
	// ヘッダーのちらつき防止
	document.getElementById("header").style.height = "41px";
	
	// 横スクロール量を取得する
	var scroll = document.documentElement.scrollLeft || document.body.scrollLeft;
	
	// ヘッダーをスクロール
	document.getElementById("header").style.left = (-scroll) + "px";
	
	// ヘッダーのちらつき防止
	document.getElementById("header").style.height = "40px";
	
	// 縦スクロール量を取得する
	var scroll = document.documentElement.scrollTop || document.body.scrollTop;
	
	// 投稿画面の位置をスクロール量だけずらす
	document.getElementById("post_article").style.marginTop = (scroll + 20) + "px";
}

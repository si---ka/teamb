/* 評価
   
   Good を押したときは高評価をつける
   Bad を押したときは低評価
   DWR で PostBean のメソッドを実行する
 */

// 高評価をつける
function good(postId, badTrue) {
	
	// ログインしていなければ終了
	if (userId == "null") {
		return;
	}
	
	// 低評価をキャンセルする
	if (badTrue) {
		badCancel(postId, true);
	}
	
	// 高評価をつける
	PostBean.evaluatePost(postId, userId, true);
	
	// 評価する（ランキング用）
	ThisRankingBean.evaluatePost(postId, false);
	
	// 現在表示されている評価を1増やす
	var nowGoodHtml = document.getElementById("good"+postId).innerHTML;
	document.getElementById("good"+postId).innerHTML = "Good:" +
	  (parseInt(nowGoodHtml.substring(nowGoodHtml.indexOf(":") + 1)) + 1);
	
	// ボタンを変える
	var goodButton = document.getElementById("goodButton"+postId);
	var badButton  = document.getElementById("badButton"+postId);
	goodButton.setAttribute("class", "goodButton isEvaluated");
	goodButton.setAttribute("onclick", "goodCancel(" + postId +", false);");
	badButton.setAttribute("onclick", "bad(" + postId +", true);");
}

// 低評価をつける
function bad(postId, goodTrue) {
	
	// ログインしていなければ終了
	if (userId == "null") {
		return;
	}
	
	// 高評価をキャンセルする
	if (goodTrue) {
		goodCancel(postId, true);
	}
	
	// 低評価をつける
	PostBean.evaluatePost(postId, userId, false);
	
	// 現在表示されている評価を1増やす
	var nowBadHtml = document.getElementById("bad"+postId).innerHTML;
	document.getElementById("bad"+postId).innerHTML = "Bad:" +
	  (parseInt(nowBadHtml.substring(nowBadHtml.indexOf(":") + 1)) + 1);
	
	// ボタンを変える
	var goodButton = document.getElementById("goodButton"+postId);
	var badButton  = document.getElementById("badButton"+postId);
	goodButton.setAttribute("onclick", "good(" + postId +", true);");
	badButton.setAttribute("class", "badButton isEvaluated");
	badButton.setAttribute("onclick", "badCancel(" + postId +", false);");
}

// 高評価をキャンセルする
function goodCancel(postId, badTrue) {
	
	// ログインしていなければ終了
	if (userId == "null") {
		return;
	}
	
	// 評価をキャンセルする
	PostBean.cancelEvaluation(postId, userId);
	
	// 評価をキャンセルするする（ランキング用）
	ThisRankingBean.evaluatePost(postId, true);
	
	// 現在表示されている評価を1減らす
	var nowGoodHtml = document.getElementById("good"+postId).innerHTML;
	document.getElementById("good"+postId).innerHTML = "Good:" +
	  (parseInt(nowGoodHtml.substring(nowGoodHtml.indexOf(":") + 1)) - 1);
	
	// ボタンを変える
	var goodButton = document.getElementById("goodButton"+postId);
	var badButton  = document.getElementById("badButton"+postId);
	goodButton.setAttribute("class", "goodButton");
	goodButton.setAttribute("onclick", "good(" + postId +", false);");
	badButton.setAttribute("onclick", "bad(" + postId +", false);");
}

// 低評価をキャンセルする
function badCancel(postId, goodTrue) {
	
	// ログインしていなければ終了
	if (userId == "null") {
		return;
	}
	
	// 評価をキャンセルする
	PostBean.cancelEvaluation(postId, userId);
	
	// 現在表示されている評価を1減らす
	var nowBadHtml = document.getElementById("bad"+postId).innerHTML;
	document.getElementById("bad"+postId).innerHTML = "Bad:" +
	  (parseInt(nowBadHtml.substring(nowBadHtml.indexOf(":") + 1)) - 1);
	
	// ボタンを変える
	var goodButton = document.getElementById("goodButton"+postId);
	var badButton  = document.getElementById("badButton"+postId);
	goodButton.setAttribute("onclick", "good(" + postId +", false);");
	badButton.setAttribute("class", "badButton");
	badButton.setAttribute("onclick", "bad(" + postId +", false);");
}

function warning(){
	
	// 「OK」時の処理開始 ＋ 確認ダイアログの表示
	if(window.confirm('管理者にこの投稿を通報しますか？')){

		window.alert('管理者に通報しました。'); // 警告ダイアログを表示

	}
	
	else{

	}
}

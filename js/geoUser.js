/*
 * ユーザーの初期位置やユーザーの保持する緯度経度を管理
 */

var User = function(){											//クラスユーザーのコンストラクタ
	this.userLatLng = new google.maps.LatLng(null, null);		//ユーザーの現在地の座標
	this.userId = null;											//ユーザーID ログイン判定
	if(arguments[0] != null){									//もしコンストラクタに引数があればtrue
		this.userLatLng = arguments[0];							//ユーザーの座標に引数の座標を代入
	}
}

User.prototype = {												//Userのプロトタイプ
	setUserId: function(UserId){ this.userId = UserId; },		//ユーザIDのsetter,ログインしている場合はIDを格納する
	confirmLogin: function(){									//ログイン確認メソッド
		if(this.userId != null){ return true; }						//ユーザーIDがあればtrueを返す
		else { return false; }
	},

	getLatLng: function(){ return this.userLatLng.toString() },					//緯度経度まとめて
	setLatLng: function(receiveLatLng){ this.userLatLng = receiveLatLng; },	//緯度経度まとめて

	getLng: function(){ return this.userLatLng.lng(); },				//経度のgetter
	setLng: function(Lng){ this.userLatLng.lng = Lng; },			//経度のsetter

	getLat: function(){ return this.userLatLng.lat(); },				//緯度のgetter
	setLat: function(Lat){ this.userLatLng.lat = Lat; },				//緯度のsetter
}
